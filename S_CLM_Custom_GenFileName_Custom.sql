
CREATE OR REPLACE PROCEDURE `S_CLM_Custom_GenFileName_Custom`(
IN RUN_MODE Varchar(20),
IN V_ID  BIGINT(20),
IN V_Communication_Channel  VARCHAR(255), 
IN vEnd_CntCust  BIGINT(20), 
OUT Out_FileName TEXT, 
OUT Out_FileDelimiter VARCHAR(10),
OUT Out_FileMarker TEXT)
BEGIN
DECLARE CONTINUE HANDLER FOR SQLWARNING
BEGIN
	GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
	SET @logmsg=CONCAT(ifnull(@errsqlstate,'WARNING') , ifnull(@errno,'1'), ifnull(@errtext,'WARNING MESSAGE'));
    INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
		values ('S_CLM_Custom_GenFileName', @logmsg ,now(),null,@errno, 1);

END;
DECLARE EXIT HANDLER FOR SQLEXCEPTION
BEGIN
	GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
	SET @logmsg=CONCAT(ifnull(@errsqlstate,'ERROR') , ifnull(@errno,'1'), ifnull(@errtext,'ERROR MESSAGE'));
    INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
		values ('S_CLM_Custom_GenFileName', @logmsg ,now(),null,@errno,1);
SELECT 
    'S_CLM_Custom_GenFileName : Error Message :' AS '',
    @logmsg AS '';
END;

SET SQL_SAFE_UPDATES=0;
set foreign_key_checks=0;

SELECT 
    Header
INTO @vHeader FROM
    Communication_Template
WHERE
    ID = V_ID;
SELECT 
    Timeslot_Id
INTO @vTimeslot_Id FROM
    Communication_Template
WHERE
    ID = V_ID
LIMIT 1;

set @vTimeslot_Id = IFNULL(@vTimeslot_Id,-1);

SELECT File_Delimiter INTO Out_FileDelimiter FROM CLM_Channel where ChannelName=V_Communication_Channel;

if @vTimeslot_Id >0 THEN
	select Timeslot_Name into @Timeslot from CLM_Timeslot_Master where Timeslot_Id = @vTimeslot_Id;

ELSE
	SET @Timeslot = '_';

END IF;

SET @Timeslot = IFNULL(@Timeslot,"_");
SELECT 
    IFNULL(`Value`, 'STANDARD')
INTO @ExecAgency FROM
    M_Config
WHERE
    `Name` = 'Execution_Agency';

SET @is_reminder='N';
SET @is_compliance='N';
SET @ran_last_week='N';
SET @Gaurd_Rail=0;
SET @Reason = '';
SET @Gaurd_Rail_Check = 'N';


SET @File_Path= concat((select value from M_Config where Name='File_Path'));
IF @ExecAgency='STANDARD' THEN
SELECT concat(@File_Path,'/',
(select value from M_Config where Name='File_Name_Prefix'),'_',
V_Id,'_',
V_Communication_Channel,'_',
(select ExtCreativeKey from Communication_Template where ID=V_ID limit 1),'_',
(select replace(SenderKey,' ','') from Communication_Template where ID=V_ID LIMIT 1),'_',
@Timeslot,'_',
(select Event_Execution_Date_ID from Event_Execution where Communication_Template_ID = V_ID order by Event_Execution_Date_ID desc limit 1) ,'_',
vEnd_CntCust) into @FileNameWithoutPrefix;

ELSEIF @ExecAgency='ICS' THEN

SELECT concat(@File_Path,'/',
(select value from M_Config where Name='File_Name_Prefix'),'_',
(select value from M_Config where Name='Tenant_Name'),'_',
vEnd_CntCust,'_',
V_Communication_Channel,'_',
@Timeslot,'_',
(select replace(SenderKey,' ','') from Communication_Template where ID=V_ID LIMIT 1),'_',
V_Id,'_',
(select ExtCreativeKey from Communication_Template where ID=V_ID limit 1),'_',
(select Event_Execution_Date_ID from Event_Execution where Communication_Template_ID = V_ID order by Event_Execution_Date_ID desc limit 1)) into @FileNameWithoutPrefix;

ELSEIF @ExecAgency='RESULTICKS' THEN

SELECT concat(@File_Path,'/',
(select value from M_Config where Name='File_Name_Prefix'),'_',
(select value from M_Config where Name='Tenant_Name'),'_',
vEnd_CntCust,'_',
V_Communication_Channel,'_',
(select replace(SenderKey,' ','') from Communication_Template where ID=V_ID LIMIT 1),'_',
@Timeslot,'_',
V_Id,'_',
(select ExtCreativeKey from Communication_Template where ID=V_ID limit 1),'_',
1,'_',
(now(0)+1)) into @FileNameWithoutPrefix;

ELSE

SELECT concat(@File_Path,'/',
(select value from M_Config where Name='File_Name_Prefix'),'_',
V_Id,'_',
V_Communication_Channel,'_',
(select ExtCreativeKey from Communication_Template where ID=V_ID limit 1),'_',
(select replace(SenderKey,' ','') from Communication_Template where ID=V_ID LIMIT 1),'_',
@Timeslot,'_',
(select Event_Execution_Date_ID from Event_Execution where Communication_Template_ID = V_ID order by Event_Execution_Date_ID desc limit 1) ,'_',
vEnd_CntCust) into @FileNameWithoutPrefix;

END IF;



SELECT concat(@FileNameWithoutPrefix,'.',(SELECT File_Extension FROM CLM_Channel where ChannelName=V_Communication_Channel)) INTO Out_FileName;
 

SELECT Marker_File_Required INTO @Marker_File_Required FROM CLM_Channel where ChannelName=V_Communication_Channel;
IF @Marker_File_Required ='Y' AND (RUN_MODE='TEST' OR RUN_MODE='TEST_ALL') THEN
SELECT concat(@FileNameWithoutPrefix,'_',current_time()+0,'.',(SELECT Marker_File_Extension FROM CLM_Channel where ChannelName=V_Communication_Channel)) INTO Out_FileMarker;
        ELSEIF @Marker_File_Required ='Y' AND RUN_MODE<>'TEST' AND RUN_MODE<>'TEST_ALL' THEN
        SELECT concat(@FileNameWithoutPrefix,'.',(SELECT Marker_File_Extension FROM CLM_Channel where ChannelName=V_Communication_Channel)) INTO Out_FileMarker;
END IF;        


IF @Gaurd_Rail=1
THEN
	INSERT ignore INTO CLM_Guard_Rail
    values(concat(curdate(),'-',curtime()),Out_FileName,@Reason);
END IF;

END

