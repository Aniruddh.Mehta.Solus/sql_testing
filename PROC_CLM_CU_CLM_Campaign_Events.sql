
CREATE OR REPLACE PROCEDURE `PROC_CLM_CU_CLM_Campaign_Events`(
	IN vUserId BIGINT,  
	IN vEventName VARCHAR(1024), 
	IN vExtEventName VARCHAR(1024), 
	IN vCampaignThemeId BIGINT, 
	IN vCampaignId  BIGINT,    
	IN vCLMSegmentId  VARCHAR(128),-- Changed   
	IN vCampTriggerId BIGINT,  
	IN vChannelId BIGINT,  
	IN vMicroSegmentId BIGINT,  
	IN vCreativeId BIGINT,  
	IN vExecution_Sequence BIGINT,  
	INOUT vEventId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN

	DECLARE vRowCnt BIGINT DEFAULT 0;
	DECLARE vSqlstmt TEXT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;
		Select vCLMSegmentId;
	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_CU_CLM_Campaign_Events';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);
	set @vEventName=vEventName;
	IF vEventName IS NULL OR vEventName = '' THEN
		SET vErrMsg =  concat('Event Name is a Mandatory Field and cannot be empty or NULL ',@vEventName,'');
		SET vSuccess = 0;
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
	ELSE
	Select vEventId;
		IF vEventId IS NULL OR vEventId <= 0 THEN
		
		Select vEventName,vExtEventName,vCLMSegmentId;
			SET vSqlstmt = CONCAT(
					'INSERT IGNORE INTO CLM_Campaign_Events (
						EventName,
						ExtEventName,
						CampaignThemeId,
						CampaignId,
						CLMSegmentId,
						CampTriggerId,
						ChannelId,
						MicroSegmentId,
						CreativeId,
						Execution_Sequence,
						CreatedBy
					) VALUES ('
					,'"', vEventName, '"'				
					,',"', vExtEventName, '"'				
					,',', vCampaignThemeId
					,',', vCampaignId
					,',"', vCLMSegmentId, '"'	
					,',', vCampTriggerId
					,',', vChannelId
					,',', vMicroSegmentId
					,',', vCreativeId
					,',', vExecution_Sequence
					,',', vUserId
					,')'
				);

			SET vErrMsg = vSqlstmt;
            Select vSqlstmt;
			SET @sql_stmt = vSqlstmt;
			PREPARE EXEC_STMT FROM @sql_stmt;
			EXECUTE EXEC_STMT;
			DEALLOCATE PREPARE EXEC_STMT;
			CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 3, vErrMsg, vSuccess);
		ELSE
			
			SELECT EXISTS(
				SELECT 1 
				FROM CLM_Campaign_Events
				WHERE 
					EventId = vEventId
					AND Has_Records_In_Execution_Hist = 1
			) INTO vRowCnt;
			
			IF vRowCnt = 0 THEN
				Select 1;
				SET vSqlstmt = CONCAT(
						'UPDATE IGNORE CLM_Campaign_Events
						SET
							EventName = ','"', vEventName, '"'
							, ', ExtEventName = ','"', vExtEventName, '"'
							, ', CampaignThemeId = ', vCampaignThemeId
							, ', CampaignId = ', vCampaignId
							, ', CLMSegmentId = ','"', vCLMSegmentId, '"'
							, ', CampTriggerId = ', vCampTriggerId
							, ', ChannelId = ', vChannelId
							, ', MicroSegmentId = ', vMicroSegmentId
							, ', CreativeId = ', vCreativeId
							, ', Execution_Sequence = ', vExecution_Sequence
							, ', CreatedBy = ', vUserId
						, ' WHERE EventId = ', vEventId
						, ' AND Has_Records_In_Execution_Hist = 0'
					);
				
				SET vErrMsg = vSqlstmt;
				
				Select vSqlstmt;
				SET @sql_stmt = vSqlstmt;
				PREPARE EXEC_STMT FROM @sql_stmt;
				EXECUTE EXEC_STMT;
				DEALLOCATE PREPARE EXEC_STMT;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 4, vErrMsg, vSuccess);
			ELSE
				SET vErrMsg = 'The Campaign Event already exists and has been used in Campaign execution and hence cannot be modified.';
				SET vSuccess = 0;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 5, vErrMsg, vSuccess);
			END IF;
		END IF;
	END IF;
	
	SELECT EventId  INTO vEventId
	FROM CLM_Campaign_Events 
    WHERE
		CampaignThemeId = vCampaignThemeId
		AND CampaignId = vCampaignId
		AND CampTriggerId = vCampTriggerId
		AND ChannelId = vChannelId
		AND MicroSegmentId = vMicroSegmentId
	LIMIT 1;


/* This is updating the Execution Sequence Which is not required

	UPDATE
		CLM_Campaign_Events AS CCE,
		CLM_Campaign AS CC,
		CLM_Campaign_Trigger AS CCTRIG,
		CLM_CampaignTheme As CCT
	SET
		CCE.Execution_Sequence = ((CCT.Execution_Sequence * 10000000000000) + (CC.Execution_Sequence * 1000000000) + (CCTRIG.Execution_Sequence * 100000) + CCE.EventId)
		
	WHERE
		CC.CampaignId = CCE.CampaignId
		AND CCE.CampTriggerId = CCTRIG.CampTriggerId
		AND CC.CampaignThemeId = CCT.CampaignThemeId
		AND CCE.EventId = vEventId;
		
		Update CLM_Campaign_Events AS CCE,CLM_Campaign_Trigger AS CCTRIG
		set  CCTRIG.Execution_Sequence =CCE.Execution_Sequence
		where CCE.CampTriggerId = CCTRIG.CampTriggerId 
		   and      CCTRIG.CampTriggerId=vCampTriggerId;
           
 */
		

	SET vSuccess = 1;
	
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 6, vErrMsg, vSuccess);
END

