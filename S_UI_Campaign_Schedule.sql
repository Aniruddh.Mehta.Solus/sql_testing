
CREATE OR REPLACE PROCEDURE `S_UI_Campaign_Schedule`(IN in_request_json json, OUT out_result_json json)
BEGIN
   

    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN,Critical_Trigger varchar(400); 
	declare Only_One_Time text; 
	declare Recurring,Type_of_Event,Type_of_Channel,Event_Sent_Time,Start_Date,End_Date,Recurring_Start_Date,Recurring_End_Date text;
	-- declare Favourite_Day text;
	declare Event_Id varchar(400); 

	SET Only_One_Time = json_unquote(json_extract(in_request_json,"$.Only_One_Time"));
	SET Recurring = json_unquote(json_extract(in_request_json,"$.Recurring"));
	SET Event_Id = json_unquote(json_extract(in_request_json,"$.Event_Id"));
	SET Event_Sent_Time = json_unquote(json_extract(in_request_json,"$.Time"));
	SET @Type_of_Event = json_unquote(json_extract(in_request_json,"$.AGGREGATION_5"));
	-- SET Favourite_Day = json_unquote(json_extract(in_request_json,"$.Favourite_Day"));
	SET @Type_of_Channel = json_unquote(json_extract(in_request_json,"$.AGGREGATION_6"));
	SET Start_Date = json_unquote(json_extract(in_request_json,"$.Start_Date"));
	SET End_Date = json_unquote(json_extract(in_request_json,"$.End_Date"));
	
	SET Recurring_Start_Date = json_unquote(json_extract(in_request_json,"$.Recurring_Start_Date"));
	SET Recurring_End_Date = json_unquote(json_extract(in_request_json,"$.Recurring_End_Date"));
	
	SET Critical_Trigger = json_unquote(json_extract(in_request_json,"$.Critical_Trigger"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));

	Select Event_Id;
	Set Event_Id= replace (Event_Id,"[",'');
	
	Set Event_Id= replace (Event_Id,"]",'');
	Set Event_Id= replace (Event_Id,'"','');
	Set Event_Id= replace (Event_Id,', ',',');
	
	
	Set @Event_ID=Event_Id;
	Set   @vSuccess ='0';
	SET SQL_SAFE_UPDATES=0;
	SET foreign_key_checks=0;
	SET @temp_result = Null;
	
	Select @Event_ID;

If @Event_Id !=''
Then
	-- Updating the Replaced Query to defaul query which was entered while creating the Trigger.
	Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
	       Set CTReplacedQuery = CTReplacedQueryAsInUI
	where A.CampTriggerId=B.CampTriggerId and B.EventId =@Event_Id;
	
	If Event_Sent_Time =''
	Then Set Event_Sent_Time = NULL;
	END IF;
	
	If Critical_Trigger =''
	Then Set Critical_Trigger = NULL;
	END IF;
	


	SET @Query_String0 = Concat('UPDATE CLM_Campaign_Events
	SET Event_Sent_Time=NULL
	WHERE EventId in (',@Event_Id,');');
			SELECT @Query_String0;
			PREPARE statement from @Query_String0;
			Execute statement;
			Deallocate PREPARE statement;
	
	IF  Event_Sent_Time is not null 
		THEN 
			SET @Query_String0 = Concat('UPDATE CLM_Campaign_Events
				SET Event_Sent_Time="',Event_Sent_Time,'"
				WHERE EventId in (',@Event_Id,');');
			SELECT @Query_String0;
			PREPARE statement from @Query_String0;
			Execute statement;
			Deallocate PREPARE statement;
								
	END IF;	
	
	IF  Event_Sent_Time="NORMAL_TIME"
		THEN SET @Query_String0 = Concat('UPDATE CLM_Campaign_Events
				SET Event_Sent_Time=(Select Value from M_Config where Name="Default_Sent_time")
				WHERE EventId in (',@Event_Id,');');
				
			SELECT @Query_String0;
			PREPARE statement from @Query_String0;
			Execute statement;
			Deallocate PREPARE statement;	
				
	END IF;	

	SET @Query_String0 = Concat('UPDATE Event_Master_UI
				SET Critical_Trigger="N"
				WHERE Event_Id in (',@Event_Id,');');
			
			SELECT @Query_String0;
			PREPARE statement from @Query_String0;
			Execute statement;
			Deallocate PREPARE statement;
	
	IF  Critical_Trigger is not null
		THEN
			SET @Query_String0 = Concat('UPDATE M_Config
			SET Value="',Critical_Trigger,'"
			WHERE Name="Critical_Trigger_Email_ids";');
			
			SELECT @Query_String0;
			PREPARE statement from @Query_String0;
			Execute statement;
			Deallocate PREPARE statement;
			
			SET @Query_String0 = Concat('UPDATE Event_Master_UI
				SET Critical_Trigger="Y"
				WHERE Event_Id in (',@Event_Id,');');
			
			SELECT @Query_String0;
			PREPARE statement from @Query_String0;
			Execute statement;
			Deallocate PREPARE statement;
				
		
	END IF;

	

			If Only_One_Time !=''  
			  Then
-- ---------- One Time Event Run -------------------------------------------------------------------------------------------------------------------------------------------------------------------

					If 	Only_One_Time ="Today" and (Start_Date='' and End_Date='') and Recurring ="Not_Applicable"
					   Then 	SET @Query_String1 = Concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
																Set A.TriggerStartDate = CURDATE(),
																	A.TriggerEndDate = CURDATE()
																where A.CampTriggerId = B.CampTriggerId and B.EventId in (',@Event_Id,');');
																
								SELECT @Query_String1;
								PREPARE statement from @Query_String1;
								Execute statement;
								Deallocate PREPARE statement;
								
							
					 	  

					ElseIf 	Only_One_Time ="Tomorrow" and (Start_Date='' and End_Date='') and Recurring ="Not_Applicable"
					    Then SET @Query_String2 = Concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
							 Set A.TriggerStartDate = DATE_ADD(CURDATE(),INTERVAL 1 DAY),
								 A.TriggerEndDate = DATE_ADD(CURDATE(),INTERVAL 1 DAY)
							     where A.CampTriggerId = B.CampTriggerId and B.EventId in (',@Event_Id,');');
																
								SELECT @Query_String2;
								PREPARE statement from @Query_String2;
								Execute statement;
								Deallocate PREPARE statement;
					 	  
								 	  
							

					Elseif Only_One_Time !="Not_Applicable" and (Start_Date='' and End_Date='') and Recurring ="Not_Applicable"
						Then 
								SET @Query_String3 = Concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
																Set A.TriggerStartDate = "',Only_One_Time,'",
																	A.TriggerEndDate = "',Only_One_Time,'"
																  where A.CampTriggerId = B.CampTriggerId and B.EventId in (',@Event_Id,');');
																
								SELECT @Query_String3;
								PREPARE statement from @Query_String3;
								Execute statement;
								Deallocate PREPARE statement;
					 	  
					End If;

				Set @vSuccess ='1';
				SET @Query_String1a = Concat('Update Event_Master_UI A,Event_Master B
											Set  Scheduled_at =Concat(" From: ",date_format(Start_Date_ID,"%m-%d")," To: ",date_format(End_Date_ID,"%m-%d")," At: ",case when Event_Sent_Time is null then "NA" else Event_Sent_Time end )
											where  A.Event_Id=B.Id and B.Id  in (',@Event_Id,');');
																
				SELECT @Query_String1a;
				PREPARE statement from @Query_String1a;
				Execute statement;
				Deallocate PREPARE statement;
				
				
			End if;
			

			If Recurring !='' 
			   Then 

-- -----------------One Recurring Event Run --------------------------------------------------------------------------------------------------------------------------------------------------------------

					If 	Recurring =	"Daily" and  Only_One_Time ="Not_Applicable" and (Start_Date='' and End_Date='')
						Then  
								SET @Query_String3 = Concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
							  Set A.TriggerStartDate = CURDATE(),
							    A.TriggerEndDate = DATE_ADD(CURDATE(),INTERVAL 2000 DAY)
						      where A.CampTriggerId = B.CampTriggerId and B.EventId in (',@Event_Id,');');
																
								SELECT @Query_String3;
								PREPARE statement from @Query_String3;
								Execute statement;
								Deallocate PREPARE statement;
								
								
							SET @Query_String2a = Concat('Update Event_Master_UI A,Event_Master B
											Set  Scheduled_at =Concat(" From: ",date_format(Start_Date_ID,"%m-%d")," To: ",date_format(End_Date_ID,"%m-%d"),"<br>At: ",case when Event_Sent_Time is null then "NA" else Event_Sent_Time end )
											where  A.Event_Id=B.Id and B.Id in (',@Event_Id,');');
																
							SELECT @Query_String2a;
							PREPARE statement from @Query_String2a;
							Execute statement;
							Deallocate PREPARE statement;	

					ElseIf 	Recurring =	"Monday"  and  Only_One_Time ="Not_Applicable" and (Start_Date='' and End_Date='') and (Recurring_Start_Date!='' and Recurring_End_Date!='')
						Then  Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
							  Set CTReplacedQuery = Concat (CTReplacedQuery," and WeekDay(CURDATE())=0")
							  where A.CampTriggerId=B.CampTriggerId and B.EventId in (@Event_Id);
							  
							  SET @Query_String3 = Concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
							  Set A.TriggerStartDate = "',Recurring_Start_Date,'",
							    A.TriggerEndDate = "',Recurring_End_Date,'"
						      where A.CampTriggerId = B.CampTriggerId and B.EventId in (',@Event_Id,');');
																
								SELECT @Query_String3;
								PREPARE statement from @Query_String3;
								Execute statement;
								Deallocate PREPARE statement;
							
							SET @Query_String2a = Concat('Update Event_Master_UI A,Event_Master B
											Set  Scheduled_at =Concat(" Every Monday From: ",date_format(Start_Date_ID,"%m-%d")," To: ",date_format(End_Date_ID,"%m-%d")," At: ",case when Event_Sent_Time is null then "NA" else Event_Sent_Time end )
											where  A.Event_Id=B.Id and B.Id in (',@Event_Id,');');
																
							SELECT @Query_String2a;
							PREPARE statement from @Query_String2a;
							Execute statement;
							Deallocate PREPARE statement;
					        
					ElseIf 	Recurring =	"Tuesday"  and  Only_One_Time ="Not_Applicable" and (Start_Date='' and End_Date='')and (Recurring_Start_Date!='' and Recurring_End_Date!='')
						Then     
						Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
								  Set CTReplacedQuery = Concat (CTReplacedQuery," and WeekDay(CURDATE())=1")
								  where A.CampTriggerId=B.CampTriggerId and B.EventId in (@Event_Id);
							 
							 SET @Query_String3 = Concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
							  Set A.TriggerStartDate = "',Recurring_Start_Date,'",
							    A.TriggerEndDate = "',Recurring_End_Date,'"
						      where A.CampTriggerId = B.CampTriggerId and B.EventId in (',@Event_Id,');');
																
								SELECT @Query_String3;
								PREPARE statement from @Query_String3;
								Execute statement;
								Deallocate PREPARE statement;
														
								SET @Query_String2a = Concat('Update Event_Master_UI A,Event_Master B
											Set  Scheduled_at =Concat(" Every Tuesday From: ",date_format(Start_Date_ID,"%m-%d")," To: ",date_format(End_Date_ID,"%m-%d")," At: ",case when Event_Sent_Time is null then "NA" else Event_Sent_Time end )
											where  A.Event_Id=B.Id and B.Id in (',@Event_Id,');');
																
							SELECT @Query_String2a;
							PREPARE statement from @Query_String2a;
							Execute statement;
							Deallocate PREPARE statement;
					        

					ElseIf 	Recurring =	"Wednesday"  and  Only_One_Time ="Not_Applicable" and (Start_Date='' and End_Date='')and (Recurring_Start_Date!='' and Recurring_End_Date!='')
						Then  Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
							  Set CTReplacedQuery = Concat (CTReplacedQuery," and WeekDay(CURDATE())=2")
							  where A.CampTriggerId=B.CampTriggerId and B.EventId in (@Event_Id);
									
							  SET @Query_String3 = Concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
							  Set A.TriggerStartDate = "',Recurring_Start_Date,'",
							    A.TriggerEndDate = "',Recurring_End_Date,'"
						      where A.CampTriggerId = B.CampTriggerId and B.EventId in (',@Event_Id,');');
																
								SELECT @Query_String3;
								PREPARE statement from @Query_String3;
								Execute statement;
								Deallocate PREPARE statement;
							
								SET @Query_String2a = Concat('Update Event_Master_UI A,Event_Master B
											Set  Scheduled_at =Concat(" Every Wednesday From: ",date_format(Start_Date_ID,"%m-%d")," To: ",date_format(End_Date_ID,"%m-%d")," At: ",case when Event_Sent_Time is null then "NA" else Event_Sent_Time end )
											where  A.Event_Id=B.Id and B.Id in (',@Event_Id,');');
																
							SELECT @Query_String2a;
							PREPARE statement from @Query_String2a;
							Execute statement;
							Deallocate PREPARE statement;
							
					ElseIf 	Recurring =	"Thursday"  and  Only_One_Time ="Not_Applicable" and (Start_Date='' and End_Date='')and (Recurring_Start_Date!='' and Recurring_End_Date!='')
						Then  Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
							  Set CTReplacedQuery = Concat (CTReplacedQuery," and WeekDay(CURDATE())=3")
							  where A.CampTriggerId=B.CampTriggerId and B.EventId in (@Event_Id);
									
							  SET @Query_String3 = Concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
							  Set A.TriggerStartDate = "',Recurring_Start_Date,'",
							    A.TriggerEndDate = "',Recurring_End_Date,'"
						      where A.CampTriggerId = B.CampTriggerId and B.EventId in (',@Event_Id,');');
																
								SELECT @Query_String3;
								PREPARE statement from @Query_String3;
								Execute statement;
								Deallocate PREPARE statement;
								
								SET @Query_String2a = Concat('Update Event_Master_UI A,Event_Master B
											Set  Scheduled_at =Concat(" Every Thursday From: ",date_format(Start_Date_ID,"%m-%d")," To: ",date_format(End_Date_ID,"%m-%d")," At: ",case when Event_Sent_Time is null then "NA" else Event_Sent_Time end )
											where  A.Event_Id=B.Id and B.Id in (',@Event_Id,');');
																
							SELECT @Query_String2a;
							PREPARE statement from @Query_String2a;
							Execute statement;
							Deallocate PREPARE statement;
							
					ElseIf 	Recurring =	"Friday"  and  Only_One_Time ="Not_Applicable" and (Start_Date='' and End_Date='')and (Recurring_Start_Date!='' and Recurring_End_Date!='')
						Then  Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
							  Set CTReplacedQuery = Concat (CTReplacedQuery," and WeekDay(CURDATE())=4")
							  where A.CampTriggerId=B.CampTriggerId and B.EventId in (@Event_Id);
									
							  SET @Query_String3 = Concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
							  Set A.TriggerStartDate = "',Recurring_Start_Date,'",
							    A.TriggerEndDate = "',Recurring_End_Date,'"
						      where A.CampTriggerId = B.CampTriggerId and B.EventId in (',@Event_Id,');');
																
								SELECT @Query_String3;
								PREPARE statement from @Query_String3;
								Execute statement;
								Deallocate PREPARE statement;
								
								SET @Query_String2a = Concat('Update Event_Master_UI A,Event_Master B
											Set  Scheduled_at =Concat(" Every Friday From: ",date_format(Start_Date_ID,"%m-%d")," To: ",date_format(End_Date_ID,"%m-%d")," At: ",case when Event_Sent_Time is null then "NA" else Event_Sent_Time end )
											where  A.Event_Id=B.Id and B.Id in (',@Event_Id,');');
																
							SELECT @Query_String2a;
							PREPARE statement from @Query_String2a;
							Execute statement;
							Deallocate PREPARE statement;
							
					ElseIf 	Recurring =	"Saturday"  and  Only_One_Time ="Not_Applicable" and (Start_Date='' and End_Date='')and (Recurring_Start_Date!='' and Recurring_End_Date!='')
						Then    Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
								Set CTReplacedQuery = Concat (CTReplacedQuery," and WeekDay(CURDATE())=5")
								where A.CampTriggerId=B.CampTriggerId and B.EventId in (@Event_Id);
									
							  SET @Query_String3 = Concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
							  Set A.TriggerStartDate = "',Recurring_Start_Date,'",
							    A.TriggerEndDate = "',Recurring_End_Date,'"
						      where A.CampTriggerId = B.CampTriggerId and B.EventId in (',@Event_Id,');');
																
								SELECT @Query_String3;
								PREPARE statement from @Query_String3;
								Execute statement;
								Deallocate PREPARE statement;
								
								SET @Query_String2a = Concat('Update Event_Master_UI A,Event_Master B
											Set  Scheduled_at =Concat(" Every Saturday From: ",date_format(Start_Date_ID,"%m-%d")," To: ",date_format(End_Date_ID,"%m-%d")," At: ",case when Event_Sent_Time is null then "NA" else Event_Sent_Time end )
											where  A.Event_Id=B.Id and B.Id in (',@Event_Id,');');
																
							SELECT @Query_String2a;
							PREPARE statement from @Query_String2a;
							Execute statement;
							Deallocate PREPARE statement;
							
					ElseIf 	Recurring =	"Sunday"  and  Only_One_Time ="Not_Applicable" and (Start_Date='' and End_Date='')and (Recurring_Start_Date!='' and Recurring_End_Date!='')
						Then    Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
							    Set CTReplacedQuery = Concat (CTReplacedQuery," and WeekDay(CURDATE())=6")
								where A.CampTriggerId=B.CampTriggerId and B.EventId in (@Event_Id);
									
							  SET @Query_String3 = Concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
							  Set A.TriggerStartDate = "',Recurring_Start_Date,'",
							    A.TriggerEndDate = "',Recurring_End_Date,'"
						      where A.CampTriggerId = B.CampTriggerId and B.EventId in (',@Event_Id,');');
																
								SELECT @Query_String3;
								PREPARE statement from @Query_String3;
								Execute statement;
								Deallocate PREPARE statement;
								
								SET @Query_String2a = Concat('Update Event_Master_UI A,Event_Master B
											Set  Scheduled_at =Concat(" Every Sunday From: ",date_format(Start_Date_ID,"%m-%d")," To: ",date_format(End_Date_ID,"%m-%d")," At: ",case when Event_Sent_Time is null then "NA" else Event_Sent_Time end )
											where  A.Event_Id=B.Id and B.Id in (',@Event_Id,');');
																
							SELECT @Query_String2a;
							PREPARE statement from @Query_String2a;
							Execute statement;
							Deallocate PREPARE statement;
							
							
					Elseif Recurring !="Not_Applicable"  and  Only_One_Time ="Not_Applicable" and (Start_Date='' and End_Date='')and (Recurring_Start_Date!='' and Recurring_End_Date!='')
						Then
								Update CLM_Campaign_Trigger A,CLM_Campaign_Events B
								Set CTReplacedQuery = Concat (CTReplacedQuery," and Day(CURDATE())=",Recurring)
								where A.CampTriggerId=B.CampTriggerId and B.EventId in (@Event_Id);
								
								SET @Query_String3 = Concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
							  Set A.TriggerStartDate = "',Recurring_Start_Date,'",
							    A.TriggerEndDate = "',Recurring_End_Date,'"
						      where A.CampTriggerId = B.CampTriggerId and B.EventId in (',@Event_Id,');');
																
								SELECT @Query_String3;
								PREPARE statement from @Query_String3;
								Execute statement;
								Deallocate PREPARE statement;
								
								SET @Query_String2a = Concat('Update Event_Master_UI A,Event_Master B
											Set  Scheduled_at =Concat(" Monthly on ',Recurring,'  From: ",date_format(Start_Date_ID,"%m-%d")," To: ",date_format(End_Date_ID,"%m-%d")," At: ",case when Event_Sent_Time is null then "NA" else Event_Sent_Time end )
											where  A.Event_Id=B.Id and B.Id in (',@Event_Id,');');
																
							SELECT @Query_String2a;
							PREPARE statement from @Query_String2a;
							Execute statement;
							Deallocate PREPARE statement;
							
					End If; 

					Set @vSuccess ='1';
			End if;

    End if;				
		
	If Start_Date!='' and End_Date=''
		THEN	SET @vSuccess ='0';
	END IF;	
	
	If Start_Date!='' and End_Date!=''
		THEN				SET @Query_String1 = Concat('Update CLM_Campaign_Trigger A, CLM_Campaign_Events B
							  Set A.TriggerStartDate = "',Start_Date,'",
							    A.TriggerEndDate = "',End_Date,'"
						      where A.CampTriggerId = B.CampTriggerId and B.EventId in (',@Event_Id,');');
		
							SELECT @Query_String1;
							PREPARE statement from @Query_String1;
							Execute statement;
							Deallocate PREPARE statement;
							
							SET @Query_String2a = Concat('Update Event_Master_UI A,Event_Master B
											Set  Scheduled_at =Concat(" From: ",date_format(Start_Date_ID,"%m-%d")," To: ",date_format(End_Date_ID,"%m-%d"),"<br>At: ",case when Event_Sent_Time is null then "NA" else Event_Sent_Time end )
											where  A.Event_Id=B.Id and B.Id in (',@Event_Id,');');
																
							SELECT @Query_String2a;
							PREPARE statement from @Query_String2a;
							Execute statement;
							Deallocate PREPARE statement;	
							Set @vSuccess ='1';		
							

	END IF;	
	
	
	 If IN_SCREEN ="VERIFIED"
		 Then 
			Set @IN_SCREEN=4;
	End If;

	If IN_SCREEN ="Active"
		 Then 
			  Set @IN_SCREEN=1;
	End If;
			
	If IN_SCREEN ="SCHEDULETRIGGERS"
	 Then 
		Set @IN_SCREEN=5;
	End If;	

	If IN_SCREEN ="Inactive"
		Then 
			Set @IN_SCREEN=0;
	End If;
		
	Set @Display_Formate="Message";
	Set @Sorting_Order = 'Event_Master_UI.ModifiedDate DESC,Execution_Sequence asc,Id desc';
		
	If @Type_of_Event is null
		then Set @Type_of_Event="CLM";
	End if;	
		
	Select @IN_SCREEN;
	If @IN_SCREEN is null or @IN_SCREEN<0
		then Select 1;Set @IN_SCREEN=3;
	end if;	
		
	If @Type_of_Channel is null 
		then Set @Type_of_Channel='SMS';
	end if;
		
	Select Value into @GlobalCoolOff from M_Config where Name='Communication_CoolOff_Days';
	Select Value into @CLM_Event_Limit from M_Config where Name='CLM_Event_Limit';
		
	IF @Type_of_Channel ="SMS"
		THEN Select Value into @ChannelCoolOff from M_Config where Name='SMS_Communication_CoolOff_Days';
	END IF;
		
	IF @Type_of_Channel ="Email"
		THEN Select Value into @ChannelCoolOff from M_Config where Name='Email_Communication_CoolOff_Days';
	END IF;
		
	IF @Type_of_Channel ="WhatsApp"
		THEN Select Value into @ChannelCoolOff from M_Config where Name='WhatsApp_Communication_CoolOff_Days';
	END IF;
		
	IF @Type_of_Channel ="PN"
		THEN Select Value into @ChannelCoolOff from M_Config where Name='PN_Communication_CoolOff_Days';
	END IF;
	if @GlobalCoolOff ='' then Set @GlobalCoolOff =0; end if;
	if @ChannelCoolOff ='' or @ChannelCoolOff is null then Set @ChannelCoolOff =0; end if;
	if @CLM_Event_Limit ='' then Set @CLM_Event_Limit =0; end if;
	Select @Display_Formate,@Type_of_Event, @IN_SCREEN,@Type_of_Channel,@GlobalCoolOff,@ChannelCoolOff,@CLM_Event_Limit,@Sorting_Order;

	
		 	
	 SET @view_stmt=concat('CREATE OR REPLACE VIEW V_UI_Campaign_List AS(
	 Select Id,Name,CLMSegmentName,ModifiedDate,Coverage,Scheduled_at,CampaignName,
	"" as More_Info,
	 
	 Status_Tested,Status_QC,Status_DLT,Status_Schedule,Status_Active,Message,CampaignUsesWaterfall,Execution_Sequence,Critical_Trigger
	From

	(Select 
	`CLM_Campaign_Events`.`EventId` as ID, CONCAT(`CLM_Campaign_Trigger`.`CampTriggerName`) AS `Name`,
	replace(replace(`EventSegmentName`,"(",""),")","") as CLMSegmentName, CampaignName,
	Case when  "',@Display_Formate,'" ="Message" then Replace(Replace(Message,"<SOLUS_PFIELD>","<b> #"),"</SOLUS_PFIELD>","# </b>") else Replace(Replace(`CLM_Creative`.Creative,"<SOLUS_PFIELD>","<b> #"),"</SOLUS_PFIELD>","# </b>") end as Message,
	Case when Event_Master_UI.Coverage <=0 then "NA" else Convert(format(Event_Master_UI.Coverage,","),CHAR) END as Coverage,
	date_format(Event_Master_UI.ModifiedDate,"%d-%m %H:%i") as ModifiedDate,
	CLM_Campaign_Trigger.TriggerStartDate as Scheduled_at,
	Case when Communication_Cooloff <=0 then "Mandatory" 
		else  Communication_Cooloff
	end as Communication_Cooloff_Days,
   
	Case  WHEN`CLM_Campaign_Trigger`.`TriggerUsesWaterfall` IS NULL 
			THEN 
					CASE WHEN `CLM_Campaign`.`CampaignUsesWaterfall` = 100 THEN "CMABPerfBased "	  
						WHEN CLM_Campaign_Events.Execution_Sequence=(Select Min(Execution_Sequence) from Event_Master) then "Top_Priority"
						WHEN row_number()over(order by CLM_Campaign_Events.Execution_Sequence) =1 then "Top_Priority" 
				ELSE row_number()over(order by CLM_Campaign_Events.Execution_Sequence)
				END
			ELSE
					CASE	WHEN `CLM_Campaign_Trigger`.`TriggerUsesWaterfall` = 100 THEN "CMABPerfBased "
						WHEN CLM_Campaign_Events.Execution_Sequence=(Select Min(Execution_Sequence) from Event_Master) then "Top_Priority"
						WHEN row_number()over(order by CLM_Campaign_Events.Execution_Sequence) =1 then "Top_Priority" 
				ELSE row_number()over(order by CLM_Campaign_Events.Execution_Sequence)
				END
				
	END as Priority,
	Case WHEN Event_Limit <=0 THEN "NA"
		
		 Else Concat(" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Throttling:",Event_Limit)
	end as Event_Limit,
	Case when Segment_Query_Condition != "1=1" THEN Concat(" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CampaigncSegment Name: ",Segment_Name)
	else "NA"
	end as Campaign_SegmentName
	,
	Case when Segment_Query_Condition != "1=1" THEN Segment_Query_Condition
	else "NA"
	end as Campaign_SegmentCondition
	,
	Case when RegionReplacedQuery != "1=1" THEN Concat(" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Region Segment Name: ",RegionSegmentName)
	else "NA"
	end as Region_SegmentName
	,
	Case when RegionReplacedQuery != "1=1" THEN RegionReplacedQuery
	else "NA"
	end as Region_SegmentCondition
	,
	
    Case when CTReplacedQuery Like "%Lt_Fav_Day_Of_Week%" THEN Concat(" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Favourite_Day: YES")
	else "NA"
	end as Favourite_Day,
	Case when CTReplacedQuery != "1=1" and  CTReplacedQuery Not Like "%Lt_Fav_Day_Of_Week =weekDay(now())" THEN 
			Concat(" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Additional_Criterai_SVOC: ",CTReplacedQuery)
        when CTReplacedQuery != "1=1" and  CTReplacedQuery = "Lt_Fav_Day_Of_Week =weekDay(now())" THEN  "NA" 	
     
    when CTReplacedQuery != "1=1" and  CTReplacedQuery  Like "%Lt_Fav_Day_Of_Week =weekDay(now())" THEN 
					Concat(" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Additional_Criterai_SVOC: ",Replace(CTReplacedQuery," and Lt_Fav_Day_Of_Week =weekDay(now())  ",""))   
   
	else "NA"
	end as Additional_Criterai_Replaced_Query,
	Case when  CTReplacedQueryBill != " "  Then Concat(" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Additional_Criterai_Bill: ",CTReplacedQueryBill)
		else "NA"
	end as Additional_Criterai_Bill_Query,
	
	Status_Tested,Status_QC,Status_DLT,Status_Schedule,Status_Active ,CampaignUsesWaterfall, CLM_Campaign_Events.Execution_Sequence,Critical_Trigger
	FROM
			(((((((((((`CLM_CampaignTheme`
			
			JOIN `CLM_Campaign`)
			JOIN `CLM_Campaign_Trigger`)
			JOIN `CLM_MicroSegment`)
			JOIN `CLM_Channel`)
			JOIN `CLM_Campaign_Events`)
			Join Event_Master_UI)
			JOIN `CLM_Creative`)
			JOIN CLM_Segment)
			
			LEFT JOIN `CLM_Offer` `CO` ON (`CLM_Creative`.`OfferId` = `CO`.`OfferId`))
			Left Join CLM_RegionSegment on (`CLM_Campaign_Events`.`RegionSegmentId` = `CLM_RegionSegment`.`RegionSegmentId` ))
			Left Join Campaign_Segment on (`CLM_Campaign_Events`.`Campaign_SegmentId` = `Campaign_Segment`.`Segment_Id` ))
		WHERE
			`CLM_Campaign_Events`.`CampaignThemeId` = `CLM_CampaignTheme`.`CampaignThemeId`
				
				AND `CLM_Campaign_Events`.`CampaignId` = `CLM_Campaign`.`CampaignId`
				AND `CLM_Campaign_Events`.`CampTriggerId` = `CLM_Campaign_Trigger`.`CampTriggerId`
				AND `CLM_Campaign_Events`.`MicroSegmentId` = `CLM_MicroSegment`.`MicroSegmentId`
				AND `CLM_Campaign_Events`.`ChannelId` = `CLM_Channel`.`ChannelId`
				AND `CLM_Campaign_Events`.`CreativeId` = `CLM_Creative`.`CreativeId`
				AND `CLM_Campaign_Events`.`EventId` = `Event_Master_UI`.`Event_ID`
				And    FIND_IN_SET(CLM_Segment.CLMSegmentId,CLM_Campaign_Events.CLMSegmentId)
				-- AND `CLM_Campaign_Events`.`RegionSegmentId` = `CLM_RegionSegment`.`RegionSegmentId` 
			AND Event_Master_UI.Type_Of_Event="',@Type_of_Event,'" AND CLM_Channel.ChannelName="',@Type_of_Channel,' " AND `CLM_Campaign_Trigger`.`CTState`= "',@IN_SCREEN,'"
		   GROUP BY EventId
	ORDER BY ',@Sorting_Order,')A
	);');
					   
						SELECT @view_stmt;
						PREPARE statement from @view_stmt;
						Execute statement;
						Deallocate PREPARE statement;
	
	If @vSuccess ='1'
		Then

			If IN_SCREEN ="VERIFIED"
			    Then 
						    
					-- Updating the Status in Event_Master_UI
        			 SET @Query_String =  concat('Update Event_Master_UI
					Set Status_Schedule ="Schedule"
					Where Event_Id in (',@Event_Id,')
					;') ;
			      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement;

				  SET @Update_Event = concat(' Update Event_Master_UI
					    	Set ModifiedDate =current_timestamp()
					    	Where Event_Id  in (',@Event_ID,');') ;
						
						SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
	 
	 
			        SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Verified_ID",`ID`,"Verified_name",`Name`,"Verified_Campaign",`CampaignName`,"Verified_msg",Message,"Verified_segment",`CLMSegmentName`,"Verified_comms",`Coverage`,"Verified_date",ModifiedDate,"Status_QC",`Status_QC`,"Status_DLT",`Status_DLT`,"Status_Schedule",`Status_Schedule`,"More_Info",`More_Info`) )),"]")
									    into @temp_result from  V_UI_Campaign_List
									     ;') ;
			      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement;

				
			End If;		

			
				If IN_SCREEN ="SCHEDULETRIGGERS" or IN_SCREEN ="SCHEDULE"
			    Then 
						    
					-- Updating the Status in Event_Master_UI
        			 SET @Query_String =  concat('Update Event_Master_UI
					Set Status_Schedule ="Schedule"
					Where Event_Id in (',@Event_Id,')
					;') ;
			      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement;

			
							
					-- Updating the Status in Event_Master_UI
        			 SET @Query_String =  concat('Update Event_Master_UI
					Set ModifiedDate =current_timestamp()
					Where Event_Id in (',@Event_Id,')
					;') ;
			      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement;		
	 
	 
			       SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Schedule_ID",`ID`,"Schedule_name",`Name`,"Schedule_Campaign",`CampaignName`,"Schedule_msg",Message,"Schedule_segment",`CLMSegmentName`,"Schedule_comms",`Coverage`,"Schedule_date",ModifiedDate,"Status_Schedule",`Status_Schedule`,"More_Info",`More_Info`,"Scheduled_at",`Scheduled_at`,"Critical_Trigger",Critical_Trigger) )),"]")
									    into @temp_result from V_UI_Campaign_List ;') ;
			      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement;

				
			End If;		

			
			If IN_SCREEN ="Active"
				Then 
					
					-- DISPLAY SCRREN
							
					-- Updating the Status in Event_Master_UI
        			 SET @Query_String =  concat('Update Event_Master_UI
					Set ModifiedDate =current_timestamp()
					Where Event_Id in (',@Event_Id,')
					;') ;
			      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement;		
					
					 SET @Update_Event = concat(' Update Event_Master_UI
					    	Set ModifiedDate =current_timestamp()
					    	Where Event_Id  in (',@Event_ID,');') ;
						
						SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
	 
					 SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Activate_ID",`ID`,"Activate_name",`Name`,"Activat_Campaign",`CampaignName`,"Activate_msg",Message,"Activate_segment",`CLMSegmentName`,"Activate_comms",`Coverage`,"Activate_date",ModifiedDate,"Status_QC",`Status_QC`,"Status_DLT",`Status_DLT`,"Status_Schedule",`Status_Schedule`,"Status_Active",`Status_Active`,"More_Info",`More_Info`,"Scheduled_at",`Scheduled_at`,"Critical_Trigger",Critical_Trigger) )),"]")
									    into @temp_result from V_UI_Campaign_List;') ;
			      
				   SELECT @Query_String;
				   PREPARE statement from @Query_String;
				   Execute statement;
				   Deallocate PREPARE statement; 



			End if;

			if IN_SCREEN ="Inactive"
			 Then 

				
				-- DISPLAY SCRREN
						
					-- Updating the Status in Event_Master_UI
        			 SET @Query_String =  concat('Update Event_Master_UI
					Set ModifiedDate =current_timestamp()
					Where Event_Id in (',@Event_Id,')
					;') ;
			      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement;		
	 
	 
	  SET @Update_Event = concat(' Update Event_Master_UI
					    	Set ModifiedDate =current_timestamp()
					    	Where Event_Id  in (',@Event_ID,');') ;
						
						SELECT @Update_Event;
						PREPARE statement from @Update_Event;
						Execute statement;
						Deallocate PREPARE statement;
					

							  SET @Query_String =  concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Inactivate_ID",`ID`,"Inactivate_name",`Name`,"Inactivate_Campaign",`CampaignName`,"Inactivate_msg",Message,"Inactivate_segment",`CLMSegmentName`,"Inactivate_comms",`Coverage`,"Inactivate_date",ModifiedDate,"Status_Schedule",`Status_Schedule`,"More_Info",`More_Info`,"Scheduled_at",`Scheduled_at`,"Critical_Trigger",Critical_Trigger) )),"]")
									    into @temp_result from V_UI_Campaign_List;') ;
							  
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 

							  if @temp_result is null
								then set @temp_result="[]";
							 End if;	


							SET out_result_json = @temp_result;
			   End If;



    ELSE 	Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","The Event is not Schedule") )),"]") into @temp_result ;') ;
   
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement;

	End if;			
	  


	if @temp_result is null
		
		Then
								
			Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") into @temp_result ;') ;
   
			SELECT @Query_String;
			PREPARE statement from @Query_String;
			Execute statement;
			Deallocate PREPARE statement; 
						
	End if;
							
	SET out_result_json = @temp_result;
								
 END

