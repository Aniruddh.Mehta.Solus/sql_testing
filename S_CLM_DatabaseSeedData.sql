
CREATE or replace PROCEDURE `S_CLM_DatabaseSeedData`()
begin
INSERT IGNORE INTO `M_Config`(`Name`,`Description`,`Value`)
VALUES
(
'File_Generation_Method',
'There are two options to generate file. Generate for each batch of each event or generate only one file for each event',
'Customer_Batch'
);
INSERT IGNORE INTO `M_Config`(`Name`,`Description`,`Value`)
VALUES
(
'File_Generation_BatchSize',
'You have two options to decide the no of records in file. Either system decides automatically or you specify. If you specify provide a valid intger number e.g. 1000000 ',
'AUTO'
);
INSERT IGNORE INTO `M_Config`(`Name`,`Description`,`Value`)
VALUES
(
'Recommendation_UseCase_Method',
'There are two options to generate file. Generate for each batch of each event or generate only one file for each event',
'Stories'
);
INSERT IGNORE INTO `M_Config`(`Name`,`Description`,`Value`)
VALUES
(
'SMS_Communication_CoolOff_Days',
'SMS Channel Communication Cool Off',
'0'
);
INSERT IGNORE INTO `M_Config`(`Name`,`Description`,`Value`)
VALUES
(
'Email_Communication_CoolOff_Days',
'Email Channel Communication Cool Off',
'0'
);
INSERT IGNORE INTO `M_Config`(`Name`,`Description`,`Value`)
VALUES
(
'PN_Communication_CoolOff_Days',
'PN Channel Communication Cool Off',
'0'
);
INSERT IGNORE INTO `M_Config`(`Name`,`Description`,`Value`)
VALUES
(
'WhatsApp_Communication_CoolOff_Days',
'WhatsApp Channel Communication Cool Off',
'0'
);
INSERT IGNORE INTO `M_Config`(`Name`,`Description`,`Value`)
VALUES
(
'Message_Update_Batch_Size',
'Message column updating loop',
'10000'
);
INSERT IGNORE INTO `M_Config`(`Name`,`Description`,`Value`)
VALUES
(
'Event_Execution_Batch_Size',
'Event Execution Batch Size',
'100000'
);

DELETE FROM M_Config where `Name` = 'Communication_File_Batch_Size';
END


