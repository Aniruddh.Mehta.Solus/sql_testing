
CREATE OR REPLACE PROCEDURE `S_Blueprint_Upload`()
BEGIN
/** Version : 2.6 Updated On : 20230620 **/

DECLARE Loop_Check_Var Int Default 0;
DECLARE cur_Campaign_Segment VARCHAR(128);

DECLARE curALL_Campaign_Segments CURSOR For 
SELECT 
   DISTINCT Campaign_Segment
FROM
    EventMaster_csv;
    
DECLARE CONTINUE HANDLER FOR NOT FOUND SET Loop_Check_Var=1;


set sql_safe_updates=0;  
alter table EventMaster_csv
add column if not exists Loading_Status varchar(128);

 
 update  EventMaster_csv 
set Loading_Status = 'Trigger already exists!'
where Trigger_Name in (select name from Event_Master);

update  EventMaster_csv 
set Loading_Status = 'Invalid Campaign Name'
where Campaign_Name not in (select CampaignName from CLM_Campaign)
and Loading_Status is NULL;

 

update  EventMaster_csv 
set Loading_Status = 'Invalid Lifecycle Segment'
where LifeCycle_Segment not in (select CLMSegmentName from CLM_Segment union select 'All')
and Loading_Status is NULL;


 SET Loop_Check_Var = FALSE;
OPEN curALL_Campaign_Segments;
LOOP_ALL_Campaign_Segments: LOOP
	FETCH curALL_Campaign_Segments into cur_Campaign_Segment;
	IF Loop_Check_Var THEN
	   LEAVE LOOP_ALL_Campaign_Segments;
	END IF;
	SELECT cur_Campaign_Segment,now();
    
	
	SET @Campaign_Segment_Names = cur_Campaign_Segment;

	SELECT @Campaign_Segment_Names;
	SELECT (length(@Campaign_Segment_Names)-length(replace(@Campaign_Segment_Names,',','')))+1 into @num_of_rc;
		SELECT @num_of_rc;
		SET @loop_var=1;
		
		
			
		WHILE @loop_var<=@num_of_rc
		DO
			SET @selected_name=substring_index(substring_index(@Campaign_Segment_Names,',',@loop_var),',',-1);
			SELECT @selected_name as 'Selected_Name';
            SELECT CONCAT("'",@selected_name,"'") as 'Passed_Value';
			update  EventMaster_csv 
			set Loading_Status = CONCAT('Invalid Campaign Segment :',@selected_name)
			where Campaign_Segment = CONCAT("'",cur_Campaign_Segment,"'")
            and CONCAT("'",@selected_name,"'") not in (select Segment_Name from Campaign_Segment)
			and Loading_Status is NULL;
					
			
			SET @loop_var=@loop_var+1;
		END WHILE;
END LOOP LOOP_ALL_Campaign_Segments;  
    
    

update  EventMaster_csv 
set Loading_Status = 'Invalid Channel'
where `Channel` not in (select ChannelName from CLM_Channel)
and Loading_Status is NULL;

 


insert ignore into CLM_Campaign_Trigger(CampTriggerName,CTState,Event_Limit,TriggerStartDate,TriggerEndDate,TriggerResponseDays,CTReplacedQUery,CTReplacedQueryAsInUI)
select Trigger_Name,3,Event_Limit,curdate(),date_add(curdate(),interval 10 day),'7',Custom_SQL,Custom_SQL from EventMaster_csv
where Loading_Status is NULL;

 

insert ignore into CLM_Creative(CreativeName,Creative,Header,Communication_Cooloff,Timeslot_Id,OfferId,ExtCreativeKey,SenderKey)
select Trigger_Name,Creative,File_Header,Comm_Cool_Off,'1','3',IFNULL(External_Campaign_Key,Trigger_Name),(SELECT IFNULL(Value,'SOLUS') FROM M_Config WHERE Name = 'Default_Sender_Key') AS Sender_Key from EventMaster_csv where Loading_Status is NULL;

 

INSERT IGNORE INTO CLM_Reco_In_Creative(CreativeId) SELECT CreativeId FROM  CLM_Creative A,  EventMaster_csv B
WHERE A.CreativeName = B.Trigger_Name
and Loading_Status is NULL;

 


insert ignore into CLM_Campaign_Events(EventName,CampaignId,CampTriggerId,ChannelId,CreativeId,CampaignThemeId,CLMSegmentId,MicroSegmentId,Campaign_SegmentId)
select Trigger_Name,b.CampaignId,c.CampTriggerId,d.ChannelId,e.CreativeId,b.CampaignThemeId,f.CLMSegmentId,1,GROUP_CONCAT(g.Segment_Id SEPARATOR ',') as Segment_Ids from EventMaster_csv a, CLM_Campaign b, CLM_Campaign_Trigger c, CLM_Channel d, CLM_Creative e, CLM_Segment f, Campaign_Segment g
where  
a.Channel = d.ChannelName
and a.Trigger_Name = e.CreativeName
and a.Trigger_Name = c.CampTriggerName
and b.CampaignName = a.Campaign_Name
and a.LifeCycle_Segment = f.CLMSegmentName 
and FIND_IN_SET(g.Segment_Name,a.Campaign_Segment)
and Loading_Status is NULL
Group by a.Trigger_Name;

 update CLM_Creative a, CLM_Campaign_Events b
 set a.ChannelId = b.ChannelId
 where a.CreativeId = b.CreativeId;

update EventMaster_csv
set Loading_Status = 'Failed'
where Trigger_Name not in (select name from Event_Master)
and Loading_Status is NULL;

 

update EventMaster_csv
set Loading_Status = 'Success'
where Trigger_Name in (select name from Event_Master)
and Loading_Status is NULL;

 

Insert Ignore Event_Master_UI (Event_Id)
Select ID from Event_Master where  name in (select Trigger_Name from EventMaster_csv where Loading_Status = 'Success');

 

Update Event_Master_UI A , Event_Master B, EventMaster_csv C
set A.Message = B.Creative1,
A.EventSegmentName = B.CLMSegmentName,
A.Coverage="0",
A.Type_Of_Event = C.Campaign_Type,
A.Trigger_Type = (case when  C.Campaign_Type = 'GTM'then 'GTM_Advance' else 'Advance' end)
    where A.Event_Id=B.ID
    and B.name = C.Trigger_Name;

													

END

