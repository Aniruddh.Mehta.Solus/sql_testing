
CREATE or replace PROCEDURE `S_CLM_Update_Segment_Count`()
begin
	SET SQL_SAFE_UPDATES=0;
    TRUNCATE Response_Segment_Count;
	INSERT INTO Response_Segment_Count(Segment_Id,Num_of_Customers)
    (SELECT Seg_For_model_3,COUNT(1) as cnt from svoc_segments group by Seg_For_model_3);
    select * from CLM_Segment;
    UPDATE Response_Segment_Count rsc, CLM_Segment cs 
    SET rsc.Segment_Name = cs.CLMSegmentName,
    rsc.Segment_Query = cs.CLMSegmentReplacedQuery
    WHERE rsc.Segment_Id=cs.CLMSegmentId;
    
    SELECT COUNT(1) into @Total_Cust from CDM_Customer_Master;
    select @Total_Cust;
    UPDATE Response_Segment_Count
    SET Percentage_of_Customers=(Num_of_Customers/@Total_Cust)*100;
    
    delete from Response_EEH_Count where Month_Year_of_Execution=concat(monthname(current_date()),'_',year(current_date()));
    INSERT INTO Response_EEH_Count(Num_of_Messages,Communication_Channel,Month_Year_of_Execution,Num_of_Customers)
    (select cnt,Communication_Channel,concat(monthname(current_date()),'_',year(current_date())),Count(1) from 
	(select Customer_Id,ct.Communication_Channel as Communication_Channel,count(1) cnt from Event_Execution_History eeh, Communication_Template ct
	where eeh.Communication_Template_Id= ct.ID
    AND Event_Execution_Date_ID between concat(date_format(current_date(),'%Y%m'),'01') and date_format(current_date(),'%Y%m%d')
	group by eeh.Customer_Id,ct.Communication_Channel)A
	group by cnt,Communication_Channel);
END

