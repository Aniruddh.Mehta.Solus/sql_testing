
CREATE or replace PROCEDURE `PROC_UI_CU_CLM_Campaign_Events`(
	IN vUserId BIGINT, 
	IN vEventName VARCHAR(1024),
	IN vExtEventName VARCHAR(1024),
	IN vCampaignThemeId BIGINT,
	IN vCampaignId BIGINT, 
	IN vCLMSegmentId   VARCHAR(128),-- Changed   
	IN vCampTriggerId BIGINT, 
	IN vChannelId BIGINT, 
	IN vMicroSegmentId BIGINT, 
	IN vCreativeId BIGINT, 
	IN vExecution_Sequence BIGINT, 
	INOUT vEventId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vResult TINYINT;
	DECLARE vInputValues TEXT;
	
	SET vSuccess = 1;
	SET vErrMsg = '';

	SET vInputValues  = CONCAT(
			'UserId: ', IFNULL(vUserId,'NULL VALUE')
			,' | vEventName: ',IFNULL(vEventName,'NULL VALUE')
			,' | vExtEventName: ',IFNULL(vExtEventName,'NULL VALUE')
			,' | vCampaignThemeId: ',IFNULL(vCampaignThemeId,'NULL VALUE')
			,' | vCampaignId: ',IFNULL(vCampaignId,'NULL VALUE')
			,' | vCLMSegmentId: ',IFNULL(vCLMSegmentId,'NULL VALUE')
			,' | vCampTriggerId: ',IFNULL(vCampTriggerId,'NULL VALUE')
			,' | vChannelId: ',IFNULL(vChannelId,'NULL VALUE')
			,' | vMicroSegmentId: ',IFNULL(vMicroSegmentId,'NULL VALUE')
			,' | vCreativeId: ',IFNULL(vCreativeId,'NULL VALUE')
			,' | vExecution_Sequence: ',IFNULL(vExecution_Sequence,'NULL VALUE')
			,' | vEventId: ',IFNULL(vEventId,'NULL VALUE')
		);

	IF vUserId IS NULL OR vUserId <= 0 THEN
		SET vErrMsg = CONCAT('User Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vEventName IS NULL OR vEventName = '' THEN
		SET vErrMsg = CONCAT('vEventName cannot be empty.');
		SET vSuccess = 0;
	END IF;
	IF vCampaignThemeId IS NULL OR vCampaignThemeId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid CampaignThemeId.');
		SET vSuccess = 0;
	END IF;
	IF vCampaignId IS NULL OR vCampaignId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid CampaignId');
		SET vSuccess = 0;
	END IF;
	IF vCLMSegmentId IS NULL OR vCLMSegmentId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid CLMSegmentId');
		SET vSuccess = 0;
	END IF;
	IF vCampTriggerId IS NULL OR vCampTriggerId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid CampTriggerId');
		SET vSuccess = 0;
	END IF;
	IF vChannelId IS NULL OR vChannelId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid ChannelId');
		SET vSuccess = 0;
	END IF;
	IF vMicroSegmentId IS NULL OR vMicroSegmentId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid MicroSegmentId');
		SET vSuccess = 0;
	END IF;
	IF vCreativeId IS NULL OR vCreativeId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid CreativeId');
		SET vSuccess = 0;
	END IF;

	IF vSuccess = 1 THEN 
	
	Select vUserId, 
		IFNULL(vEventName,''),
		IFNULL(vExtEventName,''),
		IFNULL(vCampaignThemeId ,0),
		IFNULL(vCampaignId ,0), 
		IFNULL(vCLMSegmentId ,0), 
		IFNULL(vCampTriggerId ,0), 
		IFNULL(vChannelId ,0), 
		IFNULL(vMicroSegmentId ,0), 
		IFNULL(vCreativeId ,0), 
		IFNULL(vExecution_Sequence ,0), 
		vEventId,
		vErrMsg,
		vSuccess;
	CALL PROC_CLM_CU_CLM_Campaign_Events(
			vUserId, 
		IFNULL(vEventName,''),
		IFNULL(vExtEventName,''),
		IFNULL(vCampaignThemeId ,0),
		IFNULL(vCampaignId ,0), 
		IFNULL(vCLMSegmentId ,0), 
		IFNULL(vCampTriggerId ,0), 
		IFNULL(vChannelId ,0), 
		IFNULL(vMicroSegmentId ,0), 
		IFNULL(vCreativeId ,0), 
		IFNULL(vExecution_Sequence ,0), 
		vEventId,
		vErrMsg,
		vSuccess
	);
	END IF;
	IF vSuccess = 0 THEN 
		SET vErrMsg = CONCAT(vErrMsg , '\r\n', vInputValues);	
	END IF;

END

