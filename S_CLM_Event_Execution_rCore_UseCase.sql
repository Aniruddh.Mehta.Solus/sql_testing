
CREATE or REPLACE PROCEDURE `S_CLM_Event_Execution_rCore_UseCase`(IN OnDemandEventId BIGINT(20),IN RUN_MODE Varchar(10), OUT no_of_customers BIGINT(20),OUT vSuccess int (4), OUT vErrMsg Text)
begin
DECLARE vEvent_Limit_1 varchar(16);
DECLARE vRecommendation_Filter_Logic varchar(2048);
Declare Event_Condition_SVOC_1, Event_Condition_Bill_1,Communication_CoolOff_1 text;
DECLARE V_Offer_Code_1 varchar(20);
DECLARE  Exe_ID, Event_ID_cur1_1, Communication_Template_ID_cur1_1, Reminder_Parent_TriggerId_cur1_1, Reminder_Days_cur1_1,vGoodTime_1,vEvent_CoolOff_Days_1 int;
DECLARE vCommunication_Channel_1 VARCHAR(256);
DECLARE done1,done2 INT DEFAULT FALSE;
DECLARE cur1 cursor for 
	SELECT 	em.ID,
			em.Replaced_Query, 
            em.Replaced_Query_Bill,
            em.Reminder_Parent_Event_Id, 
			em.Reminder_Days , 
            em.Communication_Template_ID,
            ct.Offer_Code,Event_Limit,
            em.Is_Good_Time,
            ct.Communication_Channel,
            ct.Recommendation_Filter_Logic
           
	FROM Event_Master em

    JOIN Communication_Template ct on ct.ID=em.Communication_Template_ID
    WHERE  
     ct.Template<>'' AND ct.Template is not NULL and ct.Template<>'<SOLUS_PFIELD></SOLUS_PFIELD>' and
		CASE
         WHEN OnDemandEventId = '' AND RUN_MODE='TEST' THEN em.ID in (Select ID from Event_Master where State='Testing') and (curdate() between F_Get_Std_Date(em.Start_Date_ID) and F_Get_Std_Date(em.End_Date_ID))
         WHEN OnDemandEventId = '' AND RUN_MODE='RUN' THEN em.ID in (Select ID from Event_Master where State='Enabled') and (curdate() between F_Get_Std_Date(em.Start_Date_ID) and F_Get_Std_Date(em.End_Date_ID))
		WHEN OnDemandEventId <> '' AND RUN_MODE='TEST' THEN em.ID in (Select ID from Event_Master where State='Testing') and (curdate() between F_Get_Std_Date(em.Start_Date_ID) and F_Get_Std_Date(em.End_Date_ID)) AND em.ID =OnDemandEventId
         WHEN OnDemandEventId <> '' AND RUN_MODE='RUN' THEN em.ID in (Select ID from Event_Master where State='Enabled') and (curdate() between F_Get_Std_Date(em.Start_Date_ID) and F_Get_Std_Date(em.End_Date_ID)) AND em.ID =OnDemandEventId
	 WHEN OnDemandEventId = '' AND RUN_MODE='' THEN em.ID in (Select ID from Event_Master where State='Enabled') and (curdate() between F_Get_Std_Date(em.Start_Date_ID) and F_Get_Std_Date(em.End_Date_ID)) 
      WHEN OnDemandEventId <> '' AND RUN_MODE='' THEN em.ID in (Select ID from Event_Master where State='Enabled') and (curdate() between F_Get_Std_Date(em.Start_Date_ID) and F_Get_Std_Date(em.End_Date_ID)) AND em.ID =OnDemandEventId
       WHEN OnDemandEventId = '' AND RUN_MODE='TEST_ALL' THEN em.ID in (Select ID from Event_Master where State='Enabled') and (curdate() between F_Get_Std_Date(em.Start_Date_ID) and F_Get_Std_Date(em.End_Date_ID))
        WHEN OnDemandEventId <> '' AND RUN_MODE='TEST_ALL' THEN em.ID in (Select ID from Event_Master where State='Enabled') and (curdate() between F_Get_Std_Date(em.Start_Date_ID) and F_Get_Std_Date(em.End_Date_ID)) AND em.ID =OnDemandEventId
		END
    ORDER BY em.Execution_Sequence;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;

SET done1 = FALSE;
SET SQL_SAFE_UPDATES=0;
SET foreign_key_checks=0;
SELECT CAST(REPLACE(CURRENT_DATE(), '-', '') AS UNSIGNED) INTO @Event_Execution_Date_ID;
set @vStart_Cnt = 0;set @vStart_CntCust=0; set @vEnd_Cnt=0; set @vEnd_CntCust=0; set @vBatchSizeBills=100000; set @vBatchSizeCust=100000;
insert into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
	values ('S_CLM_Event_Execution', concat(CASE WHEN RUN_MODE='' THEN 'RUN' ELSE RUN_MODE END,':',CASE WHEN OnDemandEventId='' THEN 'ALL EVENTS' ELSE OnDemandEventId END),now(),null,'Started', 090921);
select * from ETL_Execution_Details Order by Execution_Id DESC LIMIT 1;
SET Exe_ID = F_Get_Execution_Id();
SELECT Customer_id INTO @Rec_Cnt FROM V_CLM_Customer_One_View ORDER BY Customer_id DESC LIMIT 1;
SET @Rec_Cnt=ifnull(@Rec_Cnt,0);
SELECT `Value` INTO @V_Tenant_Name FROM M_Config WHERE `Name` = 'Tenant_Name';
truncate Event_Execution;
truncate Event_Execution_Stg;
truncate Event_Execution_Rejected;

Insert Into Event_Execution
(SELECT * from Event_Execution_Voucher_Pending 
where Customer_Id IN(Select Customer_Id from CDM_CLM_Vouchers where Processed_Date IS NULL));

Insert Into Event_Execution_Voucher_Pending_History
(select * from Event_Execution);

Delete from Event_Execution_Voucher_Pending
where Event_Execution_Id in (select Event_Execution_Id from Event_Execution);

UPDATE Event_Execution
SET Event_Execution_Date_ID = @Event_Execution_Date_ID;

Update CDM_CLM_Vouchers
Set Processed_Date=Current_Date()
where Customer_Id in (select Customer_Id from Event_Execution where Processed_Date is NULL);

SET done1 = FALSE;
OPEN cur1;
LOOP_ALL_EVENTS: LOOP

	FETCH cur1 into 
	  Event_ID_cur1_1, 
	  Event_Condition_SVOC_1,  
	  Event_Condition_Bill_1, 
	  Reminder_Parent_TriggerId_cur1_1,
	  Reminder_Days_cur1_1,
	  Communication_Template_ID_cur1_1, 
	  V_Offer_Code_1,
	  vEvent_Limit_1,
	  vGoodTime_1,
      vCommunication_Channel_1,
	  vRecommendation_Filter_Logic;
	 
	 SET @Event_ID_cur1=Event_ID_cur1_1;
	 SET @Event_Condition_SVOC=Event_Condition_SVOC_1;
	 SET @Event_Condition_Bill=Event_Condition_Bill_1;
	 SET @Reminder_Parent_TriggerId_cur1=Reminder_Parent_TriggerId_cur1_1;
	 SET @Reminder_Days_cur1=Reminder_Days_cur1_1;
	 SET @Communication_Template_ID_cur1=Communication_Template_ID_cur1_1;
	 SET @V_Offer_Code=V_Offer_Code_1;
	 SET @vEvent_Limit=vEvent_Limit_1;
	 SET @vGoodTime=vGoodTime_1;
     SET @vCommunication_Channel=vCommunication_Channel_1;
	IF done1 THEN
	   LEAVE LOOP_ALL_EVENTS;
	END IF; 
    truncate Event_Execution_Stg;
	set @vStart_Cnt = 0;set @vStart_CntCust=0; set @vEnd_Cnt=0; set @vEnd_CntCust=0;  
    SELECT MIN(Value) INTO @Commu_CoolOff_Days FROM M_Config WHERE `Name` = 'Communication_CoolOff_Days';
	IF @Commu_CoolOff_Days IS NULL OR @Commu_CoolOff_Days = '' OR @Commu_CoolOff_Days ="0"
	THEN
	SET @global_cooloff_condition=" 1 = 1 ";
	ELSE
	SET @global_cooloff_condition = "Customer_Id not in (select Customer_Id from Event_Execution_History 
	where F_Get_Std_Date(Event_Execution_Date_ID) > date_sub(current_date, interval @Commu_CoolOff_Days day) and Event_Id not in (select ID from Event_Master where Reminder_Parent_Event_Id > 0))";
	END IF;

	SELECT MIN(Value) INTO @vEvent_CoolOff_Days FROM M_Config WHERE `Name` = 'Event_Hold_Out_Cool_Off_Duration';
	IF @vEvent_CoolOff_Days IS NULL OR @vEvent_CoolOff_Days = '' OR @vEvent_CoolOff_Days =0
	THEN
	SET @event_cooloff_condition="  1 = 1 ";
	ELSE
	SET @event_cooloff_condition = " Customer_Id not in (select Customer_Id from Event_Execution_History 
	where Communication_Template_ID = @Communication_Template_ID_cur1 and F_Get_Std_Date(Event_Execution_Date_ID) >   date_sub(current_date, interval @vEvent_CoolOff_Days day)) ";
	END IF;
	IF @Reminder_Parent_TriggerId_cur1>0
	THEN
	SET @global_cooloff_condition = " 1 = 1 ";
    select ID into @Reminder_Parent_EvntId from Event_Master where `Name` in (select CampTriggerName from CLM_Campaign_Trigger where CampTriggerId=@Reminder_Parent_TriggerId_cur1);
	SET @reminder_condition = concat("Customer_Id in (select Customer_Id from Event_Execution_History where Event_Id= ",@Reminder_Parent_EvntId," and 
		  F_Get_Std_Date(Event_Execution_Date_ID) = DATE_SUB(curdate(), interval @Reminder_Days_cur1 day))"); 
	else
	set @reminder_condition = "  1 = 1 ";
	END IF;
	
	IF Run_Mode='TEST'  
	THEN 
	 SET @vEvent_Limit = "10";
	END IF;
	
	IF @Event_Condition_SVOC IS NULL OR @Event_Condition_SVOC = ''
	THEN
	 SET @Event_Condition_SVOC = ' 1 = 1 ';
	END IF;
    
    IF  @Event_Condition_Bill IS NULL OR @Event_Condition_Bill = ''
	THEN
	 SET @SVOT_Condition='SELECT 1 from DUAL';
	ELSE
	 SET @SVOT_Condition = CONCAT('SELECT 1 from V_CLM_Bill_Detail_One_View svot 
			WHERE  svoc.Customer_id=svot.Customer_Id  
			AND  Customer_Id between ',@vStart_Cnt,' and ',@vEnd_Cnt,'
			AND ',@Event_Condition_Bill,'');
	END IF;
    select @Event_Condition_SVOC;
     
PROCESS_EVENT_EXECUTION_SVOC_QUERY: LOOP
			SET @vEnd_CntCust = @vStart_CntCust + @vBatchSizeCust;
            SET @SQL1= concat('INSERT IGNORE 
		    INTO Event_Execution_Stg (Customer_Id, Event_Id,       Communication_Template_ID,       Offer_Code,    Event_Execution_Date_ID, Event_Execution_ID, Is_Target,Communication_Channel)
		    SELECT Customer_id, ',@Event_ID_cur1,', ',@Communication_Template_ID_cur1,', "',@V_Offer_Code,'", ',@Event_Execution_Date_ID,',','CONCAT(Customer_Id,"-",',@Event_Execution_Date_ID,',"-",',@Event_ID_cur1,'), "Y","',@vCommunication_Channel,'"','
			FROM V_CLM_Customer_One_View svoc 
			WHERE Customer_Id between ',@vStart_CntCust,' and ',@vEnd_CntCust
			,' AND ',@Event_Condition_SVOC
			,' AND ',@reminder_condition
			,' AND ',@event_cooloff_condition
			,' AND ',@global_cooloff_condition
            ,' AND EXISTS 
            ( ',@SVOT_Condition,' ) ');
            select @SQL1;
			
			IF @vStart_CntCust <= 0
            THEN
				select 'S_CLM_Event_Execution : ' as '', NOW() as '', concat (' EVENT STARTED: ',@Event_ID_cur1) as '',  '  <SQL> ' as '', @SQL1 as '',' <SQL> ' as '';
            END IF;
			select @SQL1;
            PREPARE stmt1 from @SQL1;
			EXECUTE stmt1;
			DEALLOCATE PREPARE stmt1;
            select count(1) into @eecnt from Event_Execution_Stg;
            SET @Overall_Limit_Test = @eecnt-10;
            IF RUN_MODE='TEST' and @eecnt>=15 
                        THEN
						   set @sql2=concat("UPDATE Event_Execution_Stg SET Is_Target = 'X' where Is_Target = 'Y' order by   RAND() LIMIT ",@Overall_Limit_Test,"");
	                       select @sql2;
		                   prepare stmt2 from @sql2;
		                   execute stmt2;
		                   deallocate prepare stmt2;
              DELETE from Event_Execution_Stg where Is_Target = 'X'; 
			  LEAVE PROCESS_EVENT_EXECUTION_SVOC_QUERY;
			END IF;
			IF @vEnd_CntCust >= @Rec_Cnt
            THEN
			  LEAVE PROCESS_EVENT_EXECUTION_SVOC_QUERY;
			END IF;

			SET @vStart_CntCust = @vEnd_CntCust;
END LOOP PROCESS_EVENT_EXECUTION_SVOC_QUERY;

select Offer_Type_ID into @OfferType_ID FROM
    Communication_Template
WHERE
    ID = @Communication_Template_ID_cur1;

select OfferTypeId into @External_Offer_Type_ID from CLM_OfferType where OfferTypeName = 'External_Customer_Vouchers';

if @OfferType_ID = @External_Offer_Type_ID
THEN
	
	INSERT INTO Event_Execution_Voucher_Pending
    SELECT * from Event_Execution_Stg
    where Communication_Template_ID = @Communication_Template_ID_cur1;
    SELECT 
			`Value`
		INTO @Out_FileLocation FROM
			M_Config
		WHERE
			`Name` = 'File_Path';
	select Offer_Code into @Offer from Communication_Template where ID =@Communication_Template_ID_cur1;
    SET @Offer = replace(@Offer,' ','_');
	SET @Out_FileName = concat(@Out_FileLocation,'/CLM_Event_Execution_Voucher_Pending_',@Offer,'_',date_format(Current_Date(),"%Y%m%d"),'.csv');
    select @Out_FileName;
    DELETE from Event_Execution_Stg
    where Communication_Template_ID = @Communication_Template_ID_cur1;    
    
    SET @s1 = Concat(' (Select b.Customer_Key, a.Offer_Code from Event_Execution_Voucher_Pending a JOIN CDM_Customer_Key_Lookup b ON a.Customer_Id = b.Customer_Id and a.Communication_Template_ID = ',@Communication_Template_ID_cur1,' )');
    SET @s2=concat(' SELECT "Customer_Key","Offer_Key" UNION ALL ',@s1,' INTO OUTFILE "',@Out_FileName,'" FIELDS TERMINATED BY "|" escaped by "\" LINES TERMINATED BY "\n"');
		PREPARE stmt from @s2;        
		execute stmt;
		deallocate prepare stmt;
END IF;

SELECT 
    Number_Of_Recommendation,
    Recommendation_Type,
    Exclude_Stock_out,
    Exclude_No_Image,
    Exclude_Recommended_X_Days,
    Exclude_Transaction_X_Days,
    Exclude_Never_Brought,
    Exclude_Recommended_SameLTD_FavCat
INTO @no_of_reco , @algo , @Exclude_Stock_out , @Exclude_No_Image , @Exclude_Recommended_X_Days , 
@Exclude_Transaction_X_Days , @Exclude_Never_Brought , @Exclude_Recommended_SameLTD_FavCat 
FROM
    Communication_Template
WHERE
    ID = @Communication_Template_ID_cur1;

IF ifnull(@no_of_reco,0) > 0
THEN
    select vRecommendation_Filter_Logic;
	set @vStart_Cnt = 0;set @vStart_CntCust=0; set @vEnd_Cnt=0; set @vEnd_CntCust=0; set @vBatchSizeBills=100000; set @vBatchSizeCust=100000;
    PROCESS_RCORE_USECASE: LOOP
		SET @vEnd_CntCust = @vStart_CntCust + @vBatchSizeCust;
		   IF @algo <> '"HYBRIDIZER"' 
              THEN 
				   IF @algo='"IBCF"' 
                       THEN 
                            SET @algo_condition= 'rcore.IBCF=1';
							ELSEIF  @algo='"GENOME"' THEN SET @algo_condition ='rcore.GENOME=1';
                            ELSEIF  @algo='"NN"' THEN SET @algo_condition ='rcore.NN=1';
							ELSEIF  @algo='"PREFERENCE"' THEN SET @algo_condition ='rcore.PREFERENCE=1';
							ELSEIF  @algo='"TRENDING"' THEN SET @algo_condition ='rcore.TRENDING=1';
						    ELSEIF  @algo='"TRENDINGSTLY"' THEN SET @algo_condition ='rcore.TRENDINGSTLY=1';
							ELSE SET @algo_condition = ' 1 = 1 ';
                        END IF;
				   ELSE SET @algo_condition = ' 1 = 1 ';
		END IF;   
set @Str=vRecommendation_Filter_Logic;
set @n_loop=(select length(@Str)-length(replace(@Str,'|','')))+1;
set @loop_var=1;
set @svoc_criteria='1=1';
set @bill_criteria='1=1';
while @loop_var<=@n_loop
do
set @str1=substring_index(substring_index(@Str,'|',@loop_var),'|',-1);
set @cond1= substring_index(substring_index(@str1,':::',2),':::',-1);
set @cond2= substring_index(substring_index(@str1,':::',3),':::',-1);
set @cond3= substring_index(substring_index(@str1,':::',4),':::',-1);
select @str1;
select @cond1;
select @cond2;
select @cond3;
if @cond1 in ('INCLUDE','EXCLUDE')
then 
      set @inclusion_exclusion_operator=
      case 
      when @cond1='INCLUDE' then  '='
      when @cond1='EXCLUDE' then  '<>'
      end;
set @period=@cond2;
set @attribute=@cond3;
select @inclusion_exclusion_operator;
set @svoc_attrib=concat(@period,'_',@attribute);
SET @svocattributename=replace(concat(@period,'_',@attribute,'_','LOV_Id'),'LTD','CP');
SET @productattributename=concat(replace(@attribute,'Fav_',''),'_','LOV_Id');
SELECT @productattributename;
set @svoc_criteria=concat('exists(
				select 1 from  ',case when @period='LT' then 'CDM_Customer_TP_Var' when @period='AP' then 'CDM_Customer_AP_Var'  when @period='LTD' then 'CDM_Customer_CP_Var' end ,' svoc, CDM_Product_Master pm1
where svoc.',@svocattributename,'',@inclusion_exclusion_operator,'pm1.',@productattributename,' AND
pm1.Product_Id=pm.Product_Id AND
ee.Customer_Id=svoc.Customer_Id) AND ',@svoc_criteria,'');			
end if;
if @cond1 in ('PURCHASE','NEVER_PURCHASE')
then 
      set @exists_criteria=
      case 
      when @cond1='PURCHASE' then  'exists'
      when @cond1='NEVER_PURCHASE' then  'not exists'
      end;
set @period=@cond2;	
set @attribute=@cond3;
SET @productattributename=concat(replace(@attribute,'Fav_',''),'_','LOV_Id');
select @period,@productattributename,@svocattributename;
set @bill_criteria= concat('',@exists_criteria,'( 
				select 1 from CDM_Bill_Details bills,CDM_Product_Master pm2
				where (bills.Customer_Id between ',@vStart_CntCust,' and ',@vEnd_CntCust,')
                and bills.Bill_Date > date_sub(current_date(),interval ',@period,' day)
				and bills.Product_id = pm2.Product_id
                and bills.Customer_Id = rcore.Customer_Id
                and pm2.',@productattributename,'=pm.',@productattributename,'
				 ) AND ',@bill_criteria,'');
                 
end if;
set @loop_var=@loop_var+1;
END WHILE;	
 
 		SET @SQL2=concat('update Event_Execution_Stg ee1, 
			(select Customer_Id, 
			group_concat(recogenome) recogenome,
            group_concat(Product_Id) Product_Id,
			rank1,
            Number_Of_Recommendation from (
			select
			rcore.Customer_Id as Customer_Id, 
			(pm.Product_Genome_LOV_Id) recogenome,
            pm.Product_Id Product_Id,
			dense_rank() over( partition by rcore.Customer_Id order by rcore.Hybridizer_Rank asc) rank1,
            ct.Number_Of_Recommendation as Number_Of_Recommendation
			from RankedPickList rcore, Event_Execution_Stg ee, CDM_Product_Master pm, Communication_Template ct
			where ee.Event_id = ',@Event_ID_cur1,'
			and ee.Customer_Id between ',@vStart_CntCust,' and ',@vEnd_CntCust,'
			and ee.Customer_Id=rcore.Customer_Id
			and pm.Product_Id=rcore.Product_Id 
            and ee.Communication_Template_Id = ct.ID
			-- recommedendation from a specific algo
			and ',@algo_condition,'
		   -- SVoC_Condition
            and ',@svoc_criteria,'
		   -- Bill_Condition
            and ',@bill_criteria,'
			) drvd_Tbl where rank1 <= Number_Of_Recommendation
            group by Customer_Id) temprecos
            SET ee1.Recommendation_Product_Id_6=substring_Index(substring_index(CONCAT(temprecos.Product_Id,",-1"),",",6),",",-1),
            ee1.Recommendation_Product_Id_5=substring_Index(substring_index(CONCAT(temprecos.Product_Id,",-1"),",",5),",",-1),
            ee1.Recommendation_Product_Id_4=substring_Index(substring_index(CONCAT(temprecos.Product_Id,",-1"),",",4),",",-1),
            ee1.Recommendation_Product_Id_3=substring_Index(substring_index(CONCAT(temprecos.Product_Id,",-1"),",",3),",",-1),
            ee1.Recommendation_Product_Id_2=substring_Index(substring_index(CONCAT(temprecos.Product_Id,",-1"),",",2),",",-1),
            ee1.Recommendation_Product_Id_1=substring_Index(substring_index(CONCAT(temprecos.Product_Id,",-1"),",",1),",",-1),
            ee1.Recommendation_Genome_Lov_Id_6=substring_Index(substring_index(CONCAT(temprecos.recogenome,",-1"),",",6),",",-1),
            ee1.Recommendation_Genome_Lov_Id_5=substring_Index(substring_index(CONCAT(temprecos.recogenome,",-1"),",",5),",",-1),
            ee1.Recommendation_Genome_Lov_Id_4=substring_Index(substring_index(CONCAT(temprecos.recogenome,",-1"),",",4),",",-1),
            ee1.Recommendation_Genome_Lov_Id_3=substring_Index(substring_index(CONCAT(temprecos.recogenome,",-1"),",",3),",",-1),
            ee1.Recommendation_Genome_Lov_Id_2=substring_Index(substring_index(CONCAT(temprecos.recogenome,",-1"),",",2),",",-1),
            ee1.Recommendation_Genome_Lov_Id_1=substring_Index(substring_index(CONCAT(temprecos.recogenome,",-1"),",",1),",",-1)
			where ee1.Event_id = ',@Event_ID_cur1,'
			and ee1.Customer_Id between ',@vStart_CntCust,' and ',@vEnd_CntCust,'
			and ee1.Customer_Id=temprecos.Customer_Id');
             select  @SQL2;          
			IF @vStart_CntCust <= 0
            THEN
            	select 'S_CLM_Event_Execution : ' as '', NOW() as '', concat (' RCORE STARTED FOR EVENT : ',@Event_ID_cur1) as '',  '  <RCORE><SQL> ' as '', @SQL2 as '',' </SQL></RCORE> ' as '';
           END IF;
			
            PREPARE stmt1 from @SQL2;
			EXECUTE stmt1;
			DEALLOCATE PREPARE stmt1;
            
            IF @vEnd_CntCust >= @Rec_Cnt
            THEN
			  LEAVE PROCESS_RCORE_USECASE;
			END IF;

			SET @vStart_CntCust = @vEnd_CntCust;
END LOOP PROCESS_RCORE_USECASE;

set sql_safe_updates=0;

UPDATE Event_Execution_Stg ee,Communication_Template ct
SET Is_Target = 
case when ct.Number_Of_Recommendation=1 and Recommendation_Product_Id_1 in (-1,'') then 'N'
 when ct.Number_Of_Recommendation=2 and (Recommendation_Product_Id_1 in (-1,'') or Recommendation_Product_Id_2 in (-1,'')) then 'N' 
when ct.Number_Of_Recommendation=3 and (Recommendation_Product_Id_1 in (-1,'') or Recommendation_Product_Id_2 in (-1,'') or Recommendation_Product_Id_3 in (-1,'')) then 'N'
when ct.Number_Of_Recommendation=4 and (Recommendation_Product_Id_1 in (-1,'') or Recommendation_Product_Id_2 in (-1,'') or Recommendation_Product_Id_3 in (-1,'') or Recommendation_Product_Id_4 in (-1,'')) then 'N'
when ct.Number_Of_Recommendation=5 and (Recommendation_Product_Id_1 in (-1,'') or Recommendation_Product_Id_2 in (-1,'') or Recommendation_Product_Id_3 in (-1,'') or Recommendation_Product_Id_4 in (-1,'') or Recommendation_Product_Id_5 in (-1,'')) then 'N'
when ct.Number_Of_Recommendation=6 and (Recommendation_Product_Id_1 in (-1,'') or Recommendation_Product_Id_2 in (-1,'') or Recommendation_Product_Id_3 in (-1,'') or Recommendation_Product_Id_4 in (-1,'') or Recommendation_Product_Id_5 in (-1,'') or Recommendation_Product_Id_6 in (-1,'')) then 'N'
else 'Y'
end
where ee.Communication_Template_ID=ct.ID
and Communication_Template_ID = @Communication_Template_ID_cur1;
end if;

select @vEvent_Limit;
select count(1) into @Customer_Count from Event_Execution_Stg Where Event_Id = @Event_ID_cur1;
IF @vEvent_Limit > 0 AND @vEvent_Limit <= 1
THEN
	SET @vEvent_Limit = round(@Customer_Count*@vEvent_Limit);
	SET @limit_conditions = concat( " order by GoodTime_Score desc LIMIT ",@vEvent_Limit);
ELSEIF @vEvent_Limit > 1 
THEN
	SET  @limit_conditions = concat( " order by GoodTime_Score desc LIMIT ",@vEvent_Limit);
ELSE 
	SET @limit_conditions = '';
END IF;

select @vEvent_Limit;

SET @SQL3=concat('INSERT IGNORE INTO Event_Execution
select ees.* from Event_Execution_Stg ees, V_CLM_Customer_One_View SVOC
WHERE ees.Customer_Id = SVOC.Customer_Id
and ees.Event_Id = ',@Event_ID_cur1,'
and ees.Is_Target="Y" 
',@limit_conditions
);
PREPARE stmt1 from @SQL3;
SELECT @SQL3;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;

SET @SQL4=concat('insert ignore into Event_Execution_Rejected
(SELECT * from Event_Execution_Stg ees
WHERE ees.Customer_Id not in (select Customer_Id from Event_Execution ee where Event_Id = ',@Event_ID_cur1,' and Event_Execution_Date_ID = ',@Event_Execution_Date_ID,'))'
);
PREPARE stmt1 from @SQL4;
SELECT @SQL4;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;
UPDATE Event_Execution_Rejected 
SET Message=concat(@Event_ID_cur1,' : Event Limit Exceeded')
WHERE Message is null and Is_Target <>'N';
UPDATE Event_Execution_Rejected 
SET Message=concat(@Event_ID_cur1,' : Reccomendation Not Available')
WHERE Message is null and Is_Target ='N';
    
END LOOP LOOP_ALL_EVENTS;	
close cur1;


UPDATE Event_Execution ee, Hold_Out ho 
SET ee.In_Control_Group = "Y",ee.Is_Target = "Y" 
WHERE ee.Customer_Id = ho.Customer_Id ;

CALL S_CLM_Exclusion_Filter_Execution; 

SELECT 
    `Value`
INTO @Overall_Limit FROM
    M_Config
WHERE
    `Name` = 'CLM_Event_Limit';
select count(1) into @Customers_Count from Event_Execution where Is_Target = 'Y';
if ifnull(@Overall_Limit,-1)>0 and ifnull(@Customers_Count,0) > @Overall_Limit
then
	SET @Overall_Limit = @Customers_Count-@Overall_Limit;
	set @sql2=concat("UPDATE Event_Execution SET Is_Target = 'X' where Is_Target = 'Y' order by RAND() LIMIT ",@Overall_Limit,"");
	select @sql2;
		prepare stmt2 from @sql2;
		execute stmt2;
		deallocate prepare stmt2;
	insert ignore into Event_Execution_Rejected
    select * from Event_Execution Where Is_Target = 'X';
    UPDATE Event_Execution_Rejected 
	SET Message=concat(@Event_ID_cur1,' : Global Limit Exceeded')
	WHERE Message is null and Is_Target ='X';
    DELETE from Event_Execution where Is_Target = 'X';
end if;


SELECT COUNT(1) INTO @vCustomer_Count FROM Event_Execution ;
If  @vCustomer_Count =0 
Then
    SET @vSuccess = 1;
	SET @vErrMsg = CONCAT('\r\n','Event Created but not tested');
ELSE
	SET @vSuccess = 1;
	SET @vErrMsg = '';
End if;
SELECT @vCustomer_Count INTO no_of_customers;
SELECT @vSuccess INTO vSuccess;
SELECT @vErrMsg INTO vErrMsg;

UPDATE ETL_Execution_Details SET Status = 'Succeeded', End_Time = NOW() WHERE Procedure_Name = 'S_CLM_Event_Execution' AND Execution_ID = Exe_ID;       
select 'S_CLM_Event_Execution : COMPLETED ' as '',  NOW() as '', 'No of Customers : ' as '', @vCustomer_Count as '' , ' Message :' as '', 
@vErrMsg as '';
END

