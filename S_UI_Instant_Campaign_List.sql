
CREATE or replace PROCEDURE `S_UI_Instant_Campaign_List`(IN in_request_json json, OUT out_result_json json)
BEGIN
    
    

    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
    declare EDIT Bigint; 
    

declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare IN_AGGREGATION_4 text;
	declare IN_AGGREGATION_5 text;
	
	SET SQL_SAFE_UPDATES=0;
	SET foreign_key_checks=0;

	Set in_request_json = Replace(in_request_json,'\n','\\n');
  
   
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1")); 
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    
	Set @temp_result =Null;
    Set @vSuccess ='';
	Set @vErrMsg='';
	Set @vSuccess='';
	Set @Err =0;
	
	if IN_AGGREGATION_1 = 'ReadytoSend_List'
    then 
	SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Trigger_Name",Trigger_Name,"Channel",a.Channel,"Creative",replace(Creative,"null",""),"LifeCycle_Segment",LifeCycle_Segment,"Campaign_Segment",Campaign_Segment,"Throttling",Throttling,"Files_Created_On",date_format(Files_Created_On, "%d/%m %h:%i"),"Target_Count",Target_Count,"Control_Count",Control_Count,"Campaign_Sent_On",Campaign_Sent_On,"Status",a.Status,"Provider_Name",Provider_Name)order by a.Modified_Date desc)),"]")
				    into @temp_result from Event_Master_Instant_Campaign a, Downstream_Adapter_Details b where a.Channel = b.Channel and a.Status not in ("Live_Sent","Sending_Live")  and Files_Created_On is not NULL and In_Use = "Enable" ;') ;
				      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
		
        
	end if;
    
    if IN_AGGREGATION_1 = 'Sent_List'
    then 
	SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Trigger_Name",Trigger_Name,"Channel",a.Channel,"Creative",replace(Creative,"null",""),"LifeCycle_Segment",LifeCycle_Segment,"Campaign_Segment",Campaign_Segment,"Throttling",Throttling,"Files_Created_On",date_format(Files_Created_On, "%d/%m %h:%i"),"Target_Count",Target_Count,"Control_Count",Control_Count,"Campaign_Sent_On",Campaign_Sent_On,"Status",a.Status,"Provider_Name",Provider_Name,"Created_By",Created_By)order by a.Modified_Date desc)),"]")
				    into @temp_result from Event_Master_Instant_Campaign a, Downstream_Adapter_Details b where a.Channel = b.Channel and a.Status in ("Live_Sent") ;') ;
				      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
		
        
        
        
	end if;
    
    if IN_AGGREGATION_1 = 'Funnel_List'
    then 
	SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Trigger_Name",Trigger_Name,"Channel",a.Channel,"Creative",replace(Creative,"null",""),"LifeCycle_Segment",LifeCycle_Segment,"Campaign_Segment",Campaign_Segment,"Throttling",Throttling,"Files_Created_On",date_format(Files_Created_On, "%i:%h %d/%m"),"Target_Count",Target_Count,"Control_Count",Control_Count,"Campaign_Sent_On",Campaign_Sent_On,"Status",a.Status,"Provider_Name",Provider_Name)order by a.Modified_Date desc )),"]")
				    into @temp_result from Event_Master_Instant_Campaign a, Downstream_Adapter_Details b where a.Channel = b.Channel and a.Status in ("Sent","Sending_Live") ;') ;
				      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
		
        
        
        
	end if;
    
    if IN_AGGREGATION_1 = 'Header_list'
    then 
	SET @Query_String = concat('select CONCAT("[",
(GROUP_CONCAT(json_object("header","Mobile" )) ),","
,(GROUP_CONCAT(json_object("header","Email" )) ),","
,(GROUP_CONCAT(json_object("header","First_Name" )) ),","
,(GROUP_CONCAT(json_object("header","Customer_Key" )) ),","
,(GROUP_CONCAT(json_object("header","Message" )) ),","
,(GROUP_CONCAT(json_object("header","Communication_Channel" )) ),","
,(GROUP_CONCAT(json_object("header","Event_Execution_Id" )) ),","
,(GROUP_CONCAT(json_object("header","Event_Id" )) )
,"]")  
				    into @temp_result  ;') ;
				      
					SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
		
        
        
        
	end if;
    
    
    if IN_AGGREGATION_1 = 'Default_Customer'
    then
    select `Value` into @field1 from M_Config where `Name` = 'Customer_Valid_Name_Default';
    
    Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Name",','"',@field1,'"',') )),"]") 
														into @temp_result                       
												      ;') ;
                                                      SELECT @Query_String;
					PREPARE statement from @Query_String;
					Execute statement;
					Deallocate PREPARE statement; 
		
                                                      
		end if;
                                                      
    
    if @temp_result is null
		Then
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") 
														into @temp_result                       
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	End if;
							
    SET out_result_json = @temp_result;


END


