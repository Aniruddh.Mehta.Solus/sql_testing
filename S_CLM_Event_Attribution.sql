
CREATE or replace PROCEDURE `S_CLM_Event_Attribution`(IN vBatchSize BIGINT)
BEGIN
DECLARE Load_Exe_ID, Exe_ID,Load_Execution_ID int;
DECLARE Loop_Check_Var Int Default 0;
DECLARE V_Event_ID, V_Event_Execution_Date_ID,V_Event_Sent_Date_ID, V_Event_Response_Date_ID int ;
DECLARE V_Event_Execution_ID Varchar(20);
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT;
DECLARE vStart_Cnt_Init BIGINT DEFAULT 0;
DECLARE vResponseDays int;
DECLARE vMax_Response_Window int default 8;
DECLARE Num_of_Attr INT DEFAULT 0;	
DECLARE V_Current_Execution_Date date default DATE_SUB(Current_Date(), INTERVAL 
(select ifnull(Value,1)  from CLM_Response_Config where Name='Response_LTD') DAY);
DECLARE V_Current_Execution_Date_Id int(11) default date_format(DATE_SUB(Current_Date(),INTERVAL 
(select ifnull(Value,11) from CLM_Response_Config where Name='Response_LTD') DAY),'%Y%m%d');


DECLARE V_Current_Execution_Date_Id_End int(11) default date_format(DATE_SUB(CURRENT_DATE(),
	INTERVAL (SELECT 
			ifnull(Value,10) 
		FROM
			CLM_Response_Config
		WHERE
			name = 'Response_FTD')  DAY) ,'%Y%m%d');
DECLARE curALL_EVENTS CURSOR For SELECT DISTINCT
    Event_ID,
    Event_Execution_Date_ID
FROM
    Event_Execution_History eeh
WHERE Customer_Id between vStart_Cnt and vEnd_Cnt
AND Event_Execution_Date_ID=V_Current_Execution_Date_Id
ORDER BY Created_Date DESC;


DECLARE CONTINUE HANDLER FOR NOT FOUND SET Loop_Check_Var=1;
DECLARE CONTINUE HANDLER FOR SQLWARNING
BEGIN
	GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
	SET @logmsg=CONCAT(ifnull(@errsqlstate,'WARNING') , ifnull(@errno,'1'), ifnull(@errtext,'WARNING MESSAGE'));
    INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
		values ('S_CLM_Event_Attribution', @logmsg ,now(),null,@errno, ifnull(Load_Exe_ID,1));
    SELECT 'S_CLM_Event_Attribution : Warning Message :' as '', @logmsg as '';
END;
DECLARE EXIT HANDLER FOR SQLEXCEPTION
BEGIN
	GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
	SET @vErrMsg = CONCAT(@errno, " (", @sqlstate, "): ", @text);
	INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
			values ('S_CLM_Event_Attribution ', @vErrMsg ,now(),null,'ERROR', Load_Exe_ID);
	select now() as '', @vErrMsg as '' ;
END;
SET SQL_SAFE_UPDATES=0;
SET FOREIGN_KEY_CHECKS=0;
SET Load_Exe_ID = F_Get_Load_Execution_Id();
SET Exe_ID = F_Get_Execution_Id();
SET Load_Execution_ID = 1;
INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
		values ('S_CLM_Event_Attribution ', ' ' ,now(),null,'Started', Load_Exe_ID);
SELECT 'S_CLM_Event_Attribution : STARTED ' as '', NOW() as '' ;



select ifnull(Value,10) into vResponseDays from CLM_Response_Config where Name='Response_FTD'; 
set vMax_Response_Window=(select(SELECT max(Event_Response_Days) FROM Event_Master) + 
		(select ifnull(Value,1) from CLM_Response_Config where Name='Campaign_Execution_Delay'));
SELECT Customer_Id INTO vStart_Cnt_Init FROM CDM_Bill_Details ORDER BY Customer_Id ASC LIMIT 1;
SELECT Customer_Id INTO @Rec_Cnt FROM CDM_Bill_Details ORDER BY Customer_Id DESC LIMIT 1;


LOOP_ALL_DATES: LOOP
	IF V_Current_Execution_Date_Id<V_Current_Execution_Date_Id_End
	THEN
		LEAVE LOOP_ALL_DATES;
	END IF;
	SET vStart_Cnt=0;
	
	SELECT 'S_CLM_Event_Attribution :Attribute ' as '', V_Current_Execution_Date_Id as '', V_Current_Execution_Date as '' ,NOW() as '' ; 
	INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
		values ('S_CLM_Event_Attribution ', V_Current_Execution_Date_Id,now(),null,'Attribute', Load_Exe_ID);
	
	TRUNCATE Event_Execution_History_1DAY;
    
	
	LOOP_ALL_CUSTOMERS: LOOP
	    select count(1) into @NO_Campaigns from Event_Execution_History where Event_Execution_Date_ID=V_Current_Execution_Date_Id;
		IF @NO_Campaigns=0
	    THEN 
			     LEAVE LOOP_ALL_CUSTOMERS;
		END IF;
		SET vEnd_Cnt = vStart_Cnt + vBatchSize-1;
		
		SELECT 'S_CLM_Event_Attribution :' as '', concat(V_Current_Execution_Date_Id,'_',vEnd_Cnt) as '', NOW() as '' ; 
		INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
			values ('S_CLM_Event_Attribution ', concat(V_Current_Execution_Date_Id,'_',vEnd_Cnt),now(),null,'Customer', Load_Exe_ID);
			
		SELECT `Value` into @Excluded_Stores from CLM_Response_Config where `Name` = 'Excluded_Store_Ids';
        if @Excluded_Stores is NULL or @Excluded_Stores = -1 or @Excluded_Stores = '' 
        THEN
			SET @Store_Exclusion_Condition = ' 1 = 1 ';
		ELSE 
			SET @Store_Exclusion_Condition = concat(' Store_Id not in (',@Excluded_Stores,') ');
		END IF;
        
        SELECT @Store_Exclusion_Condition;
		
		TRUNCATE CDM_Bill_Details_ResponseWindow;
        
		SET @SQL1=CONCAT('INSERT IGNORE into CDM_Bill_Details_ResponseWindow (  
		Customer_Id,
		Bill_Details_Id,
		Bill_Header_Id,
		Bill_Date,
		Product_Id,
		Sale_Net_Val,
		Sale_Disc_Val,
		Sale_Qty) 
		SELECT 
		Customer_Id,
		Bill_Details_Id,
		Bill_Header_Id,
		Bill_Date,
		Product_Id,
		Sale_Net_Val,
		Sale_Disc_Val,
		Sale_Qty
		FROM
			CDM_Bill_Details bills
		WHERE
		bills.Customer_Id BETWEEN ',vStart_Cnt,' AND ',vEnd_Cnt,'
		AND bills.Bill_Date BETWEEN "',V_Current_Execution_Date,'" AND DATE_ADD("',V_Current_Execution_Date,'", INTERVAL ',vMax_Response_Window,' DAY)
        AND ',@Store_Exclusion_Condition,'');
        SELECT @SQL1;
        
        prepare stmt from @SQL1;
		execute stmt;
		deallocate prepare stmt;
		
		INSERT IGNORE INTO `Event_Execution_History_1DAY`
		(`Event_Execution_ID`,
		`Event_ID`,
		`Event_Execution_Date_ID`,
		`Customer_Id`,
		`Communication_Template_ID`,
		`Communication_Channel`,
		`In_Control_Group`,
		`In_Event_Control_Group`,
		`Segment_Id`,
		`Revenue_Center`,
        `Recommendation_Product_Id_1`,
		`Recommendation_Product_Id_2`,
		`Recommendation_Product_Id_3`,
		`Recommendation_Product_Id_4`,
		`Recommendation_Product_Id_5`,
		`Recommendation_Product_Id_6`)
		SELECT 
		`Event_Execution_ID`,
		`Event_ID`,
		`Event_Execution_Date_ID`,
		`Customer_Id`,
		`Communication_Template_ID`,
		`Communication_Channel`,
		`In_Control_Group`,
		`In_Event_Control_Group`,
		`Segment_Id`,
		`Revenue_Center`,
        `Recommendation_Product_Id_1`,
		`Recommendation_Product_Id_2`,
		`Recommendation_Product_Id_3`,
		`Recommendation_Product_Id_4`,
		`Recommendation_Product_Id_5`,
		`Recommendation_Product_Id_6`
		FROM Event_Execution_History where Customer_Id between vStart_Cnt and vEnd_Cnt 
		AND Event_Execution_Date_ID=V_Current_Execution_Date_Id;
        
		UPDATE Event_Execution_History_1DAY eeh, CDM_Delivery_Report dr
		SET 
         eeh.Delivery_Status=dr.Delivery_Status,
		 eeh.Delivery_Date=dr.Delivery_Date,
		 eeh.CTA_Status=dr.CTA_Status,
		 eeh.CTA_Date=dr.CTA_Date,
		 eeh.Open_Status=dr.Open_Status,
		 eeh.Open_Date=dr.Open_Date
		WHERE  eeh.Customer_Id between vStart_Cnt and vEnd_Cnt 
        AND eeh.Customer_Id=dr.Customer_Id
		AND eeh.Event_Execution_ID=dr.Event_Execution_ID; 

		
		DELETE FROM Event_Attribution_History 
		WHERE Event_Execution_Date_ID = V_Current_Execution_Date_ID
		AND Customer_Id BETWEEN vStart_Cnt AND vEnd_Cnt;
        
		SET Loop_Check_Var=0;
		OPEN curALL_EVENTS;
		LOOP_ALL_EVENTS : Loop
			FETCH curALL_EVENTS into V_Event_ID,V_Event_Execution_Date_ID;
			IF Loop_Check_Var THEN    
				LEAVE LOOP_ALL_EVENTS;
			END IF;
			INSERT IGNORE into Event_Attribution_History (
				`Customer_Id` ,
				`Bill_Details_ID` ,
				`Bill_Header_ID` ,
				`Bill_Date` ,
				`Product_Id`,
				`Sale_Net_Val`,
				`Sale_Disc_Val`,
				`Sale_Qty`,
				`Event_Execution_ID` ,
				`Event_ID` ,
				`Event_Execution_Date_ID`,
				`In_Control_Group`,
				`In_Event_Control_Group`
				 )
				SELECT 
				bills.`Customer_Id` ,
				bills.`Bill_Details_ID` ,
				bills.`Bill_Header_ID` ,
				bills.`Bill_Date`,
				bills.`Product_Id`,
				bills.`Sale_Net_Val`,
				bills.`Sale_Disc_Val`,
				bills.`Sale_Qty`,
				campaigns.`Event_Execution_ID` ,
				campaigns.`Event_ID` ,
				campaigns.`Event_Execution_Date_ID`,
				campaigns.`In_Control_Group`,
				campaigns.`In_Event_Control_Group`
			FROM Event_Execution_History_1DAY campaigns, CDM_Bill_Details_ResponseWindow bills 
			WHERE campaigns.Customer_Id between vStart_Cnt and vEnd_Cnt
			AND campaigns.Event_Execution_Date_ID=V_Event_Execution_Date_ID
			AND campaigns.Event_Id=V_Event_ID
            AND (campaigns.Delivery_Status is null or campaigns.Delivery_Status <> 'N')
			AND bills.Customer_Id=campaigns.Customer_Id
			AND bills.Bill_date 
				between date_add(V_Event_Execution_Date_ID,INTERVAL (select ifnull(Value,1) from CLM_Response_Config where Name='Campaign_Execution_Delay') DAY )
				AND date_add(V_Event_Execution_Date_ID,INTERVAL ((select ifnull(Value,1) from CLM_Response_Config where Name='Campaign_Execution_Delay') + 
				(SELECT Event_Response_Days FROM Event_Master WHERE Event_Master.ID = V_Event_ID) ) DAY );
            SET @vStart_Cnt=vStart_Cnt;
			SET @vEnd_Cnt=vEnd_Cnt;
			SET @V_Current_Execution_Date_Id=V_Current_Execution_Date_Id;
			SET @V_Event_ID=V_Event_ID;
            SET @V_Event_Execution_Date_ID=V_Event_Execution_Date_ID;
            SET @Check1=(SELECT `Value` from CLM_Response_Config where `Name` = 'Calculate_Hard_Response');
            SET @Check1=IFNULL(@Check1,'N');
            SET @Num_of_Recos=(select Number_Of_Recommendation from Communication_Template where ID=(SELECT Communication_Template_ID from Event_Master where ID=@V_Event_ID));
            SELECT @Num_of_Recos,@vStart_Cnt,@vEnd_Cnt,@V_Current_Execution_Date_Id,@V_Event_Execution_Date_ID,@V_Event_ID;
			IF @Num_of_Recos > 0 AND @Check1 = 'Y'
            THEN
				update Event_Attribution_History eah, CDM_Bill_Details_ResponseWindow bills
				SET eah.Matched_ProductId = 1
				WHERE eah.Event_Execution_Date_Id = @V_Current_Execution_Date_Id
				AND eah.Customer_Id between @vStart_Cnt and @vEnd_Cnt
				AND eah.Event_Execution_Date_ID=@V_Event_Execution_Date_ID
				AND eah.Event_Id=@V_Event_ID
				AND eah.Bill_Details_ID=bills.Bill_Details_ID
				AND bills.Product_Id in 
				((SELECT IFNULL((select Recommendation_Product_Id_1 from Event_Execution_History_1DAY eeh1  where eeh1.Event_Execution_Id=eah.Event_Execution_Id),-1))
				,
				(SELECT IFNULL((SELECT Recommendation_Product_Id_2 from Event_Execution_History_1DAY eeh2  where eeh2.Event_Execution_Id=eah.Event_Execution_Id),-1))
				,
				(SELECT IFNULL((SELECT Recommendation_Product_Id_3 from Event_Execution_History_1DAY eeh3  where eeh3.Event_Execution_Id=eah.Event_Execution_Id),-1))
				,
				(SELECT IFNULL((SELECT Recommendation_Product_Id_4 from Event_Execution_History_1DAY eeh4  where eeh4.Event_Execution_Id=eah.Event_Execution_Id),-1))
				,
				(SELECT IFNULL((SELECT Recommendation_Product_Id_5 from Event_Execution_History_1DAY eeh5  where eeh5.Event_Execution_Id=eah.Event_Execution_Id),-1))
				,
				(SELECT IFNULL((SELECT Recommendation_Product_Id_6 from Event_Execution_History_1DAY eeh6  where eeh6.Event_Execution_Id=eah.Event_Execution_Id),-1)));

				SET @Num_of_Attr=(select (char_length(Product_Genome)-char_length(replace(Product_Genome,'___','__')))+1 from CDM_Product_Master where Product_Genome <> '' and Product_Genome is not null LIMIT 1);
                SELECT @Num_of_Attr;

				IF @Num_of_Attr>=1
				THEN
					update Event_Attribution_History eah, CDM_Bill_Details_ResponseWindow bills, CDM_Product_Master pm
							SET eah.Matched_ProductGenome_Percent = Matched_ProductGenome_Percent+(1/@Num_of_Attr),
							eah.Matched_ProductGenome=1
							WHERE eah.Event_Execution_Date_Id = @V_Current_Execution_Date_Id
							AND eah.Customer_Id between @vStart_Cnt and @vEnd_Cnt
							AND eah.Event_Execution_Date_ID=@V_Event_Execution_Date_ID
							AND eah.Event_Id=@V_Event_ID
							AND eah.Bill_Details_ID=bills.Bill_Details_ID
							AND pm.Product_Id=bills.Product_Id
							AND @Num_of_Attr>1
							AND substring_Index(substring_index(pm.Product_Genome,"___",1),"___",-1) in 
							((SELECT IFNULL((select substring_Index(substring_index(pm1.Product_Genome,"___",1),"___",-1) from Event_Execution_History_1DAY eeh1, CDM_Product_Master pm1 where eeh1.Event_Id=@V_Event_ID AND eeh1.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh1.Recommendation_Product_Id_1=pm1.Product_Id and eeh1.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm2.Product_Genome,"___",1),"___",-1) from Event_Execution_History_1DAY eeh2, CDM_Product_Master pm2  where eeh2.Event_Id=@V_Event_ID AND eeh2.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh2.Recommendation_Product_Id_2=pm2.Product_Id and eeh2.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm3.Product_Genome,"___",1),"___",-1) from Event_Execution_History_1DAY eeh3, CDM_Product_Master pm3  where eeh3.Event_Id=@V_Event_ID AND eeh3.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh3.Recommendation_Product_Id_3=pm3.Product_Id and eeh3.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm4.Product_Genome,"___",1),"___",-1) from Event_Execution_History_1DAY eeh4, CDM_Product_Master pm4  where eeh4.Event_Id=@V_Event_ID AND eeh4.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh4.Recommendation_Product_Id_4=pm4.Product_Id and eeh4.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm5.Product_Genome,"___",1),"___",-1) from Event_Execution_History_1DAY eeh5, CDM_Product_Master pm5  where eeh5.Event_Id=@V_Event_ID AND eeh5.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh5.Recommendation_Product_Id_5=pm5.Product_Id and eeh5.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm6.Product_Genome,"___",1),"___",-1) from Event_Execution_History_1DAY eeh6, CDM_Product_Master pm6 where eeh6.Event_Id=@V_Event_ID AND eeh6.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh6.Recommendation_Product_Id_6=pm6.Product_Id and eeh6.Event_Execution_Id=eah.Event_Execution_Id),-1)));

				END IF;
				IF @Num_of_Attr>=2
				THEN
					update Event_Attribution_History eah, CDM_Bill_Details_ResponseWindow bills, CDM_Product_Master pm
							SET eah.Matched_ProductGenome_Percent = Matched_ProductGenome_Percent+(1/@Num_of_Attr),
							eah.Matched_ProductGenome=concat(eah.Matched_ProductGenome,1)
							WHERE eah.Event_Execution_Date_Id = @V_Current_Execution_Date_Id
							AND eah.Customer_Id between @vStart_Cnt and @vEnd_Cnt
							AND eah.Event_Execution_Date_ID=@V_Event_Execution_Date_ID
							AND eah.Event_Id=@V_Event_ID
							AND eah.Bill_Details_ID=bills.Bill_Details_ID
							AND pm.Product_Id=bills.Product_Id
							AND @Num_of_Attr>1
							AND substring_Index(substring_index(pm.Product_Genome,"___",2),"___",-1) in 
							((SELECT IFNULL((select substring_Index(substring_index(pm1.Product_Genome,"___",2),"___",-1) from Event_Execution_History_1DAY eeh1, CDM_Product_Master pm1  where eeh1.Event_Id=@V_Event_ID AND eeh1.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh1.Recommendation_Product_Id_1=pm1.Product_Id and eeh1.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm2.Product_Genome,"___",2),"___",-1) from Event_Execution_History_1DAY eeh2, CDM_Product_Master pm2  where eeh2.Event_Id=@V_Event_ID AND eeh2.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh2.Recommendation_Product_Id_2=pm2.Product_Id and eeh2.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm3.Product_Genome,"___",2),"___",-1) from Event_Execution_History_1DAY eeh3, CDM_Product_Master pm3  where eeh3.Event_Id=@V_Event_ID AND eeh3.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh3.Recommendation_Product_Id_3=pm3.Product_Id and eeh3.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm4.Product_Genome,"___",2),"___",-1) from Event_Execution_History_1DAY eeh4, CDM_Product_Master pm4  where eeh4.Event_Id=@V_Event_ID AND eeh4.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh4.Recommendation_Product_Id_4=pm4.Product_Id and eeh4.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm5.Product_Genome,"___",2),"___",-1) from Event_Execution_History_1DAY eeh5, CDM_Product_Master pm5  where eeh5.Event_Id=@V_Event_ID AND eeh5.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh5.Recommendation_Product_Id_5=pm5.Product_Id and eeh5.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm6.Product_Genome,"___",2),"___",-1) from Event_Execution_History_1DAY eeh6, CDM_Product_Master pm6 where eeh6.Event_Id=@V_Event_ID AND eeh6.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh6.Recommendation_Product_Id_6=pm6.Product_Id and eeh6.Event_Execution_Id=eah.Event_Execution_Id),-1)));

					UPDATE Event_Attribution_History eah
					SET eah.Matched_ProductGenome=CONCAT(eah.Matched_ProductGenome,0)
					WHERE eah.Customer_Id between @vStart_Cnt and @vEnd_Cnt
					AND eah.Event_Execution_Date_ID=@V_Event_Execution_Date_ID
					AND eah.Event_Id=@V_Event_ID
					AND LENGTH(eah.Matched_ProductGenome)=1;
				END IF;

				IF @Num_of_Attr>=3
				THEN        
					update Event_Attribution_History eah, CDM_Bill_Details_ResponseWindow bills, CDM_Product_Master pm
							SET eah.Matched_ProductGenome_Percent = Matched_ProductGenome_Percent+(1/@Num_of_Attr),
							eah.Matched_ProductGenome=concat(eah.Matched_ProductGenome,1)
							WHERE eah.Event_Execution_Date_Id = @V_Current_Execution_Date_Id
							AND eah.Customer_Id between @vStart_Cnt and @vEnd_Cnt
							AND eah.Event_Execution_Date_ID=@V_Event_Execution_Date_ID
							AND eah.Event_Id=@V_Event_ID
							AND eah.Bill_Details_ID=bills.Bill_Details_ID
							AND pm.Product_Id=bills.Product_Id
							AND @Num_of_Attr>1
							AND substring_Index(substring_index(pm.Product_Genome,"___",3),"___",-1) in 
							((SELECT IFNULL((select substring_Index(substring_index(pm1.Product_Genome,"___",3),"___",-1) from Event_Execution_History_1DAY eeh1, CDM_Product_Master pm1  where eeh1.Event_Id=@V_Event_ID AND eeh1.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh1.Recommendation_Product_Id_1=pm1.Product_Id and eeh1.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm2.Product_Genome,"___",3),"___",-1) from Event_Execution_History_1DAY eeh2, CDM_Product_Master pm2  where eeh2.Event_Id=@V_Event_ID AND eeh2.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh2.Recommendation_Product_Id_2=pm2.Product_Id and eeh2.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm3.Product_Genome,"___",3),"___",-1) from Event_Execution_History_1DAY eeh3, CDM_Product_Master pm3  where eeh3.Event_Id=@V_Event_ID AND eeh3.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh3.Recommendation_Product_Id_3=pm3.Product_Id and eeh3.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm4.Product_Genome,"___",3),"___",-1) from Event_Execution_History_1DAY eeh4, CDM_Product_Master pm4  where eeh4.Event_Id=@V_Event_ID AND eeh4.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh4.Recommendation_Product_Id_4=pm4.Product_Id and eeh4.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm5.Product_Genome,"___",3),"___",-1) from Event_Execution_History_1DAY eeh5, CDM_Product_Master pm5  where eeh5.Event_Id=@V_Event_ID AND eeh5.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh5.Recommendation_Product_Id_5=pm5.Product_Id and eeh5.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm6.Product_Genome,"___",3),"___",-1) from Event_Execution_History_1DAY eeh6, CDM_Product_Master pm6 where eeh6.Event_Id=@V_Event_ID AND eeh6.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh6.Recommendation_Product_Id_6=pm6.Product_Id and eeh6.Event_Execution_Id=eah.Event_Execution_Id),-1)));

					UPDATE Event_Attribution_History eah
					SET eah.Matched_ProductGenome=CONCAT(eah.Matched_ProductGenome,0)
					WHERE eah.Customer_Id between @vStart_Cnt and @vEnd_Cnt
					AND eah.Event_Execution_Date_ID=@V_Event_Execution_Date_ID
					AND eah.Event_Id=@V_Event_ID
					AND LENGTH(eah.Matched_ProductGenome)=2;
				END IF;

				IF @Num_of_Attr>=4
				THEN
					update Event_Attribution_History eah, CDM_Bill_Details_ResponseWindow bills, CDM_Product_Master pm
							SET eah.Matched_ProductGenome_Percent = Matched_ProductGenome_Percent+(1/@Num_of_Attr),
							eah.Matched_ProductGenome=concat(eah.Matched_ProductGenome,1)
							WHERE eah.Event_Execution_Date_Id = @V_Current_Execution_Date_Id
							AND eah.Customer_Id between @vStart_Cnt and @vEnd_Cnt
							AND eah.Event_Execution_Date_ID=@V_Event_Execution_Date_ID
							AND eah.Event_Id=@V_Event_ID
							AND eah.Bill_Details_ID=bills.Bill_Details_ID
							AND pm.Product_Id=bills.Product_Id
							AND @Num_of_Attr>1
							AND substring_Index(substring_index(pm.Product_Genome,"___",4),"___",-1) in 
							((SELECT IFNULL((select substring_Index(substring_index(pm1.Product_Genome,"___",4),"___",-1) from Event_Execution_History_1DAY eeh1, CDM_Product_Master pm1  where eeh1.Event_Id=@V_Event_ID AND eeh1.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh1.Recommendation_Product_Id_1=pm1.Product_Id and eeh1.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm2.Product_Genome,"___",4),"___",-1) from Event_Execution_History_1DAY eeh2, CDM_Product_Master pm2  where eeh2.Event_Id=@V_Event_ID AND eeh2.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh2.Recommendation_Product_Id_2=pm2.Product_Id and eeh2.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm3.Product_Genome,"___",4),"___",-1) from Event_Execution_History_1DAY eeh3, CDM_Product_Master pm3  where eeh3.Event_Id=@V_Event_ID AND eeh3.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh3.Recommendation_Product_Id_3=pm3.Product_Id and eeh3.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm4.Product_Genome,"___",4),"___",-1) from Event_Execution_History_1DAY eeh4, CDM_Product_Master pm4  where eeh4.Event_Id=@V_Event_ID AND eeh4.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh4.Recommendation_Product_Id_4=pm4.Product_Id and eeh4.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm5.Product_Genome,"___",4),"___",-1) from Event_Execution_History_1DAY eeh5, CDM_Product_Master pm5  where eeh5.Event_Id=@V_Event_ID AND eeh5.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh5.Recommendation_Product_Id_5=pm5.Product_Id and eeh5.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm6.Product_Genome,"___",4),"___",-1) from Event_Execution_History_1DAY eeh6, CDM_Product_Master pm6 where eeh6.Event_Id=@V_Event_ID AND eeh6.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh6.Recommendation_Product_Id_6=pm6.Product_Id and eeh6.Event_Execution_Id=eah.Event_Execution_Id),-1)));

					UPDATE Event_Attribution_History eah
					SET eah.Matched_ProductGenome=CONCAT(eah.Matched_ProductGenome,0)
					WHERE eah.Customer_Id between @vStart_Cnt and @vEnd_Cnt
					AND eah.Event_Execution_Date_ID=@V_Event_Execution_Date_ID
					AND eah.Event_Id=@V_Event_ID
					AND LENGTH(eah.Matched_ProductGenome)=3;
				END IF;

				IF @Num_of_Attr>=5
				THEN
					update Event_Attribution_History eah, CDM_Bill_Details_ResponseWindow bills, CDM_Product_Master pm
							SET eah.Matched_ProductGenome_Percent = Matched_ProductGenome_Percent+(1/@Num_of_Attr),
							eah.Matched_ProductGenome=concat(eah.Matched_ProductGenome,1)
							WHERE eah.Event_Execution_Date_Id = @V_Current_Execution_Date_Id
							AND eah.Customer_Id between @vStart_Cnt and @vEnd_Cnt
							AND eah.Event_Execution_Date_ID=@V_Event_Execution_Date_ID
							AND eah.Event_Id=@V_Event_ID
							AND eah.Bill_Details_ID=bills.Bill_Details_ID
							AND pm.Product_Id=bills.Product_Id
							AND @Num_of_Attr>1
							AND substring_Index(substring_index(pm.Product_Genome,"___",5),"___",-1) in 
							((SELECT IFNULL((select substring_Index(substring_index(pm1.Product_Genome,"___",5),"___",-1) from Event_Execution_History_1DAY eeh1, CDM_Product_Master pm1  where eeh1.Event_Id=@V_Event_ID AND eeh1.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh1.Recommendation_Product_Id_1=pm1.Product_Id and eeh1.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm2.Product_Genome,"___",5),"___",-1) from Event_Execution_History_1DAY eeh2, CDM_Product_Master pm2  where eeh2.Event_Id=@V_Event_ID AND eeh2.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh2.Recommendation_Product_Id_2=pm2.Product_Id and eeh2.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm3.Product_Genome,"___",5),"___",-1) from Event_Execution_History_1DAY eeh3, CDM_Product_Master pm3  where eeh3.Event_Id=@V_Event_ID AND eeh3.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh3.Recommendation_Product_Id_3=pm3.Product_Id and eeh3.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm4.Product_Genome,"___",5),"___",-1) from Event_Execution_History_1DAY eeh4, CDM_Product_Master pm4  where eeh4.Event_Id=@V_Event_ID AND eeh4.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh4.Recommendation_Product_Id_4=pm4.Product_Id and eeh4.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm5.Product_Genome,"___",5),"___",-1) from Event_Execution_History_1DAY eeh5, CDM_Product_Master pm5  where eeh5.Event_Id=@V_Event_ID AND eeh5.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh5.Recommendation_Product_Id_5=pm5.Product_Id and eeh5.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm6.Product_Genome,"___",5),"___",-1) from Event_Execution_History_1DAY eeh6, CDM_Product_Master pm6 where eeh6.Event_Id=@V_Event_ID AND eeh6.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh6.Recommendation_Product_Id_6=pm6.Product_Id and eeh6.Event_Execution_Id=eah.Event_Execution_Id),-1)));

					UPDATE Event_Attribution_History eah
					SET eah.Matched_ProductGenome=CONCAT(eah.Matched_ProductGenome,0)
					WHERE eah.Customer_Id between @vStart_Cnt and @vEnd_Cnt
					AND eah.Event_Execution_Date_ID=@V_Event_Execution_Date_ID
					AND eah.Event_Id=@V_Event_ID
					AND LENGTH(eah.Matched_ProductGenome)=4;
				END IF;

				IF @Num_of_Attr>=6
				THEN
					update Event_Attribution_History eah, CDM_Bill_Details_ResponseWindow bills, CDM_Product_Master pm
							SET eah.Matched_ProductGenome_Percent = Matched_ProductGenome_Percent+(1/@Num_of_Attr),
							eah.Matched_ProductGenome=concat(eah.Matched_ProductGenome,1)
							WHERE eah.Event_Execution_Date_Id = @V_Current_Execution_Date_Id
							AND eah.Customer_Id between @vStart_Cnt and @vEnd_Cnt
							AND eah.Event_Execution_Date_ID=@V_Event_Execution_Date_ID
							AND eah.Event_Id=@V_Event_ID
							AND eah.Bill_Details_ID=bills.Bill_Details_ID
							AND pm.Product_Id=bills.Product_Id
							AND @Num_of_Attr>1
							AND substring_Index(substring_index(pm.Product_Genome,"___",6),"___",-1) in 
							((SELECT IFNULL((select substring_Index(substring_index(pm1.Product_Genome,"___",6),"___",-1) from Event_Execution_History_1DAY eeh1, CDM_Product_Master pm1  where eeh1.Event_Id=@V_Event_ID AND eeh1.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh1.Recommendation_Product_Id_1=pm1.Product_Id and eeh1.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm2.Product_Genome,"___",6),"___",-1) from Event_Execution_History_1DAY eeh2, CDM_Product_Master pm2  where eeh2.Event_Id=@V_Event_ID AND eeh2.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh2.Recommendation_Product_Id_2=pm2.Product_Id and eeh2.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm3.Product_Genome,"___",6),"___",-1) from Event_Execution_History_1DAY eeh3, CDM_Product_Master pm3  where eeh3.Event_Id=@V_Event_ID AND eeh3.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh3.Recommendation_Product_Id_3=pm3.Product_Id and eeh3.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm4.Product_Genome,"___",6),"___",-1) from Event_Execution_History_1DAY eeh4, CDM_Product_Master pm4  where eeh4.Event_Id=@V_Event_ID AND eeh4.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh4.Recommendation_Product_Id_4=pm4.Product_Id and eeh4.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm5.Product_Genome,"___",6),"___",-1) from Event_Execution_History_1DAY eeh5, CDM_Product_Master pm5  where eeh5.Event_Id=@V_Event_ID AND eeh5.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh5.Recommendation_Product_Id_5=pm5.Product_Id and eeh5.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm6.Product_Genome,"___",6),"___",-1) from Event_Execution_History_1DAY eeh6, CDM_Product_Master pm6 where eeh6.Event_Id=@V_Event_ID AND eeh6.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh6.Recommendation_Product_Id_6=pm6.Product_Id and eeh6.Event_Execution_Id=eah.Event_Execution_Id),-1)));

					UPDATE Event_Attribution_History eah
					SET eah.Matched_ProductGenome=CONCAT(eah.Matched_ProductGenome,0)
					WHERE eah.Customer_Id between @vStart_Cnt and @vEnd_Cnt
					AND eah.Event_Execution_Date_ID=@V_Event_Execution_Date_ID
					AND eah.Event_Id=@V_Event_ID
					AND LENGTH(eah.Matched_ProductGenome)=5;
				END IF;

				IF @Num_of_Attr>=7
				THEN
					update Event_Attribution_History eah, CDM_Bill_Details_ResponseWindow bills, CDM_Product_Master pm
							SET eah.Matched_ProductGenome_Percent = Matched_ProductGenome_Percent+(1/@Num_of_Attr),
							eah.Matched_ProductGenome=concat(eah.Matched_ProductGenome,1)
							WHERE eah.Event_Execution_Date_Id = @V_Current_Execution_Date_Id
							AND eah.Customer_Id between @vStart_Cnt and @vEnd_Cnt
							AND eah.Event_Execution_Date_ID=@V_Event_Execution_Date_ID
							AND eah.Event_Id=@V_Event_ID
							AND eah.Bill_Details_ID=bills.Bill_Details_ID
							AND pm.Product_Id=bills.Product_Id
							AND @Num_of_Attr>1
							AND substring_Index(substring_index(pm.Product_Genome,"___",7),"___",-1) in 
							((SELECT IFNULL((select substring_Index(substring_index(pm1.Product_Genome,"___",7),"___",-1) from Event_Execution_History_1DAY eeh1, CDM_Product_Master pm1  where eeh1.Event_Id=@V_Event_ID AND eeh1.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh1.Recommendation_Product_Id_1=pm1.Product_Id and eeh1.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm2.Product_Genome,"___",7),"___",-1) from Event_Execution_History_1DAY eeh2, CDM_Product_Master pm2  where eeh2.Event_Id=@V_Event_ID AND eeh2.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh2.Recommendation_Product_Id_2=pm2.Product_Id and eeh2.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm3.Product_Genome,"___",7),"___",-1) from Event_Execution_History_1DAY eeh3, CDM_Product_Master pm3  where eeh3.Event_Id=@V_Event_ID AND eeh3.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh3.Recommendation_Product_Id_3=pm3.Product_Id and eeh3.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm4.Product_Genome,"___",7),"___",-1) from Event_Execution_History_1DAY eeh4, CDM_Product_Master pm4  where eeh4.Event_Id=@V_Event_ID AND eeh4.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh4.Recommendation_Product_Id_4=pm4.Product_Id and eeh4.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm5.Product_Genome,"___",7),"___",-1) from Event_Execution_History_1DAY eeh5, CDM_Product_Master pm5  where eeh5.Event_Id=@V_Event_ID AND eeh5.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh5.Recommendation_Product_Id_5=pm5.Product_Id and eeh5.Event_Execution_Id=eah.Event_Execution_Id),-1))
							,
							(SELECT IFNULL((SELECT substring_Index(substring_index(pm6.Product_Genome,"___",7),"___",-1) from Event_Execution_History_1DAY eeh6, CDM_Product_Master pm6 where eeh6.Event_Id=@V_Event_ID AND eeh6.Customer_Id between @vStart_Cnt and @vEnd_Cnt AND eeh6.Recommendation_Product_Id_6=pm6.Product_Id and eeh6.Event_Execution_Id=eah.Event_Execution_Id),-1)));

					UPDATE Event_Attribution_History eah
					SET eah.Matched_ProductGenome=CONCAT(eah.Matched_ProductGenome,0)
					WHERE eah.Customer_Id between @vStart_Cnt and @vEnd_Cnt
					AND eah.Event_Execution_Date_ID=@V_Event_Execution_Date_ID
					AND eah.Event_Id=@V_Event_ID
					AND LENGTH(eah.Matched_ProductGenome)=6;
				END IF;
			END IF;


		END LOOP LOOP_ALL_EVENTS;
		CLOSE curALL_EVENTS;
        
		SET vStart_Cnt = vEnd_Cnt+1;
		IF vStart_Cnt  >= @Rec_Cnt THEN
			LEAVE LOOP_ALL_CUSTOMERS;
		END IF;        
	  END LOOP LOOP_ALL_CUSTOMERS; 

      
		INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
			values ('S_CLM_Event_Attribution ', V_Current_Execution_Date_Id,now(),null,'Dashboard', Load_Exe_ID);
		SELECT 'S_CLM_Event_Attribution :' as '', concat(V_Current_Execution_Date_Id,'_Dashboard') as '', NOW() as '' ; 

		TRUNCATE Event_Attribution_History_1DAY;
		INSERT INTO Event_Attribution_History_1DAY  
        (
        Event_Execution_Id,
		Responder,
		Target_Bills,
        Target_Revenue,
        Target_Discount,
        In_Control_Group,
        In_Event_Control_Group
        )
		SELECT 
		Event_Attribution_History.Event_Execution_ID AS Event_Execution_Id,
		1 AS Responder,
		count(distinct Bill_Header_Id) AS Target_Bills,	
        SUM(Event_Attribution_History.Sale_Net_Val) as Target_Revenue,
        SUM(Event_Attribution_History.Sale_Disc_Val) as Target_Discount,
		MAX(Event_Attribution_History.In_Control_Group) AS In_Control_Group,
		MAX(Event_Attribution_History.In_Event_Control_Group) AS In_Event_Control_Group
	FROM
		Event_Attribution_History
	WHERE
		Event_Execution_Date_Id = V_Current_Execution_Date_Id
	GROUP BY Event_Attribution_History.Event_Execution_ID ;
    
    
    SET @Override_Revenue_From_BillHeader=0;
    SELECT ifnull(value,0) into @Override_Revenue_From_BillHeader from CLM_Response_Config where name='Override_Revenue_From_BillHeader';
	IF @Override_Revenue_From_BillHeader = 1
	THEN
				UPDATE Event_Attribution_History_1DAY EA,  ( SELECT 
					SUM(Bill_Total_Val) AS Revenue,
					SUM(Bill_Disc) AS Discount,
					Event_Execution_Id
				FROM
					(SELECT DISTINCT
						A.Bill_Header_ID,
							B.Bill_Total_Val,
							B.Bill_Disc,
							A.Event_Execution_Id
					FROM
						Event_Attribution_History A, CDM_Bill_Header B
					WHERE A.Event_Execution_Date_ID=V_Current_Execution_Date_Id
					AND   A.Customer_Id = B.Customer_Id
					AND A.Bill_Header_Id = B.Bill_Header_Id
					GROUP BY A.Event_Execution_ID , A.Bill_Header_ID) TEMPAlias1
				GROUP BY Event_Execution_ID) revenueforevent
				SET EA.Target_Revenue = revenueforevent.Revenue,
					EA.Target_Discount = revenueforevent.discount
				WHERE EA.Event_Execution_Id=revenueforevent.Event_Execution_Id;
			END IF;
			
			DELETE FROM CLM_Event_Dashboard WHERE EE_Date=V_Current_Execution_Date;
			INSERT INTO CLM_Event_Dashboard
				(`Revenue Center`,
				`Segment`,
				`Campaign`,
				`EE_Date`,
				`Event_Execution_Date_ID`,
				`Theme`,
				`Offer`,
				`Channel`,
				`Event_ID`,
				`Control_Base`,
				`Control_Base_Event`,
				`Target_Base`,
				`Target_Delivered`,
				`Control_Responder`,
				`Control_Responder_Event`,
				`Target_Responder`,
				`Target_Revenue`,
				`Target_Discount`,
				`Target_Bills`,
				`Target_Open`,
				`Target_CTA`,
                `Control_Revenue`,
				`Control_Discount`,
				`Control_Bills`)
				select 
				 `Revenue_Center`,
				 Segment,
				 Campaign,
				 EE_Date,
				 Event_Execution_Date_ID,
				 Theme,
				 Offer,
				 Channel,
				 Event_ID,
				 Control_Base,
				 Control_Base_Event,
				 Target_Base,
				 Target_Delivered,
				 Control_Responder,
				 Control_Responder_Event,
				 Target_Responder,
				 Target_Revenue,
				 Target_Discount,
				 Target_Bills,
				 Target_Open,
				 Target_CTA,
                 Control_Revenue,
				 Control_Discount,
				 Control_Bills
				from CLM_Event_Dashboard_View;

			
	set V_Current_Execution_Date=date_sub(V_Current_Execution_Date,interval 1 day);
	set V_Current_Execution_Date_Id=date_format(V_Current_Execution_Date,'%Y%m%d');

END LOOP LOOP_ALL_DATES;


update ETL_Execution_Details
        set Status ='Succeeded',
        End_Time = now()
        where Procedure_Name='S_CLM_Event_Attribution' and 
        Load_Execution_ID = Load_Exe_ID
        and Execution_ID = Exe_ID;
select 'S_CLM_Event_Attribution : COMPLETED ' as '',  NOW() as '';

UPDATE CLM_Response_Config
SET `Value` = Current_Date()
where `Name` = 'Last_Response_Execution_Date';

UPDATE CLM_Response_Config
SET `Value` = (select date(MIN(Inserted_On)) from Hold_Out)
where `Name` = 'Control_Group_Refresh_Date';

If datediff(Current_Date(),(select `Value` from CLM_Response_Config where `Name`='Control_Group_Refresh_Date'))>(select `Value` from CLM_Response_Config where `Name`='Control_Group_Refresh_Interval')
THEN
	drop table if exists Hold_Out_Bkp_Before_Refresh;
    create table if not exists Hold_Out_Bkp_Before_Refresh as select * from Hold_Out;
    truncate Hold_Out;
    update CDM_Customer_Master set Is_New_Cust_Flag=1;
    UPDATE CLM_Response_Config
	SET `Value` = Current_Date()
	where `Name` = 'Control_Group_Refresh_Date';
END IF;
CALL S_CLM_Update_Segment_Count();
CALL S_CLM_CRM_Dashboard();
IF @Check1 = 'Y'
THEN
	CALL S_CLM_Hard_Response();
END IF;
END
