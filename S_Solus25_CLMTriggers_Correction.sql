
CREATE OR REPLACE PROCEDURE `S_Solus25_CLMTriggers_Correction`()
BEGIN


/* Creating Required Table */

CREATE TABLE IF NOT EXISTS Solus25_CLMTriggers(
Event_Id bigint(20) NOT NULL PRIMARY KEY
);

/* Inserting Records into the Table */

/* ### Modified Approach ### : Inserting unnecessary events into table*/

INSERT IGNORE INTO Solus25_CLMTriggers(Event_Id)
SELECT EventID FROM Event_Master 
WHERE State = 'Enabled'
AND EventId NOT IN (SELECT DISTINCT Event_Id FROM Event_Execution_History
WHERE Event_Execution_Date_Id BETWEEN DATE_FORMAT((DATE_ADD(CURDATE(),INTERVAL -8 DAY)),'%Y%m%d') and DATE_FORMAT(CURDATE(),'%Y%m%d')
AND Customer_Id BETWEEN 0 AND 100000);

INSERT IGNORE INTO Solus25_CLMTriggers(Event_Id)
SELECT EventID FROM Event_Master 
WHERE State = 'Enabled'
AND EventId NOT IN (SELECT DISTINCT Event_Id FROM Event_Execution_History
WHERE Event_Execution_Date_Id BETWEEN DATE_FORMAT((DATE_ADD(CURDATE(),INTERVAL -8 DAY)),'%Y%m%d') and DATE_FORMAT(CURDATE(),'%Y%m%d')
AND Customer_Id BETWEEN 100001 AND 200000);

INSERT IGNORE INTO Solus25_CLMTriggers(Event_Id)
SELECT EventID FROM Event_Master 
WHERE State = 'Enabled'
AND EventId NOT IN (SELECT DISTINCT Event_Id FROM Event_Execution_History
WHERE Event_Execution_Date_Id BETWEEN DATE_FORMAT((DATE_ADD(CURDATE(),INTERVAL -8 DAY)),'%Y%m%d') and DATE_FORMAT(CURDATE(),'%Y%m%d')
AND Customer_Id BETWEEN 200001 AND 300000);


INSERT IGNORE INTO Solus25_CLMTriggers(Event_Id)
SELECT EventID FROM Event_Master 
WHERE State = 'Enabled'
AND EventId NOT IN (SELECT DISTINCT Event_Id FROM Event_Execution_History
WHERE Event_Execution_Date_Id BETWEEN DATE_FORMAT((DATE_ADD(CURDATE(),INTERVAL -8 DAY)),'%Y%m%d') and DATE_FORMAT(CURDATE(),'%Y%m%d')
AND Customer_Id BETWEEN 300001 AND 400000);


INSERT IGNORE INTO Solus25_CLMTriggers(Event_Id)
SELECT EventID FROM Event_Master 
WHERE State = 'Enabled'
AND EventId NOT IN (SELECT DISTINCT Event_Id FROM Event_Execution_History
WHERE Event_Execution_Date_Id BETWEEN DATE_FORMAT((DATE_ADD(CURDATE(),INTERVAL -8 DAY)),'%Y%m%d') and DATE_FORMAT(CURDATE(),'%Y%m%d')
AND Customer_Id BETWEEN 400001 AND 500000);

INSERT IGNORE INTO Solus25_CLMTriggers(Event_Id)
SELECT EventID FROM Event_Master 
WHERE State = 'Enabled'
AND EventId NOT IN (SELECT DISTINCT Event_Id FROM Event_Execution_History
WHERE Event_Execution_Date_Id BETWEEN DATE_FORMAT((DATE_ADD(CURDATE(),INTERVAL -8 DAY)),'%Y%m%d') and DATE_FORMAT(CURDATE(),'%Y%m%d')
AND Customer_Id BETWEEN 500001 AND 600000);

INSERT IGNORE INTO Solus25_CLMTriggers(Event_Id)
SELECT EventID FROM Event_Master 
WHERE State = 'Enabled'
AND EventId NOT IN (SELECT DISTINCT Event_Id FROM Event_Execution_History
WHERE Event_Execution_Date_Id BETWEEN DATE_FORMAT((DATE_ADD(CURDATE(),INTERVAL -8 DAY)),'%Y%m%d') and DATE_FORMAT(CURDATE(),'%Y%m%d')
AND Customer_Id BETWEEN 600001 AND 700000);

INSERT IGNORE INTO Solus25_CLMTriggers(Event_Id)
SELECT EventID FROM Event_Master 
WHERE State = 'Enabled'
AND EventId NOT IN (SELECT DISTINCT Event_Id FROM Event_Execution_History
WHERE Event_Execution_Date_Id BETWEEN DATE_FORMAT((DATE_ADD(CURDATE(),INTERVAL -8 DAY)),'%Y%m%d') and DATE_FORMAT(CURDATE(),'%Y%m%d')
AND Customer_Id BETWEEN 700001 AND 800000);

INSERT IGNORE INTO Solus25_CLMTriggers(Event_Id)
SELECT EventID FROM Event_Master 
WHERE State = 'Enabled'
AND EventId NOT IN (SELECT DISTINCT Event_Id FROM Event_Execution_History
WHERE Event_Execution_Date_Id BETWEEN DATE_FORMAT((DATE_ADD(CURDATE(),INTERVAL -8 DAY)),'%Y%m%d') and DATE_FORMAT(CURDATE(),'%Y%m%d')
AND Customer_Id > 800000);


/*Making Changes to Event_Master View*/

/*If the View Event_Master_Original is not created then this might fail, so adding this as well*/

/*Event Master Original View*/
CREATE 
    OR REPLACE
VIEW `Event_Master_Original` AS
    SELECT 
        `CLM_Campaign_Events`.`EventId` AS `ID`,
        CASE
            WHEN `CLM_Campaign_Trigger`.`CTState` = 1 THEN 1
            WHEN `CLM_Campaign_Trigger`.`CTState` = 2 THEN 2
            WHEN `CLM_Campaign_Trigger`.`CTState` = 3 THEN 3
            WHEN `CLM_Campaign_Trigger`.`CTState` = 4 THEN 4
            WHEN `CLM_Campaign_Trigger`.`CTState` = 5 THEN 5
            WHEN `CLM_Campaign_Trigger`.`CTState` = 0 THEN 0
            ELSE 0
        END AS `State_Original`,
        CASE
            WHEN
                `CLM_Campaign_Trigger`.`TriggerUsesWaterfall` IS NULL
            THEN
                CASE
                    WHEN `CLM_Campaign`.`CampaignUsesWaterfall` = - 1 THEN 0
                    WHEN `CLM_Campaign`.`CampaignUsesWaterfall` = 100 THEN 100
                    WHEN `CLM_Campaign`.`CampaignUsesWaterfall` = 99 THEN 99
                    ELSE 0
                END
            ELSE CASE
                WHEN `CLM_Campaign_Trigger`.`TriggerUsesWaterfall` = - 1 THEN 0
                WHEN `CLM_Campaign_Trigger`.`TriggerUsesWaterfall` = 100 THEN 100
                WHEN `CLM_Campaign_Trigger`.`TriggerUsesWaterfall` = 99 THEN 99
                ELSE 0
            END
        END AS `Waterfall_Original`,
        `CLM_Channel`.`ChannelName` AS `ChannelName`,
        CASE
            WHEN
                `CLM_Campaign_Events`.`CLMSegmentId` = (SELECT 
                        GROUP_CONCAT(`CLM_Segment`.`CLMSegmentId`
                                SEPARATOR ',')
                    FROM
                        `CLM_Segment`
                    WHERE
                        `CLM_Segment`.`In_Use` = 'Yes')
            THEN
                'ALL'
            ELSE GROUP_CONCAT('(', `CLM_Segment`.`CLMSegmentName`, ')'
                SEPARATOR ' OR ')
        END AS `CLMSegmentName`,
        `CLM_Campaign_Events`.`Execution_Sequence` AS `Execution_Sequence`,
        `CLM_Campaign_Events`.`ModifiedDate` AS `Updated_On`,
        `CLM_Campaign_Events`.`EventId` AS `EventId`,
        `CLM_Campaign_Trigger`.`CampTriggerId` AS `CampTriggerId`,
        `CLM_Creative`.`CreativeId` AS `CreativeId1`,
        `CLM_Campaign`.`CampaignId` AS `CampaignId`,
        `CLM_Creative`.`RankedPickListStory_Id` AS `RankedPickListStory_Id`,
        `CLM_Creative`.`Communication_Cooloff` AS `Communication_Cooloff`,
        `CLM_Campaign_Trigger`.`Event_Limit` AS `Event_Limit`,
        CONCAT(`CLM_Campaign_Trigger`.`CampTriggerName`) AS `Name`,
        `CLM_Creative`.`ExtCreativeKey` AS `ExtCreativeKey`,
        `CLM_Creative`.`SenderKey` AS `SenderKey`,
        `CLM_Timeslot_Master`.`Timeslot_Name` AS `Timeslot_Name`,
        `CLM_Timeslot_Master`.`Timeslot_Id` AS `Timeslot_Id`,
        `CLM_Reco_In_Creative`.`Number_Of_Recommendation` AS `Number_Of_Recommendation`,
        CASE
            WHEN `CLM_Campaign_Trigger`.`TriggerStartDate` IS NULL THEN CAST(`CLM_Campaign`.`CampaignStartDate` AS UNSIGNED)
            ELSE CAST(`CLM_Campaign_Trigger`.`TriggerStartDate`
                AS UNSIGNED)
        END AS `Start_Date_ID`,
        CASE
            WHEN `CLM_Campaign_Trigger`.`TriggerEndDate` IS NULL THEN CAST(`CLM_Campaign`.`CampaignEndDate` AS UNSIGNED)
            ELSE CAST(`CLM_Campaign_Trigger`.`TriggerEndDate` AS UNSIGNED)
        END AS `End_Date_ID`,
        `CLM_Creative`.`Creative` AS `Creative1`,
        CONCAT_WS(' AND ',
                CASE
                    WHEN
                        GROUP_CONCAT('(',
                            `CLM_Segment`.`CLMSegmentReplacedQuery`,
                            ')'
                            SEPARATOR ' OR ') = ''
                    THEN
                        NULL
                    ELSE CASE
                        WHEN
                            `CLM_Campaign_Events`.`CLMSegmentId` = (SELECT 
                                    GROUP_CONCAT(`CLM_Segment`.`CLMSegmentId`
                                            SEPARATOR ',')
                                FROM
                                    `CLM_Segment`
                                WHERE
                                    `CLM_Segment`.`In_Use` = 'Yes')
                        THEN
                            '1=1'
                        ELSE GROUP_CONCAT('(',
                            `CLM_Segment`.`CLMSegmentReplacedQuery`,
                            ')'
                            SEPARATOR ' OR ')
                    END
                END,
                CASE
                    WHEN `CLM_Campaign_Trigger`.`CTReplacedQuery` = '' THEN NULL
                    ELSE `CLM_Campaign_Trigger`.`CTReplacedQuery`
                END,
                CASE
                    WHEN `CLM_MicroSegment`.`MSReplacedQuery` = '' THEN NULL
                    ELSE `CLM_MicroSegment`.`MSReplacedQuery`
                END,
                CASE
                    WHEN `CLM_RegionSegment`.`RegionReplacedQuery` = '' THEN NULL
                    ELSE `CLM_RegionSegment`.`RegionReplacedQuery`
                END,
                CASE
                    WHEN `Campaign_Segment`.`Segment_Condition_Solus` = '' THEN NULL
                    ELSE GROUP_CONCAT('(',
                        `Campaign_Segment`.`Segment_Condition_Solus`,
                        ')'
                        SEPARATOR ' OR ')
                END) AS `Replaced_Query`,
        `CLM_Creative`.`Website_URL` AS `Website_URL`,
        `CLM_CampaignTheme`.`CampaignThemeName` AS `CampaignThemeName`,
        `CLM_MicroSegment`.`MicroSegmentName` AS `MicroSegmentName`,
        `CLM_Campaign`.`CampaignName` AS `CampaignName`,
        CONCAT(`CLM_CampaignTheme`.`CampaignThemeName`,
                '_',
                GROUP_CONCAT('(', `CLM_Segment`.`CLMSegmentName`, ')'
                    SEPARATOR ' OR '),
                '_',
                `CLM_Campaign`.`CampaignName`,
                '_',
                `CLM_Campaign_Trigger`.`CampTriggerName`,
                '_',
                `CLM_Channel`.`ChannelName`,
                '_',
                `CLM_MicroSegment`.`MicroSegmentName`) AS `Description`,
        CONCAT_WS(' AND ',
                CASE
                    WHEN `CLM_Campaign_Trigger`.`CTReplacedQueryBill` = '' THEN NULL
                    ELSE `CLM_Campaign_Trigger`.`CTReplacedQueryBill`
                END,
                CASE
                    WHEN `CLM_MicroSegment`.`MSReplacedQueryBill` = '' THEN NULL
                    ELSE `CLM_MicroSegment`.`MSReplacedQueryBill`
                END,
                CASE
                    WHEN `CLM_RegionSegment`.`RegionReplacedQueryBill` = '' THEN NULL
                    ELSE `CLM_RegionSegment`.`RegionReplacedQueryBill`
                END) AS `Replaced_Query_Bill`,
        `CLM_Reco_In_Creative`.`Recommendation_Filter_Logic` AS `Recommendation_Filter_Logic`,
        `CLM_Campaign`.`CampaignCGPercentage` AS `Event_Control_Group_Percentage`,
        `CLM_Campaign_Events`.`CreativeId` AS `Communication_Template_ID`,
        `CLM_Campaign_Events`.`CreatedDate` AS `Inserted_On`,
        `CLM_Campaign_Events`.`CampaignId` AS `Event_Type_ID`,
        `CLM_Campaign_Events`.`CLMSegmentId` AS `Mission_ID`,
        `CLM_Campaign_Events`.`CLMSegmentId` AS `CLMSegmentId`,
        CASE
            WHEN `CLM_Campaign_Trigger`.`TriggerResponseDays` < 0 THEN `CLM_Campaign`.`CampaignResponseDays`
            ELSE `CLM_Campaign_Trigger`.`TriggerResponseDays`
        END AS `Event_Response_Days`,
        `CLM_Campaign_Trigger`.`PrecedingTriggerId` AS `Reminder_Parent_Event_Id`,
        `CLM_Campaign_Trigger`.`DaysOffsetFromPrecedingTrigger` AS `Reminder_Days`,
        `CLM_Campaign_Events`.`CampaignThemeId` AS `Arms`,
        `CLM_Campaign`.`CampaignSmartPtyState` AS `Is_MAB_Driven`,
        CASE
            WHEN `CLM_Campaign_Trigger`.`TriggerUsesWaterfall` IS NULL THEN `CLM_Campaign`.`CampaignUsesWaterfall`
            ELSE `CLM_Campaign_Trigger`.`TriggerUsesWaterfall`
        END AS `Follows_Waterfall`,
        `CLM_Campaign_Trigger`.`CTUsesGoodTimeModel` AS `Is_Good_Time`,
        `CLM_Campaign`.`CampaignIsOnDemand` AS `Is_On_Demand`,
        `CLM_Campaign_Events`.`CampTriggerId` AS `Condition`,
        CASE
            WHEN `CLM_Creative`.`Creative` LIKE '%<SOLUS_PFIELD>%' THEN 1
            ELSE 0
        END AS `No_Of_Personalization`,
        `CLM_Campaign_Trigger`.`TriggerUsesControlGroup` AS `Needs_ControlGroup`,
        `CO`.`OfferName` AS `Offer_Code1`,
        `CLM_Campaign_Events`.`Event_Sent_Time` AS `Event_Sent_Time`,
        `CLM_Campaign_Trigger`.`PropensityModel_Id` AS `PropensityModel_Id`,
        `CLM_Campaign_Trigger`.`PropensityTopN_Percentile` AS `PropensityTopN_Percentile`
    FROM
        ((((((((((((`CLM_CampaignTheme`
        JOIN `CLM_Segment`)
        JOIN `CLM_Campaign`)
        JOIN `CLM_Campaign_Trigger`)
        JOIN `CLM_MicroSegment`)
        JOIN `CLM_Channel`)
        JOIN `CLM_Campaign_Events`)
        JOIN `CLM_Creative`)
        JOIN `CLM_Reco_In_Creative`)
        JOIN `CLM_Timeslot_Master`)
        LEFT JOIN `CLM_Offer` `CO` ON (`CLM_Creative`.`OfferId` = `CO`.`OfferId`))
        LEFT JOIN `CLM_RegionSegment` ON (`CLM_Campaign_Events`.`RegionSegmentId` = `CLM_RegionSegment`.`RegionSegmentId`))
        LEFT JOIN `Campaign_Segment` ON (FIND_IN_SET(`Campaign_Segment`.`Segment_Id`, `CLM_Campaign_Events`.`Campaign_SegmentId`)))
    WHERE
        `CLM_Campaign_Events`.`CampaignThemeId` = `CLM_CampaignTheme`.`CampaignThemeId`
            AND FIND_IN_SET(`CLM_Segment`.`CLMSegmentId`,
                `CLM_Campaign_Events`.`CLMSegmentId`)
            AND `CLM_Campaign_Events`.`CampaignId` = `CLM_Campaign`.`CampaignId`
            AND `CLM_Campaign_Events`.`CampTriggerId` = `CLM_Campaign_Trigger`.`CampTriggerId`
            AND `CLM_Campaign_Events`.`MicroSegmentId` = `CLM_MicroSegment`.`MicroSegmentId`
            AND `CLM_Campaign_Events`.`ChannelId` = `CLM_Channel`.`ChannelId`
            AND `CLM_Campaign_Events`.`CreativeId` = `CLM_Creative`.`CreativeId`
            AND `CLM_Campaign_Events`.`CreativeId` = `CLM_Reco_In_Creative`.`CreativeId`
            AND `CLM_Creative`.`Timeslot_Id` = `CLM_Timeslot_Master`.`Timeslot_Id`
    GROUP BY `CLM_Campaign_Events`.`EventId`
    ORDER BY `CLM_Campaign_Trigger`.`CTState` DESC , `CLM_Campaign`.`CampaignUsesWaterfall` , `CLM_Campaign_Events`.`Execution_Sequence` , `CLM_Campaign_Events`.`ModifiedDate` DESC;


/* Event Master View*/

CREATE 
   OR REPLACE
VIEW `Event_Master_Testing` AS
    SELECT 
        `Event_Master_Original`.`ID` AS `ID`,
        CASE
            WHEN `Event_Master_Original`.`State_Original` = 1  
            THEN 
				CASE
					WHEN `Event_Master_Original`.`EventId` IN (SELECT Event_ID FROM Solus25_CLMTriggers) THEN 'Disabled'
                    ELSE 'Enabled' /*This will ensure that the events in the our temp table are disabled as well as events with end date less than current date*/
                END
			
            WHEN `Event_Master_Original`.`State_Original` = 2 THEN 'Testing'
            WHEN `Event_Master_Original`.`State_Original` = 3 THEN 'DRAFT'
            WHEN `Event_Master_Original`.`State_Original` = 4 THEN 'VERIFIED'
            WHEN `Event_Master_Original`.`State_Original` = 5 THEN 'Schedule'
            WHEN `Event_Master_Original`.`State_Original` = 0 THEN 'Disabled'
            ELSE 'Disabled'
        END AS `State`,
        CASE
            WHEN `Event_Master_Original`.`Waterfall_Original` = 0 THEN 'ManualWaterfall'
            WHEN `Event_Master_Original`.`Waterfall_Original` = 100 THEN 'CMABPerfBased'
            WHEN `Event_Master_Original`.`Waterfall_Original` = 99 THEN 'TopPriorityNoCoolOff'
            ELSE 'ManualWaterfall'
        END AS `Waterfall`,
        `Event_Master_Original`.`ChannelName` AS `ChannelName`,
        `Event_Master_Original`.`CLMSegmentName` AS `CLMSegmentName`,
        `Event_Master_Original`.`Execution_Sequence` AS `Execution_Sequence`,
        `Event_Master_Original`.`Updated_On` AS `Updated_On`,
        `Event_Master_Original`.`EventId` AS `EventId`,
        `Event_Master_Original`.`CampTriggerId` AS `CampTriggerId`,
        `Event_Master_Original`.`CreativeId1` AS `CreativeId1`,
        `Event_Master_Original`.`CampaignId` AS `CampaignId`,
        `Event_Master_Original`.`RankedPickListStory_Id` AS `RankedPickListStory_Id`,
        `Event_Master_Original`.`Communication_Cooloff` AS `Communication_Cooloff`,
        `Event_Master_Original`.`Event_Limit` AS `Event_Limit`,
        `Event_Master_Original`.`Name` AS `Name`,
        `Event_Master_Original`.`ExtCreativeKey` AS `ExtCreativeKey`,
        `Event_Master_Original`.`SenderKey` AS `SenderKey`,
        `Event_Master_Original`.`Timeslot_Name` AS `Timeslot_Name`,
        `Event_Master_Original`.`Timeslot_Id` AS `Timeslot_Id`,
        `Event_Master_Original`.`Number_Of_Recommendation` AS `Number_Of_Recommendation`,
        `Event_Master_Original`.`Start_Date_ID` AS `Start_Date_ID`,
        `Event_Master_Original`.`End_Date_ID` AS `End_Date_ID`,
        `Event_Master_Original`.`Creative1` AS `Creative1`,
        `Event_Master_Original`.`Replaced_Query` AS `Replaced_Query`,
        `Event_Master_Original`.`Website_URL` AS `Website_URL`,
        `Event_Master_Original`.`CampaignThemeName` AS `CampaignThemeName`,
        `Event_Master_Original`.`MicroSegmentName` AS `MicroSegmentName`,
        `Event_Master_Original`.`CampaignName` AS `CampaignName`,
        `Event_Master_Original`.`Description` AS `Description`,
        `Event_Master_Original`.`Replaced_Query_Bill` AS `Replaced_Query_Bill`,
        `Event_Master_Original`.`Recommendation_Filter_Logic` AS `Recommendation_Filter_Logic`,
        `Event_Master_Original`.`Event_Control_Group_Percentage` AS `Event_Control_Group_Percentage`,
        `Event_Master_Original`.`Communication_Template_ID` AS `Communication_Template_ID`,
        `Event_Master_Original`.`Inserted_On` AS `Inserted_On`,
        `Event_Master_Original`.`Event_Type_ID` AS `Event_Type_ID`,
        `Event_Master_Original`.`Mission_ID` AS `Mission_ID`,
        `Event_Master_Original`.`CLMSegmentId` AS `CLMSegmentId`,
        `Event_Master_Original`.`Event_Response_Days` AS `Event_Response_Days`,
        `Event_Master_Original`.`Reminder_Parent_Event_Id` AS `Reminder_Parent_Event_Id`,
        `Event_Master_Original`.`Reminder_Days` AS `Reminder_Days`,
        `Event_Master_Original`.`Arms` AS `Arms`,
        `Event_Master_Original`.`Is_MAB_Driven` AS `Is_MAB_Driven`,
        `Event_Master_Original`.`Follows_Waterfall` AS `Follows_Waterfall`,
        `Event_Master_Original`.`Is_Good_Time` AS `Is_Good_Time`,
        `Event_Master_Original`.`Is_On_Demand` AS `Is_On_Demand`,
        `Event_Master_Original`.`Condition` AS `Condition`,
        `Event_Master_Original`.`No_Of_Personalization` AS `No_Of_Personalization`,
        `Event_Master_Original`.`Needs_ControlGroup` AS `Needs_ControlGroup`,
        `Event_Master_Original`.`Offer_Code1` AS `Offer_Code1`,
        `Event_Master_Original`.`Event_Sent_Time` AS `Event_Sent_Time`,
        `Event_Master_Original`.`PropensityModel_Id` AS `PropensityModel_Id`,
        `Event_Master_Original`.`PropensityTopN_Percentile` AS `PropensityTopN_Percentile`
    FROM
        `Event_Master_Original`
    ORDER BY `Event_Master_Original`.`State_Original` , `Event_Master_Original`.`Waterfall_Original` , `Event_Master_Original`.`Execution_Sequence` , `Event_Master_Original`.`Updated_On` DESC;
    
    
/* Printing The Events which are disabled by this procedure */

SELECT A.Event_Id AS 'Event_ID',
B.Name AS 'Event_Name',
B.Start_Date_Id AS 'Event_Start_Date',
B.End_Date_Id AS 'Event_End_Date' 
FROM Solus25_CLMTriggers A, Event_Master B
WHERE A.Event_Id = B.EventId;
    
    	
END

