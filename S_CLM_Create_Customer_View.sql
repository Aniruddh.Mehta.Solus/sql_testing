
CREATE OR REPLACE PROCEDURE `S_CLM_Create_Customer_View`(IN vEvent_Id BIGINT(20))
begin
 /* Version : Solus 2.1 20221124 */
    DECLARE done1,done2 INT DEFAULT FALSE;
    DECLARE Column_Name_cur1 TEXT;
    Declare cur1 cursor for
    SELECT distinct Column_Name_in_Details_View from Customer_View_Config
    where In_Use_Customer_Details_View <> 'Mandatory';
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
    SET done1 = FALSE;
    SET SQL_SAFE_UPDATES=0;
    set foreign_key_checks=0;
    UPDATE Customer_View_Config
	SET In_Use_Customer_Details_View = "N" 
	where In_Use_Customer_Details_View <> 'Mandatory';
	SELECT Creative1 INTO @creative FROM Event_Master where ID = vEvent_Id;
    CALL PROC_CLM_Extract_All_Col_Name_From_Comm_Template(@creative,'<SOLUS_PFIELD>','</SOLUS_PFIELD>','|', @vPField_For_Check);
    select (length(@vPField_For_Check)-length(replace(@vPField_For_Check,'|','')))+1 into @num_of_rc;
	set @loop_var=1;
	set @json_select='';
	select @loop_var,@num_of_rc,@vPField_For_Check;
    SET @recos_view_query='';
	WHILE @loop_var<=@num_of_rc
	do
		SET done1 = FALSE;
        SET @selected_rc=substring_index(substring_index(concat(@vPField_For_Check,',-1'),'|',@loop_var),'|',-1);
        SET @selected_rc=REPLACE(@selected_rc,'"','\\"');
		open cur1;
		new_loop_1: LOOP
						FETCH cur1 into Column_Name_cur1;
						IF done1 THEN
				 LEAVE new_loop_1;
			END IF;
            IF @selected_rc like concat("%",Column_Name_cur1,"%")
            THEN
				SELECT "YES";
            END IF;
            set @top_query=concat('Update Customer_View_Config
            SET In_Use_Customer_Details_View = "Y" 
            where Column_Name_in_Details_View = "',Column_Name_cur1,'"
            and "',@selected_rc,'" like "%',Column_Name_cur1,'%"');
            

            PREPARE stmt from @top_query;        
			EXECUTE stmt;
			DEALLOCATE PREPARE stmt;
		end LOOP;
		close cur1;
        SET @loop_var = @loop_var+1;
	end while;
    
    
   CALL S_CLM_Create_View();
END

