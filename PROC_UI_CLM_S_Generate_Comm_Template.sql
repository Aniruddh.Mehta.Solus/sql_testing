
CREATE or replace PROCEDURE `PROC_UI_CLM_S_Generate_Comm_Template`(
	IN v_pfields TEXT, 
	IN sms_flag CHAR(1), 
	IN pn_flag CHAR(1), 
	IN email_flag CHAR(1), 
	OUT Comm_Header TEXT, 
	OUT Comm_Template TEXT
   ,OUT vSuccess int (4), 
    OUT vErrMsg Text
)
BEGIN
	DECLARE  v_Del VARCHAR(12) DEFAULT ',';

	DECLARE  v_emailStr VARCHAR(64) DEFAULT '\{\"Email\":[';
	DECLARE  v_smsStr VARCHAR(64) DEFAULT '],\"SMS\":[';
	DECLARE  v_pnStr VARCHAR(64) DEFAULT '],\"PN\":[';
	DECLARE  v_closingStr VARCHAR(64) DEFAULT ']}';
	DECLARE  v_sms_header VARCHAR(512) DEFAULT '';
	DECLARE  v_pn_header VARCHAR(512) DEFAULT '';
	DECLARE  v_email_header VARCHAR(512) DEFAULT '';
	DECLARE  v_HeaderValues VARCHAR(512) DEFAULT '';

	DECLARE Load_Exe_ID, Exe_ID int;
    
    
	Declare done,done2 INT DEFAULT FALSE;
    Declare y longtext;


                
Declare cur1 CURSOR for 

select distinct concat(substring(a,1,7),concat(substring(replace(a,'|',','),8)) )from (

select concat('select cdv.Customer_Id,concat(',Comm_Template,') 
from Customer_Details_View cdv where Customer_Id<100','') as a from
 Customer_Details_View
 where Customer_Id<100
) b;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

   
	 SET vSuccess = 0;
	SET vErrMsg = CONCAT('\r\n','Please check the Template properly ');
    
	SELECT 
		CONCAT('\'',
            GROUP_CONCAT(item
                SEPARATOR "','"),
            '\'') INTO v_HeaderValues 
	FROM (
		SELECT 
			position
			, TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(v_pfields, v_Del, position + 1), v_Del, - 1)) AS `item`
		FROM (
			SELECT 
				n AS `position`
			FROM (
				SELECT 0 AS `n` 
				UNION ALL 
				SELECT 1 AS `1` 
				UNION ALL 
				SELECT 2 AS `2` 
				UNION ALL 
				SELECT 3 AS `3` 
				UNION ALL 
				SELECT 4 AS `4` 
				UNION ALL 
				SELECT 5 AS `5` 
				UNION ALL 
				SELECT 6 AS `6` 
				UNION ALL 
				SELECT 6 AS `7` 
				UNION ALL 
				SELECT 6 AS `8` 
				UNION ALL 
				SELECT 6 AS `9` 
				UNION ALL 
				SELECT 6 AS `10` 
				UNION ALL 
				SELECT 6 AS `11` 
				UNION ALL 
				SELECT 6 AS `12` 
				UNION ALL 
				SELECT 6 AS `13` 
				UNION ALL 
				SELECT 6 AS `14` 
				UNION ALL 
				SELECT 6 AS `15` 
				UNION ALL 
				SELECT 6 AS `16`
			) AS vw
		) AS tallyGenerator
	WHERE
		position <= (CHAR_LENGTH(v_pfields) - CHAR_LENGTH(REPLACE(v_pfields, v_Del, '')))) delimitedString;


	IF sms_flag = 'Y' THEN
		SET  v_sms_header = v_HeaderValues;
	END IF;
	IF pn_flag = 'Y' THEN
		SET  v_pn_header = v_HeaderValues;
	END IF ;
	IF email_flag = 'Y' THEN
		SET v_email_header  = v_HeaderValues;
	END IF;

	SELECT replace(replace(CONCAT(v_emailStr, v_email_header ,v_smsStr, v_sms_header,v_pnStr, v_pn_header, v_closingStr),"'",'"'),'"','\\"') INTO Comm_Template;
	
	SELECT concat('<SOLUS_PFIELD>',replace(v_pfields,',','</SOLUS_PFIELD>|<SOLUS_PFIELD>'),'</SOLUS_PFIELD>') INTO Comm_Header;
    select replace(v_pfields,',','|') into Comm_Template;
select  Comm_Header;
if v_pfields='' then
    set Comm_Header='';
	set  Comm_Template=''; 
end if;
set @vSuccess=1;
set @vErrMsg='no';
 IF 	@vSuccess =1 Then
		select  @vSuccess into vSuccess ;
		select @vErrMsg into  vErrMsg ;
    ELSE 
			select  @vSuccess into vSuccess ;
		select @vErrMsg into  vErrMsg ;
   END IF;
   
    
END

