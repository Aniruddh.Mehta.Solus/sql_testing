
CREATE OR REPLACE PROCEDURE `S_UI_Instant_Brew_Segment`(IN in_request_json json, OUT out_result_json json)
BEGIN
    

    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
    declare EDIT Bigint; 
 
    

declare IN_AGGREGATION_1 text; 
	declare IN_AGGREGATION_2 text;
	declare IN_AGGREGATION_3 text;
	declare IN_AGGREGATION_4 text;
	declare IN_AGGREGATION_5 text;
	
	SET SQL_SAFE_UPDATES=0;
	SET foreign_key_checks=0;

	Set in_request_json = Replace(in_request_json,'\n','\\n');
  
   
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1")); 
	SET IN_AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));
	SET IN_AGGREGATION_3 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_3"));
	SET IN_AGGREGATION_4 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_4"));
    
	Set @temp_result =Null;
    Set @vSuccess ='';
	Set @vErrMsg='';
	Set @vSuccess='';
	Set @Err =0;

        
       
        if IN_AGGREGATION_1 = 'Check_Existing_Segment'
        then
   	SELECT Segment_Name into @CampaignSegmentName FROM Brew_Segment where Segment_Name =IN_AGGREGATION_2;

     				If @CampaignSegmentName  = IN_AGGREGATION_2
     				   Then
			 			
		                    SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Segment Name Already Present") )),"]")
													    into @temp_result 
													   ;') ;
													      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
                            
                            Else 
							SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Segment Name Not Present") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
        end if;
        
        
   elseif IN_AGGREGATION_1 = 'Save_Segment'
        then
        
         set @s2 = IN_AGGREGATION_4;
           set @s2 = replace(@s2,",AND"," AND ");
        set @s2 = replace(@s2,",OR"," OR ");
           set @s2 = replace(@s2,","," AND ");
                 set @s2 = replace(@s2,",AND"," AND ");
        set @s2 = replace(@s2,",OR"," OR ");
          -- set @s2 = replace(@s2,","," AND ");
           set @s2 = replace(@s2,"equal"," IN ");
           set @s2 = replace(@s2,"NOT >"," < ");
           set @s2 = replace(@s2,"Avg"," ");
           set @s2 = replace(@s2,"Max"," ");
        if @s2 not RLIKE '`Transacated On`|Product_Name|`Category Purchased'
			then
       Set @Query_String= concat('select count( `Customer Id`  ) into @temp_result from (
SELECT 
    Recency,
    Vintage,
    City,
    Point_Balance as `Point Balance`,
    Customer_Id as `Customer Id`,
    Lt_ABV as ABV,
    AP_Num_of_Visits as `Visits in Active Period`,
    Lt_Num_of_Visits as `Total Visits`,
    Lt_Rev as `Total Revenue`,
    Lt_Fav_Day_Of_Week as `Fav Day Of Week`,
            Tenure,
Lt_UPT as `Units per Transaction`,
Lt_GMV as `Gross Margin value`,
Lt_ASP as `Average Selling Price`,
    Age,
    LT_Distinct_Prod_Cnt as `Distinct Products Bought`,
    LT_Distinct_Cat_Cnt as `Distinct Category Bought`,
    Num_Of_Cart_Items as `Number of Items in Cart`,
    (select Lov_Value from CDM_LOV_Master a where
    a.Lov_Id = LT_Fav_Cat_LOV_Id) as `Favourite Category`,
    (select Lov_Value from CDM_LOV_Master a where
    a.Lov_Id = LT_Fav_Prod_LOV_Id) as `Favourite Product`
FROM
    Customer_One_View) t1
    where ',@s2,';');
    
    SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
        
        elseif 
        @s2 not RLIKE 'Recency|Vintage|City|Point_Balance|AP_Num_of_Visits|Lt_Num_of_Visits|Lt_Rev|Age|Num_Of_Cart_Items|Fav_Cat|Fav_Prod|`Distinct Products Bought`|`Distinct Category Bought`'
        then
        
       Set @Query_String= concat('select count(distinct(Customer_Id)) into @temp_result from (
select Customer_id as `Customer Id`, Bill_Date as `Transacated On`,(select Product_Comm_Name from CDM_Product_Master_Original a where
    a.Product_Id = b.Product_Id) as `Product Name`,
    (select Lov_Value from CDM_LOV_Master c,CDM_Product_Master_Original d where
    c.Lov_Id = d.Cat_Lov_Id and
     d.Product_Id = b.Product_Id) as `Category Purchased`
from CDM_Bill_Details b) t1
where ',@s2,';');

 SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;

else

Set @Query_String= concat('select count(distinct Customer_Id) into @temp_result from (
 select 
t1.Customer_id, Bill_Date  as `Transacated On` ,
(select Product_Comm_Name from CDM_Product_Master_Original a where a.Product_Id = t1.Product_Id) as Product_Name,
(select Lov_Value from CDM_LOV_Master c,CDM_Product_Master_Original d where c.Lov_Id = d.Cat_Lov_Id and d.Product_Id = t1.Product_Id) as `Category Purchased`,
 Recency,
    Vintage,
    City,
    Point_Balance as `Point Balance`,
    Lt_ABV as ABV,
        Tenure,
Lt_UPT as `Units per Transaction`,
Lt_GMV as `Gross Margin value`,
Lt_ASP as `Average Selling Price`,
     LT_Distinct_Prod_Cnt as `Distinct Products Bought`,
    LT_Distinct_Cat_Cnt as `Distinct Category Bought`,
        Lt_Fav_Day_Of_Week as `Fav Day Of Week`,
    t1.Customer_Id as `Customer Id`,
    AP_Num_of_Visits as `Visits in Active Period`,
    Lt_Num_of_Visits as `Total Visits`,
    Lt_Rev as `Total Revenue`,
    Age,
    Num_Of_Cart_Items as `Number of Items in Cart`,
   (select Lov_Value from CDM_LOV_Master a where
    a.Lov_Id = LT_Fav_Cat_LOV_Id) as `Favourite Category`,
    (select Lov_Value from CDM_LOV_Master a where
    a.Lov_Id = LT_Fav_Prod_LOV_Id) as `Favourite Product`
from CDM_Bill_Details t1, Customer_One_View t2
where t1.Customer_Id = t2.Customer_Id
) as t3
where ',@s2,';');

SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;


        end if;
        
  if IN_AGGREGATION_2 != ''
     				then
                    insert into Brew_Segment( Segment_Name,Segment_Type,Segment_Query_Condition,Segment_Human_Readable_Condition,Coverage)
select IN_AGGREGATION_2,IN_AGGREGATION_3,IN_AGGREGATION_4,replace(replace(IN_AGGREGATION_4,")",""),"(",""),@temp_result;
                    
								set sql_safe_updates=0;
								update Brew_Segment P, (select distinct(new_Col),old_col from (
								select Segment_Id,b.Segment_Query_Condition as old_col,replace(b.Segment_Query_Condition,a.UI_Variable_Name,a.SOLUS_Attribute) as new_Col from Brew_Segment b
								left join Campaign_Segment_Dictionary a on b.Segment_Query_Condition like '%' || a.UI_Variable_Name || '%') as t1
								where new_Col not in ( select Segment_Query_Condition from Campaign_Segment)) Q
								set Segment_Condition_Solus = Q.new_Col
								where P.Segment_Query_Condition = Q.old_col;

								update Brew_Segment
								set Segment_Condition_Solus = Segment_Query_Condition
								where Segment_Condition_Solus is NULL;
       
       
         /*Inserting the Created Brew Segment Into Campaign_Segment*/
		
								INSERT IGNORE INTO Solus_Segment(Segment_Name,
																	Segment_Desc,
																	Segment_Type,
                                                                    Segment_Scheme,
																	Segment_Query_Condition,
																	Segment_Human_Readable_Condition,
																	status,
																	Coverage,
																	LastRefreshDate,
																	progress,
																	Segment_Condition_Solus)
																	
								SELECT Segment_Name,
									   Segment_Desc,
									   'CAMPAIGN',
                                       'CLM Segment',
                                       Segment_Query_Condition,
									   Segment_Human_Readable_Condition,
									   'Not_Refreshed_Once',
									   Coverage,
									   NULL,
									   NULL,
									   Segment_Condition_Solus
									   
							  FROM Brew_Segment
							  
							  ORDER By Segment_Id DESC
							  
							  LIMIT 1;
			 			
		                    SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Success") )),"]")
													    into @temp_result 
													   ;') ;
													      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
                            
                            Else 
							SET @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Segment Name Not Present") )),"]")
													    into @temp_result 
													   ;') ;
						      
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
                            

        
        end if;
        


elseif IN_AGGREGATION_1 = 'Refresh_Segment_Status'
        then 

        select 	`Status` into @p1 from Brew_Segment where Segment_Name = IN_AGGREGATION_2;
        select 	date_format(LastRefreshDate, "%d/%m %H:%i") into @p2 from Brew_Segment where Segment_Name = IN_AGGREGATION_2;

        if @p1 = 1
        then 
           Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","Refresh Completed","Segment_Id",Segment_Id,"Segment_Name",Segment_Name,"Segment_Desc",Segment_Desc,"Last Refresh Date",date_format(LastRefreshDate, "%d/%m %H:%i"),"Coverage",Coverage ) )),"]") 
														into @temp_result  from Brew_Segment where Segment_Name = ','"',IN_AGGREGATION_2,'"',';                 
												      ;') ;
                                                      
        
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
        
        else 
         Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Refresh_Not_Yet_Complete") )),"]") 
														into @temp_result                       
												      ;') ; 
        
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
end if;
        
        
	
                            
elseif  IN_AGGREGATION_1 = 'Check_Segment_Coverage'                   
		
       then
        set @s2 = IN_AGGREGATION_2;
        set @s2 = replace(@s2,",AND"," AND ");
        set @s2 = replace(@s2,",OR"," OR ");
          -- set @s2 = replace(@s2,","," AND ");
           set @s2 = replace(@s2,"equal"," IN ");
           set @s2 = replace(@s2,"NOT >"," < ");
           set @s2 = replace(@s2,"Avg"," ");
           set @s2 = replace(@s2,"Max"," ");
        set @temp_result = @s2;
        
        
   if @s2 not RLIKE '`Transacated On`|Product_Name|`Category Purchased'
			then
       Set @Query_String= concat('select count( `Customer Id` ) into @temp_result from (
SELECT 
    Recency,
    Vintage,
    City,
    Point_Balance as `Point Balance`,
    Customer_Id as `Customer Id`,
    Lt_ABV as ABV,
    AP_Num_of_Visits as `Visits in Active Period`,
    Lt_Num_of_Visits as `Total Visits`,
    Lt_Rev as `Total Revenue`,
    Lt_Fav_Day_Of_Week as `Fav Day Of Week`,
            Tenure,
Lt_UPT as `Units per Transaction`,
Lt_GMV as `Gross Margin value`,
Lt_ASP as `Average Selling Price`,
    Age,
    LT_Distinct_Prod_Cnt as `Distinct Products Bought`,
    LT_Distinct_Cat_Cnt as `Distinct Category Bought`,
    Num_Of_Cart_Items as `Number of Items in Cart`,
    (select Lov_Value from CDM_LOV_Master a where
    a.Lov_Id = LT_Fav_Cat_LOV_Id) as `Favourite Category`,
    (select Lov_Value from CDM_LOV_Master a where
    a.Lov_Id = LT_Fav_Prod_LOV_Id) as `Favourite Product`
FROM
    Customer_One_View) t1
    where ',@s2,';');
    
    SELECT @Query_String;
    
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;
        
        elseif 
        @s2 not RLIKE 'Recency|Vintage|City|Point_Balance|AP_Num_of_Visits|Lt_Num_of_Visits|Lt_Rev|Age|Num_Of_Cart_Items|Fav_Cat|Fav_Prod|`Distinct Products Bought`|`Distinct Category Bought`'
        then
        
       Set @Query_String= concat('select count(distinct(`Customer Id`)) into @temp_result from (
select Customer_id as `Customer Id`, Bill_Date as `Transacated On`,(select Product_Comm_Name from CDM_Product_Master_Original a where
    a.Product_Id = b.Product_Id) as `Product Name`,
    (select Lov_Value from CDM_LOV_Master c,CDM_Product_Master_Original d where
    c.Lov_Id = d.Cat_Lov_Id and
     d.Product_Id = b.Product_Id) as `Category Purchased`
from CDM_Bill_Details b) t1
where ',@s2,';');

 SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;

else

Set @Query_String= concat('select count(distinct Customer_Id) into @temp_result from (
 select 
t1.Customer_id, Bill_Date  as `Transacated On` ,
(select Product_Comm_Name from CDM_Product_Master_Original a where a.Product_Id = t1.Product_Id) as Product_Name,
(select Lov_Value from CDM_LOV_Master c,CDM_Product_Master_Original d where c.Lov_Id = d.Cat_Lov_Id and d.Product_Id = t1.Product_Id) as `Category Purchased`,
 Recency,
    Vintage,
    City,
    Point_Balance as `Point Balance`,
    Lt_ABV as ABV,
        Tenure,
Lt_UPT as `Units per Transaction`,
Lt_GMV as `Gross Margin value`,
Lt_ASP as `Average Selling Price`,
     LT_Distinct_Prod_Cnt as `Distinct Products Bought`,
    LT_Distinct_Cat_Cnt as `Distinct Category Bought`,
        Lt_Fav_Day_Of_Week as `Fav Day Of Week`,
    t1.Customer_Id as `Customer Id`,
    AP_Num_of_Visits as `Visits in Active Period`,
    Lt_Num_of_Visits as `Total Visits`,
    Lt_Rev as `Total Revenue`,
    Age,
    Num_Of_Cart_Items as `Number of Items in Cart`,
   (select Lov_Value from CDM_LOV_Master a where
    a.Lov_Id = LT_Fav_Cat_LOV_Id) as `Favourite Category`,
    (select Lov_Value from CDM_LOV_Master a where
    a.Lov_Id = LT_Fav_Prod_LOV_Id) as `Favourite Product`
from CDM_Bill_Details t1, Customer_One_View t2
where t1.Customer_Id = t2.Customer_Id
) as t3
where ',@s2,';');

SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement;


        end if;
        
           Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message",','"',@temp_result,'"',') )),"]") 
														into @temp_result                       
												      ;') ; 
        
        SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
	
     
     elseif IN_AGGREGATION_1 = 'Brew_Segment_Attributes' 
        then
         Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Segment_Id",Id,"Segment_Name",`Segment Name`,"Segment_Human_Readable_Condition",Rep_Query,"Segment_Desc",Segment_Desc,"Coverage",Coverage,"ModifiedDate",date_format(ModifiedDate, "%d/%m %H:%i") ) )),"]") 
														into @temp_result  from 
(select Segment_Id as Id, Segment_Name as `Segment Name`,Segment_Query_Condition as Rep_Query,Coverage as Coverage, "Campaign_Type",ModifiedDate as ModifiedDate,Segment_Desc as Segment_Desc  from Campaign_Segment
union all
select CLMSegmentId as Id,CLMSegmentName as `Segment Name`,CLMSegmentReplacedQueryAsInUI as Rep_Query,Coverage as Coverage,"Lifecycle Type",ModifiedDate as ModifiedDate,CLMSegmentDesc as Segment_Desc from CLM_Segment
) b;             
								;') ;
                                                      
        
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
        
        
elseif IN_AGGREGATION_1 = 'Brew_Segment_List' 
        then
         Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Segment_Id",Segment_Id,"Segment_Name",Segment_Name,"Segment_Desc",Segment_Desc,"Last Refresh Date",date_format(LastRefreshDate, "%d/%m %H:%i"),"Coverage",Coverage,"Segment_Human_Readable_Condition",Segment_Human_Readable_Condition,"ModifiedDate",date_format(ModifiedDate, "%d/%m %H:%i") )order by ModifiedDate desc )),"]") 
														into @temp_result  from Brew_Segment;             
												      ;') ;
                                                      
        
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
        
        
	end if;

          
    if @temp_result is null
		Then
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") 
														into @temp_result                       
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	End if;
							
    SET out_result_json = @temp_result;                                             
  


END

