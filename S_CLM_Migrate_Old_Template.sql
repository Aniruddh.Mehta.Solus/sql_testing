
CREATE or replace PROCEDURE `S_CLM_Migrate_Old_Template`()
BEGIN
SET SQL_SAFE_UPDATES = 0;
update CLM_Creative
SET Creative = replace(replace(concat('"<SOLUS_PFIELD>"',(replace(replace(replace(replace(replace(replace(replace(Creative,'"<Email>',''),'<SMS>',''),'<PN>',''),'</Email>',''),'</SMS>',''),'</PN>"',''),'|","|','"</SOLUS_PFIELD><SOLUS_PFIELD>"')),'"</SOLUS_PFIELD>"'),'"|',''),'|"','');
SELECT * from CLM_Creative;
END

