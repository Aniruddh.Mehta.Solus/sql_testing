
CREATE or replace PROCEDURE `S_CLM_Hold_Out_Refresh`()
BEGIN
DECLARE v,v1,weekly_v,weekly_v1 int;
DECLARE Exe_ID int;

SET sql_safe_updates=0;
SET foreign_key_checks=0;


INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
							values ('S_Hold_Out', '',now(),null,'Started', 090921);
SET Exe_ID = F_Get_Execution_Id();        
select * from ETL_Execution_Details Order by Execution_Id DESC LIMIT 1;

SELECT count(1) into @vExistingHoldOut from Hold_Out;
SELECT `Value` into @percentage from M_Config where `Name`='Hold_Out_Percentage';
IF @vExistingHoldOut = 0
THEN
	
	SELECT `Value` into @percentage from M_Config where `Name`='Hold_Out_Percentage';
	SELECT count(Customer_Id) into v from V_CLM_Customer_One_View;
	SELECT round(v*(@percentage/100)) into v1;
	SET @sql1=concat('insert ignore into Hold_Out (Customer_Id,Load_Execution_ID,Inserted_On)','(
    select Customer_Id,1,now() 
    FROM V_CLM_Customer_One_View 
    ORDER BY RAND() limit ',v1,
    ')');
ELSE
	SELECT count(Customer_Id) into v from V_CLM_Customer_One_View where Is_New_Cust_Flag="Y" ;
	SELECT round(v*(@percentage/100)) into v1;
	SET @sql1=concat('insert ignore into Hold_Out (Customer_Id,Load_Execution_ID,Inserted_On)','(
    select Customer_Id,1,now() 
    FROM V_CLM_Customer_One_View 
    where Is_New_Cust_Flag="Y" 
    ORDER BY RAND() limit ',v1,
    ')');
END IF;
select @sql1;
prepare stmt from @sql1;
execute stmt;
deallocate prepare stmt;
    

SELECT `Value` into @weekly_percentage from M_Config where `Name`='Weekly_Hold_Out_Percentage';
select count(t1.Customer_Id)  into weekly_v 
from V_CLM_Customer_One_View t1 left join Hold_Out t2 on t1.Customer_Id=t2.Customer_Id
where t1.Is_New_Cust_Flag="Y" and t2.Customer_Id is null ;
select round(weekly_v*(@weekly_percentage/100)) into weekly_v1;
set @sql11=concat('insert ignore into Weekly_Hold_Out (Customer_Id,Load_Execution_ID,Inserted_On)','(select t1.Customer_Id,1,now() FROM V_CLM_Customer_One_View t1 left join Hold_Out t2 on t1.Customer_Id=t2.Customer_Id where t1.Is_New_Cust_Flag="Y" and t2.Customer_Id is null ORDER BY RAND() limit ',weekly_v1,')');
prepare stmt1 from @sql11;
execute stmt1;
deallocate prepare stmt1; 

UPDATE CDM_Customer_Master set Is_New_Cust_Flag="N" WHERE Is_New_Cust_Flag <> "N";
 
update ETL_Execution_Details
set Status ='Succeeded',
End_Time = now()
where Procedure_Name='S_Hold_Out' 
and Execution_ID = Exe_ID;
select 'S_CLM_Hold_Out : COMPLETED ' as '', now() as '';

END


