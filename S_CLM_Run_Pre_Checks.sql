
CREATE OR REPLACE PROCEDURE `S_CLM_Run_Pre_Checks`()
BEGIN
DECLARE Cnt_Strt,cov1,cov2,cdv1,cdv2,rpl,cmcnt,billdayscheck,Hold_Out_Cnt,Hold_Out_Req,Current_Hold_Out_Per BIGINT DEFAULT 0;
DECLARE Exe_ID BIGINT(20);
DECLARE billcheckflag,recocheckflag,responsecheckflag,controlgroupcheckflag char(1);
DECLARE currdateoffset,maxbilldate,respdate DATE;
DECLARE Bsize BIGINT DEFAULT 1000000;

select count(distinct(Event_Execution_Date_ID)) into @CLM_Run_Cnt from Event_Execution_History where Event_Execution_Date_Id>date_sub(Current_Date(),interval 5 day);


select count(distinct(Event_Execution_Date_ID)) into @CLM_Run_Cnt from Event_Execution_History where Event_Execution_Date_Id>date_sub(Current_Date(),interval 5 day);

if @CLM_Run_Cnt>3 then
set sql_safe_updates=0;
update Solus_Internal_Config 
set `Value`='Y' 
where `Name`='CLM_Guard_Rail_Overall_Check';
else 
update Solus_Internal_Config 
set `Value`='N' 
where `Name`='CLM_Guard_Rail_Overall_Check';
end if;

SELECT `Value` into @Overall_Check_Flag from Solus_Internal_Config where `Name`='CLM_Guard_Rail_Overall_Check';

If @Overall_Check_Flag='Y' then
update Solus_Internal_Config 
set `Value`='Y'
where `Name` in ('Bill_Check_For_CLM','Response_Engine_Check_For_CLM');
else
update Solus_Internal_Config 
set `Value`='N'
where `Name` in ('Bill_Check_For_CLM','Response_Engine_Check_For_CLM');
end if;



INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
		values ('S_CLM_Run_Pre_Checks', '' ,now(),null,'Started',090921);
SELECT 
    *
FROM
    ETL_Execution_Details
ORDER BY Execution_Id DESC
LIMIT 1;
SET Exe_ID = F_Get_Execution_Id();


insert ignore into M_Config(name,value) values ('Customer_Valid_Name_Size','3');
insert ignore into M_Config(name,value) values ('Customer_Valid_Name_Default','Customer');
insert ignore into M_Config(name,value) values ('Customer_Valid_Name_OnlyCharCheck','Y');
insert ignore into M_Config(`Name`,`Value`) values ('Default_Character_Set','utf8');

SELECT 
    MIN(Customer_Id)
INTO Cnt_Strt FROM
    CDM_Customer_Master
WHERE
    Customer_Id > 0;
    
SELECT 
    IFNULL(`Value`, 'N')
INTO billcheckflag FROM
    Solus_Internal_Config
WHERE
    `Name` = 'Bill_Check_For_CLM';
    
SELECT 
    IFNULL(`Value`, 'N')
INTO recocheckflag FROM
Solus_Internal_Config 
WHERE
    `Name` = 'Reco_Check_For_CLM';


IF billcheckflag='Y'
THEN 
            SELECT ifnull(`Value`,5) INTO billdayscheck FROM Solus_Internal_Config WHERE `Name`='Bill_Check_Interval_in_Days' ;
			SELECT 
    DATE_SUB(CURRENT_DATE(),
        INTERVAL billdayscheck DAY)
INTO currdateoffset;
			SELECT 
    MAX(Bill_Date)
INTO maxbilldate FROM
    CDM_Bill_Details;
 
	IF maxbilldate<currdateoffset
	THEN
    SET @msg = concat('ERROR : Latest Bills are not loaded in CDM_Bill_Details for last x day:', billdayscheck,' Please reset flag Solus_Internal_Config.billdayscheck' );
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = @msg;
	END IF;
END IF;                            

    
SELECT 
    IFNULL(`Value`, 'N')
INTO responsecheckflag FROM
Solus_Internal_Config 
WHERE
    `Name` = 'Response_Engine_Check_For_CLM';

IF responsecheckflag='Y'
then select max(EE_Date) into respdate from CLM_Event_Dashboard;
IF datediff(Current_Date(),respdate)>7
THEN 
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR : Response Engine not run since the last 7 days. Please Reset Solus_Internal_Config.responsecheckflag, if you want to continue';
END IF;
END IF;

SELECT 
    IFNULL(`Value`, 'N')
INTO controlgroupcheckflag FROM
Solus_Internal_Config 
WHERE
    `Name` = 'CLM_Guard_Rail_Control_Group_Check';

IF controlgroupcheckflag = 'Y'
THEN
	 SELECT 
		COUNT(1)
	INTO @Hold_Out_Cnt FROM
		Hold_Out;
	SELECT 
		`Value`
	INTO @Hold_Out_Req FROM
		M_Config
	WHERE
		`Name` = 'Hold_Out_Percentage';
	SELECT 
		(100 * (SELECT 
				COUNT(1)
			FROM
				Hold_Out) / (SELECT 
				COUNT(1)
			FROM
				V_CLM_Customer_One_View))
	INTO @Current_Hold_Out_Per FROM DUAL;                    
						  
	IF @Current_Hold_Out_Per > 1.25*(@Hold_Out_Req)
	THEN 
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR : Hold Out has more customers than the specified value';
	ELSEIF @Current_Hold_Out_Per < 0.75*(@Hold_Out_Req)
	THEN 
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR : Hold Out has less customers than the specified value';
	END IF;
      
END IF;
       
END

