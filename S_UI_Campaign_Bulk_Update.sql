
CREATE OR REPLACE PROCEDURE `S_UI_Campaign_Bulk_Update`(IN in_request_json json, OUT out_result_json json)
BEGIN
    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
	declare Start_Date text;
    declare End_Date text;	
	declare IN_AGGREGATION_1 text; 
	declare STATE_ID Text;
	declare Start_Date_ID1 text;
	declare Start_Date_ID2 text;
	declare End_Date_ID1 text;
	declare End_Date_ID2 text;
	declare Event_Id text; 
	declare Start_Date_ID text;
	declare End_Date_ID text;
    declare Execution_Sequence text;
    declare Event_Limit text;
    declare Event_Start_ID text;
	declare Event_End_ID text;	
	declare Trigger_Type text;	
	declare Trigger_Channel text;
	declare Trigger_Action text;
	
	SET IN_AGGREGATION_1 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
	SET STATE_ID = json_unquote(json_extract(in_request_json,"$.STATE_ID"));
	SET Start_Date_ID1 = json_unquote(json_extract(in_request_json,"$.Start_Date_ID1"));
	SET Start_Date_ID2 = json_unquote(json_extract(in_request_json,"$.Start_Date_ID2"));
	SET End_Date_ID1 = json_unquote(json_extract(in_request_json,"$.End_Date_ID1"));
	SET End_Date_ID2 = json_unquote(json_extract(in_request_json,"$.End_Date_ID2"));
	SET Event_Id = json_unquote(json_extract(in_request_json,"$.Event_Id"));
	SET Start_Date_ID = json_unquote(json_extract(in_request_json,"$.Start_Date_ID"));
	SET End_Date_ID = json_unquote(json_extract(in_request_json,"$.End_Date_ID"));
	SET Execution_Sequence = json_unquote(json_extract(in_request_json,"$.Execution_Sequence"));
	SET Event_Limit = json_unquote(json_extract(in_request_json,"$.Event_Limit"));
	SET Event_Start_ID = json_unquote(json_extract(in_request_json,"$.Event_Start_Id"));
	SET Event_End_ID = json_unquote(json_extract(in_request_json,"$.Event_End_Id"));
	
	SET Trigger_Type = json_unquote(json_extract(in_request_json,"$.Trigger_Type"));
	SET Trigger_Channel = json_unquote(json_extract(in_request_json,"$.Trigger_Channel"));
	SET Trigger_Action = json_unquote(json_extract(in_request_json,"$.Trigger_Action"));
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));
    SET IN_SCREEN = json_unquote(json_extract(in_request_json,"$.SCREEN"));
	
	
	
    SET @STATE_ID = STATE_ID;
	Set @Event_Id=Event_Id;
	SET @Start_Date_ID =  date_format(Start_Date_ID,"%Y%m%d");
	SET @Start_Date_ID1 =  date_format(Start_Date_ID1,"%Y%m%d");
	SET @Start_Date_ID2 =  date_format(Start_Date_ID2,"%Y%m%d");
	SET @End_Date_ID =  date_format(End_Date_ID,"%Y%m%d");
	SET @End_Date_ID1 =  date_format(End_Date_ID1,"%Y%m%d");
	SET @End_Date_ID2 =  date_format(End_Date_ID2,"%Y%m%d");
	SET @Execution_Sequence =  Execution_Sequence;
	SET @temp_result = null;
	
	Select @Start_Date_ID,Start_Date_ID,@End_Date_ID,End_Date_ID,Trigger_Action;
	
	
	
	

	if @STATE_ID is null then Set @STATE_ID=''; end if;
	IF Trigger_Action is null  then Set Trigger_Action= ''; end if;
	
	If Trigger_Action ='DOWNLOAD'then Set Trigger_Action= ''; end if;
	
	if @Event_Id is null then Set @Event_Id=''; end if;
	if @Start_Date_ID is null then Set @Start_Date_ID=''; end if;
	if @End_Date_ID is null then Set @End_Date_ID=''; end if;
	if @Execution_Sequence is null then Set @Execution_Sequence=''; end if;
	
	if (Trigger_Type is null  or Trigger_Type='') then Set @Trigger_Type='CLM","GTM'; else Set @Trigger_Type= Trigger_Type; end if;
	
	if (Trigger_Channel is null or Trigger_Channel='') then Set @Trigger_Channel='Select ChannelName From CLM_Channel';  else Set @Trigger_Channel=Concat('"',Trigger_Channel,'"') ; end if;
	
	

	
	
	if @STATE_ID="DRAFT" then Set @STATE_ID='DRAFT'; end if;
	IF Trigger_Action= "Test OK" and @STATE_ID='DRAFT' then Set Trigger_Action= 'and Status_Tested="TESTED"';  end if;
	
	IF Trigger_Action = "Generate Sample Message" and @STATE_ID='DRAFT' then Set Trigger_Action= '';  end if;
	
	
	if @STATE_ID="TESTED" then Set @STATE_ID='Testing'; end if;
	IF Trigger_Action= "QC OK" and @STATE_ID='Testing' then Set Trigger_Action= 'and Status_Tested="OKTESTED"'; end if;
	IF Trigger_Action = "Send Test Message to Internal" and @STATE_ID='Testing' then Set Trigger_Action= ''; end if;

	if @STATE_ID="VERIFIED" then Set @STATE_ID='VERIFIED'; end if;
	IF Trigger_Action= "Schedule" and @STATE_ID='VERIFIED' then Set Trigger_Action= 'and Status_DLT ="DLT" and Status_QC ="QC"';  end if;
	IF Trigger_Action = "Send Test Message to Client for Go-Live" and @STATE_ID='VERIFIED' then Set Trigger_Action= ' ';  end if;
	
	if @STATE_ID="SCHEDULE" then Set @STATE_ID='Schedule'; end if;
	IF Trigger_Action= "Go Live" and @STATE_ID='Schedule' then Set Trigger_Action= ' and Status_Schedule ="Schedule"'; end if;
	IF Trigger_Action != "Go Live" and @STATE_ID='Schedule' then Set Trigger_Action= ''; end if;
	
	if @STATE_ID="LIVE" then Set @STATE_ID='Enabled'; end if;
	IF Trigger_Action= "Reschedule" and @STATE_ID='Enabled' then Set Trigger_Action= 'and  Status_Schedule ="Schedule"';  end if;
	IF Trigger_Action= "Deactivate" and @STATE_ID='Enabled' then Set Trigger_Action= 'and Status_Schedule ="Schedule"';  end if;
	IF Trigger_Action= "Execute Now" and @STATE_ID='Enabled' then Set Trigger_Action= 'and Status_Schedule ="Schedule"';  end if;	


	if @STATE_ID="INACTIVE" then Set @STATE_ID='Disabled'; end if;
	IF Trigger_Action= "Schedule" and  @STATE_ID='Disabled' then Set Trigger_Action= ' and Status_Schedule ="Schedule"'; end if;
	IF Trigger_Action= "Activate" and  @STATE_ID='Disabled' then Set Trigger_Action= 'and  Status_Schedule ="Schedule"';  end if;


	
	
	IF IN_AGGREGATION_1='Display_Trigger' 
	THEN
		
	SET @Query_String =  Concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("ID",A.ID,"Name",A.Name,"Execution_Sequence",A.Execution_Sequence,
	"Start_Date_ID",A.Start_Date_ID,"End_Date_ID",A.End_Date_ID,"Event_Limit",A.Event_Limit,"State",A.State,"Scheduled_at",B.Scheduled_at,"Coverage",B.Coverage) order by B.ModifiedDate Desc limit 30 )),"]")
	into @temp_result from Event_Master A, Event_Master_UI B where A.Id=B.Event_Id and State in ("Enabled","Disabled")	and Type_Of_Event in ("',@Trigger_Type,'")
	and ChannelName in (',@Trigger_Channel,') ',Trigger_Action,'
	
	');
    select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
	
	
			IF @temp_result is null
			THEN SET @temp_result ="[]";
			END IF;
    		
    End If;
	
	
	IF IN_AGGREGATION_1='Display_Trigger_Based_On_Filter'
	THEN
	
		If Event_End_ID =''
			Then Set Event_End_ID = NULL;
		END IF;
    
		If Event_Start_ID =''
			Then Set Event_Start_ID = NULL;
		END IF;  

		If Event_End_ID is null
			Then Set Event_Start_ID = NULL;
		END IF; 
    
		If End_Date_ID1 =''
			Then Set End_Date_ID1 = NULL;
		END IF;      
    
    
		If End_Date_ID2 =''
			Then Set End_Date_ID2 = NULL;
		END IF;      
    
    
        
		If Start_Date_ID1 =''
			Then Set Start_Date_ID1 = NULL;
		END IF;      
    
    
		If Start_Date_ID2 =''
			Then Set Start_Date_ID2 = NULL;
		END IF;      
	
	IF STATE_ID IS NULL or  STATE_ID =''
	THEN SET @Query1 = ' State in ("Enabled","Disabled")';
	     
	
	 ELSE SET @Query1 = CONCAT(' State = "',@STATE_ID,'"');
	 Select @Query1;
	End if;
	
	IF (Start_Date_ID1 IS NULL ) and (Start_Date_ID2 IS NULL )
	THEN SET @Query2 = "1=1";
	     
	
	 ELSE SET @Query2 = CONCAT(' Start_Date_ID BETWEEN  "',@Start_Date_ID1,'" and "',@Start_Date_ID2,'"');
	 Select @Query2;
	End if;
	
	IF (End_Date_ID1 IS NULL) and (End_Date_ID2 IS NULL )
	THEN SET @Query3 = "1=1";
	     
	
	 ELSE SET @Query3 = CONCAT(' End_Date_ID BETWEEN  "',@End_Date_ID1,'" and "',@End_Date_ID2,'"');
	 Select @Query3;
	End if;
	
	IF (Event_Start_ID IS NULL ) and (Event_End_ID IS NULL)
	THEN SET @Query4 = "1=1";
	 
	 ELSE SET @Query4 = CONCAT(' ID BETWEEN  ',Event_Start_ID,' and ',Event_End_ID,'');
	 Select @Query4;
	End if;

	
	Select 	@Query1,@Query2,@Query3,@Query4,Trigger_Action;	
	
	SET @Query_String = concat( 'SELECT CONCAT("[",(GROUP_CONCAT(json_object("ID",A.ID,"Name",A.Name,"Execution_Sequence",A.Execution_Sequence,
	"Start_Date_ID",A.Start_Date_ID,"End_Date_ID",A.End_Date_ID,"Event_Limit",A.Event_Limit,"State",A.State,"Scheduled_at",DATE(A.Start_Date_ID),"Coverage",B.Coverage)order by B.ModifiedDate Desc )),"]")
	into @temp_result  from Event_Master A, Event_Master_UI B where A.Id=B.Event_Id 
		AND ',@Query1,' and ',@Query2,' and ',@Query3,' and ',@Query4,' 
		and Type_Of_Event in ("',@Trigger_Type,'") and ChannelName in (',@Trigger_Channel,')',Trigger_Action,'
	;') ;
	
    select @Query_String;
	PREPARE statement from @Query_String;
	Execute statement;
	Deallocate PREPARE statement; 
	
			IF @temp_result is null
			THEN SET @temp_result ="[]";
			END IF;
    		
    End If;
	
	
	IF IN_AGGREGATION_1='Update_Trigger' 
	THEN
				Select @STATE_ID;
				IF @STATE_ID != '' 
				THEN
					IF @STATE_ID ="Enabled"
					THEN SET @STATE_ID='1';
					END IF;
					IF @STATE_ID ="Disabled"
					THEN SET @STATE_ID='0';
					END IF;
					set @view_stmt1=concat('
					UPDATE CLM_Campaign_Events A ,CLM_Campaign_Trigger B
					SET CTState = "',@STATE_ID,'"
					WHERE B.CampTriggerId=A.CampTriggerId
					AND A.EventId = "',@Event_Id,'"');
						Select@view_stmt1;
						prepare stmt1 from @view_stmt1;
						execute stmt1;
						deallocate prepare stmt1;
				END IF;		
			
				Select @Execution_Sequence;
				IF @Execution_Sequence != ''
				THEN
					set @view_stmt2=concat('
					UPDATE CLM_Campaign_Events A ,CLM_Campaign_Trigger B
					SET	A.Execution_Sequence =  "',@Execution_Sequence,'"                 
					WHERE B.CampTriggerId=A.CampTriggerId
					AND A.EventId = "',@Event_Id,'"
					
					');
						Select@view_stmt2;
						prepare stmt1 from @view_stmt2;
						execute stmt1;
						deallocate prepare stmt1;
				END IF;		
		  
				Select @Start_Date_ID,@Event_Id;
				IF  @Start_Date_ID != '' 
				THEN
						set @view_stmt3=concat('
						UPDATE CLM_Campaign_Events A ,CLM_Campaign_Trigger B
						SET TriggerStartDate =  "',@Start_Date_ID,'"            
						WHERE B.CampTriggerId=A.CampTriggerId
						AND A.EventId = "',@Event_Id,'"
						
						');
							Select@view_stmt3;
							prepare stmt1 from @view_stmt3;
							execute stmt1;
							deallocate prepare stmt1;
						
						
						set @view_stmt=concat('
						UPDATE CLM_Campaign_Events A ,CLM_Campaign B
						SET CampaignStartDate = "',@Start_Date_ID,'"          
						WHERE B.CampaignId=A.CampaignId
						AND A.EventId= "',@Event_Id,'"
						
						');
							Select@view_stmt;
							prepare stmt1 from @view_stmt;
							execute stmt1;
							deallocate prepare stmt1;	
				END IF;	

				Select @End_Date_ID;	
				IF @End_Date_ID != ''
				THEN
					set @view_stmt4=concat('
					UPDATE CLM_Campaign_Events A ,CLM_Campaign_Trigger B
					SET            
					TriggerEndDate   =    "',@End_Date_ID,'"                 
					WHERE B.CampTriggerId=A.CampTriggerId
					AND A.EventId = "',@Event_Id,'"
					
					');
						Select@view_stmt4;
						prepare stmt1 from @view_stmt4;
						execute stmt1;
						deallocate prepare stmt1;
						
					set @view_stmt=concat('UPDATE CLM_Campaign_Events A ,CLM_Campaign B
											SET 
											CampaignEndDate =  "',@End_Date_ID,'"              
											WHERE B.CampaignId=A.CampaignId
											AND A.EventId= "',@Event_Id,'"
										');
					Select@view_stmt;
					prepare stmt1 from @view_stmt;
					execute stmt1;
					deallocate prepare stmt1;	
				END IF;		
					
					
					IF Event_Limit != '' 
				THEN
					set @view_stmt1=concat('
					UPDATE CLM_Campaign_Events A ,CLM_Campaign_Trigger B
					SET Event_Limit = "',Event_Limit,'"
					WHERE B.CampTriggerId=A.CampTriggerId
					AND A.EventId = "',@Event_Id,'"');
						Select@view_stmt1;
						prepare stmt1 from @view_stmt1;
						execute stmt1;
						deallocate prepare stmt1;
				END IF;		
			 
		 SELECT CONCAT("[",(GROUP_CONCAT(json_object("Message","SAVED") )),"]") into @temp_result  ;
		 SELECT @temp_result;
		 
		
			IF @temp_result is null
			THEN SET @temp_result ="[]";
			END IF;
		
    		
    End If;
	
	
		IF IN_AGGREGATION_1='Download' or IN_AGGREGATION_1='DOWNLOAD'
				THEN
					CREATE or replace VIEW `Bulkload_Download` AS
											SELECT 
												`A`.`ID` AS `ID`,
												`A`.`State` AS `State`,
												`A`.`Waterfall` AS `Waterfall`,
												`A`.`ChannelName` AS `ChannelName`,
												`A`.`CLMSegmentName` AS `CLMSegmentName`,
												`A`.`Execution_Sequence` AS `Execution_Sequence`,
												`A`.`Updated_On` AS `Updated_On`,
												`A`.`EventId` AS `EventId`,
												`A`.`CampTriggerId` AS `CampTriggerId`,
												`A`.`CreativeId1` AS `CreativeId1`,
												`A`.`CampaignId` AS `CampaignId`,
												`A`.`RankedPickListStory_Id` AS `RankedPickListStory_Id`,
												`A`.`Communication_Cooloff` AS `Communication_Cooloff`,
												`A`.`Event_Limit` AS `Event_Limit`,
												`A`.`Name` AS `Name`,
												`A`.`ExtCreativeKey` AS `ExtCreativeKey`,
												`A`.`SenderKey` AS `SenderKey`,
												`A`.`Timeslot_Name` AS `Timeslot_Name`,
												`A`.`Timeslot_Id` AS `Timeslot_Id`,
												`A`.`Number_Of_Recommendation` AS `Number_Of_Recommendation`,
												`A`.`Start_Date_ID` AS `Start_Date_ID`,
												`A`.`End_Date_ID` AS `End_Date_ID`,
												`A`.`Creative1` AS `Creative1`,
												`A`.`Replaced_Query` AS `Replaced_Query`,
												`A`.`Website_URL` AS `Website_URL`,
												`A`.`CampaignThemeName` AS `CampaignThemeName`,
												`A`.`MicroSegmentName` AS `MicroSegmentName`,
												`A`.`CampaignName` AS `CampaignName`,
												`A`.`Description` AS `Description`,
												`A`.`Replaced_Query_Bill` AS `Replaced_Query_Bill`,
												`A`.`Recommendation_Filter_Logic` AS `Recommendation_Filter_Logic`,
												`A`.`Event_Control_Group_Percentage` AS `Event_Control_Group_Percentage`,
												`A`.`Communication_Template_ID` AS `Communication_Template_ID`,
												`A`.`Inserted_On` AS `Inserted_On`,
												`A`.`Event_Type_ID` AS `Event_Type_ID`,
												`A`.`Mission_ID` AS `Mission_ID`,
												`A`.`CLMSegmentId` AS `CLMSegmentId`,
												`A`.`Event_Response_Days` AS `Event_Response_Days`,
												`A`.`Reminder_Parent_Event_Id` AS `Reminder_Parent_Event_Id`,
												`A`.`Reminder_Days` AS `Reminder_Days`,
												`A`.`Arms` AS `Arms`,
												`A`.`Is_MAB_Driven` AS `Is_MAB_Driven`,
												`A`.`Follows_Waterfall` AS `Follows_Waterfall`,
												`A`.`Is_Good_Time` AS `Is_Good_Time`,
												`A`.`Is_On_Demand` AS `Is_On_Demand`,
												`A`.`Condition` AS `Condition`,
												`A`.`No_Of_Personalization` AS `No_Of_Personalization`,
												`A`.`Needs_ControlGroup` AS `Needs_ControlGroup`,
												`A`.`Offer_Code1` AS `Offer_Code1`,
												`B`.`Event_ID` AS `Event_ID`,
												`B`.`No_Of_Communication_Sent` AS `No_Of_Communication_Sent`,
												`B`.`No_Of_Communication_Rejected` AS `No_Of_Communication_Rejected`,
												`B`.`Event_Execution_Date_Id` AS `Event_Execution_Date_Id`
												
											FROM
												(`Event_Master` `A`
												JOIN `Event_Master_UI` `B`)
											WHERE
												`A`.`ID` = `B`.`Event_ID`;
												
			call S_dashboard_performance_download('Bulkload_Download',@out_result_json1);									
			select @out_result_json1 into @temp_result;									
												
												
												
												
				END IF;	
	

	
  if @temp_result is null
		Then
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") into @temp_result; ') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
		End If;	


 SET out_result_json = @temp_result;  

END

