
CREATE OR REPLACE PROCEDURE `S_CLM_Personalized_Communication`(IN OnDemandEventId BIGINT(20),IN RUN_MODE Varchar(20),OUT vSuccess int (4), OUT vErrMsg Text)
Begin

	DECLARE vStart_CntBills BIGINT DEFAULT 0;
    DECLARE vEnd_CntBills BIGINT DEFAULT 0;
	DECLARE vBatchSizeBills BIGINT DEFAULT 100000;
	DECLARE vRec_CntBills BIGINT DEFAULT 0;

	DECLARE vStart_CntCust BIGINT DEFAULT 0;
    DECLARE vEnd_CntCust BIGINT DEFAULT 0;
	DECLARE vBatchSizeCust BIGINT DEFAULT 100000;
	DECLARE vRec_CntCust BIGINT DEFAULT 0;
	DECLARE Load_Exe_ID, Exe_ID int;
	Declare dataloop INT DEFAULT FALSE;
    Declare y longtext;


Declare cur1 CURSOR for 
select distinct concat(substring(a,1,7),concat(cast(b.ID as char),',',substring(replace(a,'|',','),8),b.ID ) )from (
select ctm.ID,concat('select cdv.Customer_Id,ct.Communication_Channel,ee.Event_Execution_ID,concat(',
ctm.Template,') from Customer_Details_View cdv,Event_Execution ee,Communication_Template ct where 
ee.Customer_Id=cdv.Customer_Id   and ct.ID=ee.Communication_Template_ID and ee.In_Control_Group is null and ee.In_Event_Control_Group is null and ee.Is_Target="Y"',' and  ee.Communication_Template_ID=') as a from
 Communication_Template ctm,Event_Master em where
 ctm.ID=em.Communication_Template_ID 
 and ctm.ID in (select distinct Communication_Template_ID from  Event_Execution where Is_Target='Y')
 and Case when RUN_MODE not in ('TEST','RUN' ) 
				then em.ID in (Select ID from Event_Master where State='Enabled')   
			when RUN_MODE='TEST'  
			    then em.ID =OnDemandEventId and  em. State in ('Enabled' ,'Testing')
            when  RUN_MODE='RUN' 
			then em.ID =OnDemandEventId and  em. State in ('Enabled')
			end
) b;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET dataloop = TRUE;
DECLARE CONTINUE HANDLER FOR SQLWARNING
BEGIN
	GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
	SET @logmsg=CONCAT(ifnull(@errsqlstate,'WARNING') , ifnull(@errno,'1'), ifnull(@errtext,'WARNING MESSAGE'));
    INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
		values ('S_CLM_Personalized_Communication', @logmsg ,now(),null,@errno, ifnull(Load_Exe_ID,1));
    SELECT 'S_CLM_Personalized_Communication : Warning Message :' as '', @logmsg as '';
END;
DECLARE EXIT HANDLER FOR SQLEXCEPTION
BEGIN
	GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
	SET @logmsg=CONCAT(ifnull(@errsqlstate,'ERROR') , ifnull(@errno,'1'), ifnull(@errtext,'ERROR MESSAGE'));
    INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
		values ('S_CLM_Personalized_Communication', @logmsg ,now(),null,@errno, ifnull(Load_Exe_ID,1));
    SELECT 'S_CLM_Personalized_Communication : Error Message :' as '', @logmsg as '';
END;
SET foreign_key_checks=0;
SET Load_Exe_ID = F_Get_Load_Execution_Id();
SET Exe_ID = F_Get_Execution_Id();
SET sql_safe_updates=0;
SET foreign_key_checks=0;
insert ignore into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
values ('S_CLM_Personalized_Communication', '',now(),null,'Started', Load_Exe_ID);
select 'S_CLM_Personalized_Communication : STARTED ' as '', NOW() as '' ,' Load Id : ' as '', Load_Exe_ID as ''; 

SELECT   `Value` INTO @V_Tenant_Name FROM  M_Config WHERE `Name` = 'Tenant_Name'   ;
TRUNCATE table TEMP_Customer_Communication;
SELECT   Customer_id INTO vRec_CntCust FROM    V_CLM_Customer_One_View ORDER BY Customer_id DESC LIMIT 1; 
OPEN cur1;
    new_loop: LOOP
    set y=''; 
    FETCH cur1 into y;
	IF dataloop THEN
             LEAVE new_loop;
    END IF;
	SET @SQLQuery='';
	SET @sql1=replace(y,"Offer_Code","ee.Offer_Code");
	SET @sql2=replace(@sql1,"Customer_Details_View",concat(@V_Tenant_Name,'.','Customer_Details_View'));
	SET y=@sql2;
	SET vStart_CntCust = 0;
	SET vEnd_CntCust = 0; 
	SOLUS_CUSTOMER_LOOP: LOOP
		SET vEnd_CntCust = vStart_CntCust + vBatchSizeCust;
		insert into log_solus(module,rec_start,rec_end) values ('S_CLM_Personalized_Communication',vStart_CntCust,vStart_CntCust);
		SET @SQLQuery=concat('insert ignore into TEMP_Customer_Communication (Communication_Template_ID,Customer_Id,Communication_Channel,Event_Execution_ID,Message) (',y,' and  ee.Customer_Id between ',vStart_CntCust,' and ',vEnd_CntCust,')');

         
		IF vStart_CntCust <= 0
        THEN
            	select 'S_CLM_Personalized_Communication : ' as '', NOW() as '', '  <SQL> ' as '', @SQLQuery as '',' </SQL> ' as '';
        END IF;
		

		PREPARE stmt from @SQLQuery;        
		execute stmt;
		deallocate prepare stmt; 
        
		UPDATE TEMP_Customer_Communication 
		SET 
			SMS_Message = (SELECT 
					SUBSTRING_INDEX(SUBSTRING_INDEX(Message, '<SMS>', - 1),
								'</SMS>',
								1)
				),
			Email_Message = (SELECT 
					SUBSTRING_INDEX(SUBSTRING_INDEX(Message, '<Email>', - 1),
								'</Email>',
								1)
				),
			PN_Message = (SELECT 
					SUBSTRING_INDEX(SUBSTRING_INDEX(Message, '<PN>', - 1),
								'</PN>',
								1)
				),
			WhatsApp_Message = (SELECT 
					SUBSTRING_INDEX(SUBSTRING_INDEX(Message, '<WhatsApp>', - 1),
								'</WhatsApp>',
								1)
				)
		WHERE
			Customer_Id BETWEEN vStart_CntCust AND vEnd_CntCust;
            
		IF vEnd_CntCust  >= vRec_CntCust THEN
			LEAVE SOLUS_CUSTOMER_LOOP;
		END IF;
		SET vStart_CntCust = vStart_CntCust + vBatchSizeCust;
		END LOOP SOLUS_CUSTOMER_LOOP;
END LOOP;
CLOSE cur1;

SET vStart_CntCust = 0;
SET vEnd_CntCust = 0; 
SOLUS_CUSTOMER_LOOP2: LOOP
	SET vEnd_CntCust = vStart_CntCust + vBatchSizeCust;
	insert into log_solus(module,rec_start,rec_end) values ('S_CLM_Personalized_Communication2',vStart_CntCust,vStart_CntCust);

	UPDATE Event_Execution eeh,
		TEMP_Customer_Communication tcc 
	SET 
		eeh.Message = tcc.Message,
		eeh.Email_Message = tcc.Email_Message,
		eeh.SMS_Message = tcc.SMS_Message,
		eeh.PN_Message = tcc.PN_Message,
		eeh.WhatsApp_Message = tcc.WhatsApp_Message
	WHERE
		eeh.Event_Execution_ID = tcc.Event_Execution_ID
	AND eeh.Customer_Id BETWEEN vStart_CntCust AND vEnd_CntCust;

	IF vEnd_CntCust  >= vRec_CntCust THEN
		LEAVE SOLUS_CUSTOMER_LOOP2;
	END IF;
	SET vStart_CntCust = vStart_CntCust + vBatchSizeCust;
END LOOP SOLUS_CUSTOMER_LOOP2;
		
Select Count(1) into @vCustomer_Count from TEMP_Customer_Communication;

If 	@vCustomer_Count =0 Then
SET vErrMsg = CONCAT('\r\n','No Customer Selected Please check the Template properly ');
SET vSuccess = 0;

ELSE
SET vSuccess = 1;
	SET vErrMsg = '';
	End if;

UPDATE ETL_Execution_Details 
SET 
    Status = 'Succeeded',
    End_Time = NOW()
WHERE
    Procedure_Name = 'S_CLM_Personalized_Communication'
        AND Load_Execution_ID = Load_Exe_ID
        AND Execution_ID = Exe_ID;    
SELECT 'S_CLM_Personalized_Communication : COMPLETED ' as '', NOW() as '' ,' Load Id : ' as '', Load_Exe_ID as ''; 
END

