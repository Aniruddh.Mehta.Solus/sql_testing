
CREATE or replace PROCEDURE `PROC_UI_CLM_Test`(
IN OnDemandEventId BIGINT(20),             
 OUT no_of_customers BIGINT(20),           
 OUT no_of_customers_final BIGINT(20),     
 OUT Filename Text ,                       
 OUT FileHeader Text,                      
 Out FileLocation Text,                    
 OUT RecoFilename Text,                    
 OUT RecoFileHeader Text,                  
 OUT RecoFileRecoHeader Text,              
 OUT RecoFileLocation Text,                 
OUT vSuccess int (4),                      
OUT vErrMsg Text                           
)
Begin
CALL S_CLM_Run(OnDemandEventId,'TEST', @no_of_customers,@no_of_customers_final, @Filename,@FileHeader,@FileLocation,@RecoFilename,@RecoFileHeader,
 @RecoFileRecoHeader,@RecoFileLocation,@vSuccess,@vErrMsg);
 
 select @no_of_customers,@no_of_customers_final, @Filename,@FileHeader,@FileLocation,@RecoFilename,@RecoFileHeader,
 @RecoFileRecoHeader,@RecoFileLocation,@vSuccess,@vErrMsg;
select @no_of_customers INTO no_of_customers;
select @no_of_customers_final INTO no_of_customers_final;
select @Filename INTO Filename;
select @FileHeader INTO @FileHeader;
select @FileLocation INTO FileLocation;                    
select '' into RecoFilename;                    
select '' into RecoFileHeader ;                  
select '' into RecoFileRecoHeader ;            
select '' into RecoFileLocation ;                 
select 1 into vSuccess;
select @vErrMsg into vErrMsg;                           
END

