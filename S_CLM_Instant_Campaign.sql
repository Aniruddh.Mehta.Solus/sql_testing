
CREATE or replace PROCEDURE `S_CLM_Instant_Campaign`(IN CampaignName varchar(128),
OUT Out_FileName TEXT, 
OUT Out_FileDelimiter VARCHAR(10)

)
BEGIN

DECLARE vChannel varchar(128);
DECLARE vMessage varchar(1024);
DECLARE vRun_Mode varchar(128);
DECLARE vControlGroup decimal(5,2);


SELECT  Channel, Creative, LifeCycle_Segment, Campaign_Segment, Throttling,Header_Text,Header_Column,Downstream_Type
FROM Event_Master_Instant_Campaign where Trigger_Name = CampaignName INTO 
@vChannel,@Message,@CLMSegmentName,@CampaignSegName,@Throttling,@Header_Text,@Header_Column,@Downstream_Type;


set vChannel = @vChannel;
set @vTriggerName = CampaignName;
set @validCLMSeg = @CLMSegmentName;
set @validCampSeg = @CampaignSegName;

set @Header_Text = CONCAT('"', REPLACE(@Header_Text, ',', '","'), '"');
set @Header_Column = replace(@Header_Column,'Customer_Key','c.Customer_Key');
set @Header_Column = replace(@Header_Column,'Communication_Channel','d.Channel');
set @Event_Exec_id = 'concat(b.Customer_id,"-",curdate(),"-",d.Channel)';
set @Header_Column = replace(@Header_Column,'Event_Execution_Id',@Event_Exec_id);
set @Header_Column = replace(@Header_Column,'Message','d.Creative');
set @Header_Column = replace(@Header_Column,'Event_Id','d.Id');


set @Throttling = cast(@Throttling as int);


if @CLMSegmentName = '' or @CLMSegmentName = 'NA' or @CLMSegmentName = 'Na' 
then 
set @CLMSegmentName = 'LC_All';
end if;

if @CampaignSegName = '' or @CampaignSegName = 'NA' or @CampaignSegName = 'Na'
then set @CampaignSegName = 'CM_All';
end if;


if  @CLMSegmentName != 'LC_All' 
then 
set @CLMSegmentName = concat('LC_',@validCLMSeg);
end if;



if @CampaignSegName != 'CM_All'
then
set @CampaignSegName = concat('CM_',@validCampSeg);
end if;

SELECT 
    IFNULL(Customer_Id, 0)
INTO @Start_Cnt FROM
    CDM_Customer_Master
ORDER BY Customer_Id ASC
LIMIT 1;

SELECT 
    IFNULL(Customer_Id, 0)
INTO @Rec_Cnt FROM
    CDM_Customer_Master
ORDER BY Customer_Id DESC
LIMIT 1;


set @BatchSize=10000;
set @End_Cnt = 0;
set @vdate = replace(curdate(),'-','');


set @Out_FileDelimiter = '|';
set @Enclosed_Character1 = "'";
set @Enclosed_Character2 = '"';

SET @File_Path= concat((select value from M_Config where Name='File_Path'));

set @totalcount=0;

if (@Throttling > 0) and (@Throttling <= @BatchSize)
then 
set @N = @Throttling;
end if;

if @Throttling < 1 or @Throttling >= @BatchSize
then
set @N = 100000000;
end if;


SELECT @Message;


if @Downstream_Type not like  'SFTP'
then 


PROCESS_LOOP: LOOP
SET @End_Cnt = @Start_Cnt + @BatchSize;
set @outcount = 0;

SELECT 
    CONCAT(@File_Path,
            '/',
            'Instant_Campaign/Target',
            '/',
            (SELECT 
                    value
                FROM
                    M_Config
                WHERE
                    Name = 'File_Name_Prefix'),
            '_',
            @vChannel,
            '_',
            @vTriggerName,
            '_',
            @vdate,
            '_',
            @End_Cnt,
            '.',
            'csv')
INTO @Out_FileName;


if (vChannel = 'SMS') 
then


set @view_stmt1=concat('select "Event_Execution_ID","Customer_Key","Communication_Channel","Mobile","Message"
union all select concat(b.Customer_id,"-",',@vdate,',"-",','"',vChannel,'"','),c.Customer_Key,','"',vChannel,'"',',b.Mobile,','  replace("',@Message,'","<SOLUS_PFIELD>Customer_Name</SOLUS_PFIELD>",b.First_Name)',' from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
 where  a.Customer_Id1 not in (select Customer_Id from Hold_Out) and a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.',@CLMSegmentName,' = 1 and a.',@CampaignSegName,' = 1 and length(b.Mobile) >= 10  and a.Customer_Id1 between ' ,@Start_Cnt, ' and ' ,@End_Cnt, '   limit ',@N,'  INTO OUTFILE ','"',@Out_FileName,'"',' FIELDS TERMINATED BY ','"',@Out_FileDelimiter,'"',' OPTIONALLY ENCLOSED BY ',@Enclosed_Character1,@Enclosed_Character2,@Enclosed_Character1, ' escaped by "\" LINES TERMINATED BY "\n" ;'
);

SELECT @view_stmt1;

PREPARE statement1 from @view_stmt1;
Execute statement1;
Deallocate PREPARE statement1;



set @cnt=concat("select count(1) into @outcount from ( select CUstomer_Id1 from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
 where a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.",@CLMSegmentName," = 1 and a.",@CampaignSegName," = 1 and length(b.Mobile) >= 10 and a.Customer_Id1 between " ,@Start_Cnt, " and " ,@End_Cnt, " limit ",@N,") as t;");


PREPARE statement5 from @cnt;
Execute statement5;
Deallocate PREPARE statement5;


set @totalcount = @totalcount + @outcount;
if  (@totalcount> @Throttling*1.1 and @Throttling > 1 )
then
LEAVE PROCESS_LOOP;
end if;

 


SELECT 
    CONCAT(@File_Path,
            '/',
            'Instant_Campaign',
            '/Control/',
            (SELECT 
                    value
                FROM
                    M_Config
                WHERE
                    Name = 'File_Name_Prefix'),
            '_',
            @vChannel,
            '_',
            @vTriggerName,
            '_',
            @vdate,
            '_',
            @End_Cnt,
            '.',
            'csv')
INTO @Out_FileName;


set @view_stmt_control1=concat('select "Event_Execution_ID","Customer_Key","Communication_Channel","Message","Mobile"
union all select concat(b.Customer_id,"-",',@vdate,',"-",','"',vChannel,'"','),c.Customer_Key,','"',vChannel,'"',
',','  replace("',@Message,'","<SOLUS_PFIELD>Customer_Name</SOLUS_PFIELD>",b.First_Name)',',b.Mobile from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
 where  a.Customer_Id1 in (select Customer_Id from Hold_Out) and  a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.',@CLMSegmentName,' = 1 and a.',@CampaignSegName,' = 1  and length(b.Mobile) >= 10 and a.Customer_Id1 between ' ,@Start_Cnt, ' and ' ,@End_Cnt, '  limit ',@N,' 
INTO OUTFILE ','"',@Out_FileName,'"',' FIELDS TERMINATED BY ','"',@Out_FileDelimiter,'"',' OPTIONALLY ENCLOSED BY ',@Enclosed_Character1,@Enclosed_Character2,@Enclosed_Character1,' escaped by "\" LINES TERMINATED BY "\n" ;'
);


PREPARE statement1 from @view_stmt_control1;
Execute statement1;
Deallocate PREPARE statement1;


end if;




if (vChannel = 'WhatsApp') 
then


set @view_stmt1=concat('select "Customer_Key","First_Name","Mobile","Message"
 union all select c.Customer_Key,b.First_Name,b.Mobile,',' concat(concat(b.Customer_id,"-",',@vdate,',"-",','"',vChannel,'"','),"|", replace("',@Message,'","<SOLUS_PFIELD>Customer_Name</SOLUS_PFIELD>",b.First_Name))',' from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
 where   a.Customer_Id1 not in (select Customer_Id from Hold_Out) and a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.',@CLMSegmentName,' = 1 and a.',@CampaignSegName,' = 1 and length(b.Mobile) >= 10  and a.Customer_Id1 between ' ,@Start_Cnt, ' and ' ,@End_Cnt, '   limit ',@N,'  INTO OUTFILE ','"',@Out_FileName,'"',' FIELDS TERMINATED BY ','"',@Out_FileDelimiter,'"',' OPTIONALLY ENCLOSED BY ',@Enclosed_Character1,@Enclosed_Character2,@Enclosed_Character1, ' escaped by "\" LINES TERMINATED BY "\n" ;'
);


PREPARE statement1 from @view_stmt1;
Execute statement1;
Deallocate PREPARE statement1;
SELECT @Out_FileName;


set @cnt=concat("select count(1) into @outcount from ( select CUstomer_Id1 from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
  where a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.",@CLMSegmentName," = 1 and a.",@CampaignSegName," = 1  and length(b.Mobile) >= 10 and a.Customer_Id1 between " ,@Start_Cnt, " and " ,@End_Cnt, " limit ",@N,") as t;");


PREPARE statement5 from @cnt;
Execute statement5;
Deallocate PREPARE statement5;


set @totalcount = @totalcount + @outcount;
if  (@totalcount> @Throttling*1.1 and @Throttling > 1 )
then
LEAVE PROCESS_LOOP;
end if;

 


SELECT 
    CONCAT(@File_Path,
            '/',
            'Instant_Campaign',
            '/Control/',
            (SELECT 
                    value
                FROM
                    M_Config
                WHERE
                    Name = 'File_Name_Prefix'),
            '_',
            @vChannel,
            '_',
            @vTriggerName,
            '_',
            @vdate,
            '_',
            @End_Cnt,
            '.',
            'csv')
INTO @Out_FileName;


set @view_stmt_control1=concat('select "Customer_Key","First_Name","Mobile","Message"
 union all select c.Customer_Key,b.First_Name,b.Mobile,',' concat(concat(b.Customer_id,"-",',@vdate,',"-",','"',vChannel,'"','),"|", replace("',@Message,'","<SOLUS_PFIELD>Customer_Name</SOLUS_PFIELD>",b.First_Name))',' from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
 where   a.Customer_Id1 in (select Customer_Id from Hold_Out) and a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.',@CLMSegmentName,' = 1 and a.',@CampaignSegName,' = 1  and length(b.Mobile) >= 10  and a.Customer_Id1 between ' ,@Start_Cnt, ' and ' ,@End_Cnt, '   limit ',@N,'  INTO OUTFILE ','"',@Out_FileName,'"',' FIELDS TERMINATED BY ','"',@Out_FileDelimiter,'"',' OPTIONALLY ENCLOSED BY ',@Enclosed_Character1,@Enclosed_Character2,@Enclosed_Character1, ' escaped by "\" LINES TERMINATED BY "\n" ;'
);



 PREPARE statement1 from @view_stmt_control1;
 Execute statement1;
 Deallocate PREPARE statement1;



end if;





if (vChannel = 'Email') 
then


set @view_stmt1=concat('select "Customer_Key","First_Name","Last_Name","Mobile","product_key","product_key","product_key","product_key","product_key","Event_Execution_ID","Event_ID","Event_Execution_Date_ID","Customer_Id","Is_Target","Exclusion_Filter_ID","Communication_Template_ID","Communication_Channel","Message","Email_Message|SMS_Message|PN_Message|WhatsApp_Message|In_Control_Group|In_Event_Control_Group|LT_Control_Covers_Response_Days|ST_Control_Covers_Response_Days|Offer_Code|GV_Code|E_Voucher_Code|Booklet_Code|Is_MAB_Event|Campaign_Key|Segment_Id|Revenue_Center|Recommendation_Genome_Lov_Id_1|Recommendation_Genome_Lov_Id_2|Recommendation_Genome_Lov_Id_3|Recommendation_Genome_Lov_Id_4|Recommendation_Genome_Lov_Id_5|Recommendation_Genome_Lov_Id_6|Recommendation_Product_Id_1|Recommendation_Product_Id_2|Recommendation_Product_Id_3|Recommendation_Product_Id_4|Recommendation_Product_Id_5|Recommendation_Product_Id_6|Run_Mode|Microsite_URL|CMAB_Eligible_Arms|Created_Date|Recommendation_Algo_1|Recommendation_Algo_2|Recommendation_Algo_3|Recommendation_Algo_4|Recommendation_Algo_5|Recommendation_Algo_6" union all select c.Customer_Key,b.First_Name,"null",b.mobile,"null","null","null","null","null","null","null","null","null","null","null","999","EMAIL",replace("',@Message,'","<SOLUS_PFIELD>Customer_Name</SOLUS_PFIELD>",b.First_Name),"null"
 from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
 where  a.Customer_Id1 not in (select Customer_Id from Hold_Out) and  a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.',@CLMSegmentName,' = 1 and a.',@CampaignSegName,' = 1  and b.Email != "" and a.Customer_Id1 between ' ,@Start_Cnt, ' and ' ,@End_Cnt, '  limit ',@N,' 
INTO OUTFILE ','"',@Out_FileName,'"',' FIELDS TERMINATED BY ','"',@Out_FileDelimiter,'"',' OPTIONALLY ENCLOSED BY ',@Enclosed_Character1,@Enclosed_Character2,@Enclosed_Character1,' escaped by "\" LINES TERMINATED BY "\n" ;'
);
 

 
set @cnt=concat("select count(1) into @outcount from (select Customer_id1 from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
 where a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.",@CLMSegmentName," = 1 and a.",@CampaignSegName," = 1  and b.Email != '' and a.Customer_Id1 between " ,@Start_Cnt, " and " ,@End_Cnt, " limit ",@N,") as t;");
 

 PREPARE statement5 from @cnt;
Execute statement5;
Deallocate PREPARE statement5;


if @outcount > 1 then
PREPARE statement1 from @view_stmt1;
Execute statement1;
Deallocate PREPARE statement1;
end if;


SELECT @Out_FileName;
set @totalcount = @totalcount + @outcount;


if  (@totalcount> @Throttling*1.1 and @Throttling > 1 )
then
LEAVE PROCESS_LOOP;
end if;



SELECT 
    CONCAT(@File_Path,
            '/',
            'Instant_Campaign',
            '/Control/',
            (SELECT 
                    value
                FROM
                    M_Config
                WHERE
                    Name = 'File_Name_Prefix'),
            '_',
            @vChannel,
            '_',
            @vTriggerName,
            '_',
            @vdate,
            '_',
            @End_Cnt,
            '.',
            'csv')
INTO @Out_FileName;


set @view_stmt_control1=concat('select "Customer_Key","First_Name","Last_Name","Mobile","product_key","product_key","product_key","product_key","product_key","Event_Execution_ID","Event_ID","Event_Execution_Date_ID","Customer_Id","Is_Target","Exclusion_Filter_ID","Communication_Template_ID","Communication_Channel","Message","Email_Message|SMS_Message|PN_Message|WhatsApp_Message|In_Control_Group|In_Event_Control_Group|LT_Control_Covers_Response_Days|ST_Control_Covers_Response_Days|Offer_Code|GV_Code|E_Voucher_Code|Booklet_Code|Is_MAB_Event|Campaign_Key|Segment_Id|Revenue_Center|Recommendation_Genome_Lov_Id_1|Recommendation_Genome_Lov_Id_2|Recommendation_Genome_Lov_Id_3|Recommendation_Genome_Lov_Id_4|Recommendation_Genome_Lov_Id_5|Recommendation_Genome_Lov_Id_6|Recommendation_Product_Id_1|Recommendation_Product_Id_2|Recommendation_Product_Id_3|Recommendation_Product_Id_4|Recommendation_Product_Id_5|Recommendation_Product_Id_6|Run_Mode|Microsite_URL|CMAB_Eligible_Arms|Created_Date|Recommendation_Algo_1|Recommendation_Algo_2|Recommendation_Algo_3|Recommendation_Algo_4|Recommendation_Algo_5|Recommendation_Algo_6" union all select c.Customer_Key,b.First_Name,"null",b.mobile,"null","null","null","null","null","null","null","null","null","null","null","999","EMAIL",replace("',@Message,'","<SOLUS_PFIELD>Customer_Name</SOLUS_PFIELD>",b.First_Name),"null"
 from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
 where  a.Customer_Id1 in (select Customer_Id from Hold_Out) and  a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.',@CLMSegmentName,' = 1 and a.',@CampaignSegName,' = 1 and b.Email != "" and a.Customer_Id1 between ' ,@Start_Cnt, ' and ' ,@End_Cnt, '  limit ',@N,' 
INTO OUTFILE ','"',@Out_FileName,'"',' FIELDS TERMINATED BY ','"',@Out_FileDelimiter,'"',' OPTIONALLY ENCLOSED BY ',@Enclosed_Character1,@Enclosed_Character2,@Enclosed_Character1,' escaped by "\" LINES TERMINATED BY "\n" ;'
);
 

PREPARE statement1 from @view_stmt_control1;
Execute statement1;
Deallocate PREPARE statement1;

end if;

SET @Start_Cnt = @End_Cnt + 1;

IF @Start_Cnt  >= @Rec_Cnt THEN
	LEAVE PROCESS_LOOP;
END IF;        

END LOOP PROCESS_LOOP;  




elseif @Downstream_Type = 'SFTP'
then


PROCESS_LOOP: LOOP
SET @End_Cnt = @Start_Cnt + @BatchSize;
set @outcount = 0;

SELECT 
    CONCAT(@File_Path,
            '/',
            'Instant_Campaign/Target',
            '/',
            (SELECT 
                    value
                FROM
                    M_Config
                WHERE
                    Name = 'File_Name_Prefix'),
            '_',
            @vChannel,
            '_',
            @vTriggerName,
            '_',
            @vdate,
            '_',
            @End_Cnt,
            '.',
            'csv')
INTO @Out_FileName;



if (vChannel = 'SMS') 
then

select @Header_Text;
select @Header_Column;

set @view_stmt1=concat('select ',@Header_Text,'
union all select ',@Header_Column,'  from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
, Event_Master_Instant_Campaign d
 where  d.Trigger_Name = ','"',CampaignName,'"',' and  a.Customer_Id1 not in (select Customer_Id from Hold_Out) and a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.',@CLMSegmentName,' = 1 and a.',@CampaignSegName,' = 1 and length(b.Mobile) >= 10  and a.Customer_Id1 between ' ,@Start_Cnt, ' and ' ,@End_Cnt, '   limit ',@N,'  INTO OUTFILE ','"',@Out_FileName,'"',' FIELDS TERMINATED BY ','"',@Out_FileDelimiter,'"',' OPTIONALLY ENCLOSED BY ',@Enclosed_Character1,@Enclosed_Character2,@Enclosed_Character1, ' escaped by "\" LINES TERMINATED BY "\n" ;'
);

SELECT @view_stmt1;

PREPARE statement1 from @view_stmt1;
Execute statement1;
Deallocate PREPARE statement1;



set @cnt=concat("select count(1) into @outcount from ( select CUstomer_Id1 from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
 where a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.",@CLMSegmentName," = 1 and a.",@CampaignSegName," = 1 and length(b.Mobile) >= 10 and a.Customer_Id1 between " ,@Start_Cnt, " and " ,@End_Cnt, " limit ",@N,") as t;");


PREPARE statement5 from @cnt;
Execute statement5;
Deallocate PREPARE statement5;


set @totalcount = @totalcount + @outcount;
if  (@totalcount> @Throttling*1.1 and @Throttling > 1 )
then
LEAVE PROCESS_LOOP;
end if;

 


SELECT 
    CONCAT(@File_Path,
            '/',
            'Instant_Campaign',
            '/Control/',
            (SELECT 
                    value
                FROM
                    M_Config
                WHERE
                    Name = 'File_Name_Prefix'),
            '_',
            @vChannel,
            '_',
            @vTriggerName,
            '_',
            @vdate,
            '_',
            @End_Cnt,
            '.',
            'csv')
INTO @Out_FileName;


set @view_stmt_control1=concat('select ',@Header_Text,'
union all select ',@Header_Column,'  from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
, Event_Master_Instant_Campaign d
 where  d.Trigger_Name = ','"',CampaignName,'"',' and  a.Customer_Id1 in (select Customer_Id from Hold_Out) and  a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.',@CLMSegmentName,' = 1 and a.',@CampaignSegName,' = 1  and length(b.Mobile) >= 10 and a.Customer_Id1 between ' ,@Start_Cnt, ' and ' ,@End_Cnt, '  limit ',@N,' 
INTO OUTFILE ','"',@Out_FileName,'"',' FIELDS TERMINATED BY ','"',@Out_FileDelimiter,'"',' OPTIONALLY ENCLOSED BY ',@Enclosed_Character1,@Enclosed_Character2,@Enclosed_Character1,' escaped by "\" LINES TERMINATED BY "\n" ;'
);


PREPARE statement1 from @view_stmt_control1;
Execute statement1;
Deallocate PREPARE statement1;


end if;




if (vChannel = 'WhatsApp') 
then


set @view_stmt1=concat('select ',@Header_Text,'
union all select ',@Header_Column,'  from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
, Event_Master_Instant_Campaign d
 where  d.Trigger_Name = ','"',CampaignName,'"',' and  a.Customer_Id1 not in (select Customer_Id from Hold_Out) and a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.',@CLMSegmentName,' = 1 and a.',@CampaignSegName,' = 1 and length(b.Mobile) >= 10  and a.Customer_Id1 between ' ,@Start_Cnt, ' and ' ,@End_Cnt, '   limit ',@N,'  INTO OUTFILE ','"',@Out_FileName,'"',' FIELDS TERMINATED BY ','"',@Out_FileDelimiter,'"',' OPTIONALLY ENCLOSED BY ',@Enclosed_Character1,@Enclosed_Character2,@Enclosed_Character1, ' escaped by "\" LINES TERMINATED BY "\n" ;'
);

select @view_stmt1;
PREPARE statement1 from @view_stmt1;
Execute statement1;
Deallocate PREPARE statement1;
SELECT @Out_FileName;


set @cnt=concat("select count(1) into @outcount from ( select CUstomer_Id1 from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
  where a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.",@CLMSegmentName," = 1 and a.",@CampaignSegName," = 1  and length(b.Mobile) >= 10 and a.Customer_Id1 between " ,@Start_Cnt, " and " ,@End_Cnt, " limit ",@N,") as t;");


PREPARE statement5 from @cnt;
Execute statement5;
Deallocate PREPARE statement5;


set @totalcount = @totalcount + @outcount;
if  (@totalcount> @Throttling*1.1 and @Throttling > 1 )
then
LEAVE PROCESS_LOOP;
end if;

 


SELECT 
    CONCAT(@File_Path,
            '/',
            'Instant_Campaign',
            '/Control/',
            (SELECT 
                    value
                FROM
                    M_Config
                WHERE
                    Name = 'File_Name_Prefix'),
            '_',
            @vChannel,
            '_',
            @vTriggerName,
            '_',
            @vdate,
            '_',
            @End_Cnt,
            '.',
            'csv')
INTO @Out_FileName;


set @view_stmt_control1=concat('select ',@Header_Text,'
union all select ',@Header_Column,'  from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
, Event_Master_Instant_Campaign d
 where  d.Trigger_Name = ','"',CampaignName,'"',' and  a.Customer_Id1 in (select Customer_Id from Hold_Out) and a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.',@CLMSegmentName,' = 1 and a.',@CampaignSegName,' = 1  and length(b.Mobile) >= 10  and a.Customer_Id1 between ' ,@Start_Cnt, ' and ' ,@End_Cnt, '   limit ',@N,'  INTO OUTFILE ','"',@Out_FileName,'"',' FIELDS TERMINATED BY ','"',@Out_FileDelimiter,'"',' OPTIONALLY ENCLOSED BY ',@Enclosed_Character1,@Enclosed_Character2,@Enclosed_Character1, ' escaped by "\" LINES TERMINATED BY "\n" ;'
);



 PREPARE statement1 from @view_stmt_control1;
 Execute statement1;
 Deallocate PREPARE statement1;



end if;





if (vChannel = 'Email') 
then


set @view_stmt1=concat('select "Customer_Key","First_Name","Last_Name","Mobile","product_key","product_key","product_key","product_key","product_key","Event_Execution_ID","Event_ID","Event_Execution_Date_ID","Customer_Id","Is_Target","Exclusion_Filter_ID","Communication_Template_ID","Communication_Channel","Message","Email_Message|SMS_Message|PN_Message|WhatsApp_Message|In_Control_Group|In_Event_Control_Group|LT_Control_Covers_Response_Days|ST_Control_Covers_Response_Days|Offer_Code|GV_Code|E_Voucher_Code|Booklet_Code|Is_MAB_Event|Campaign_Key|Segment_Id|Revenue_Center|Recommendation_Genome_Lov_Id_1|Recommendation_Genome_Lov_Id_2|Recommendation_Genome_Lov_Id_3|Recommendation_Genome_Lov_Id_4|Recommendation_Genome_Lov_Id_5|Recommendation_Genome_Lov_Id_6|Recommendation_Product_Id_1|Recommendation_Product_Id_2|Recommendation_Product_Id_3|Recommendation_Product_Id_4|Recommendation_Product_Id_5|Recommendation_Product_Id_6|Run_Mode|Microsite_URL|CMAB_Eligible_Arms|Created_Date|Recommendation_Algo_1|Recommendation_Algo_2|Recommendation_Algo_3|Recommendation_Algo_4|Recommendation_Algo_5|Recommendation_Algo_6" union all select c.Customer_Key,b.First_Name,"null",b.mobile,"null","null","null","null","null","null","null","null","null","null","null","999","EMAIL",replace("',@Message,'","<SOLUS_PFIELD>Customer_Name</SOLUS_PFIELD>",b.First_Name),"null"
 from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
 where  a.Customer_Id1 not in (select Customer_Id from Hold_Out) and  a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.',@CLMSegmentName,' = 1 and a.',@CampaignSegName,' = 1  and b.Email != "" and a.Customer_Id1 between ' ,@Start_Cnt, ' and ' ,@End_Cnt, '  limit ',@N,' 
INTO OUTFILE ','"',@Out_FileName,'"',' FIELDS TERMINATED BY ','"',@Out_FileDelimiter,'"',' OPTIONALLY ENCLOSED BY ',@Enclosed_Character1,@Enclosed_Character2,@Enclosed_Character1,' escaped by "\" LINES TERMINATED BY "\n" ;'
);
 

 
set @cnt=concat("select count(1) into @outcount from (select Customer_id1 from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
 where a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.",@CLMSegmentName," = 1 and a.",@CampaignSegName," = 1  and b.Email != '' and a.Customer_Id1 between " ,@Start_Cnt, " and " ,@End_Cnt, " limit ",@N,") as t;");
 

 PREPARE statement5 from @cnt;
Execute statement5;
Deallocate PREPARE statement5;


if @outcount > 1 then
PREPARE statement1 from @view_stmt1;
Execute statement1;
Deallocate PREPARE statement1;
end if;


SELECT @Out_FileName;
set @totalcount = @totalcount + @outcount;


if  (@totalcount> @Throttling*1.1 and @Throttling > 1 )
then
LEAVE PROCESS_LOOP;
end if;



SELECT 
    CONCAT(@File_Path,
            '/',
            'Instant_Campaign',
            '/Control/',
            (SELECT 
                    value
                FROM
                    M_Config
                WHERE
                    Name = 'File_Name_Prefix'),
            '_',
            @vChannel,
            '_',
            @vTriggerName,
            '_',
            @vdate,
            '_',
            @End_Cnt,
            '.',
            'csv')
INTO @Out_FileName;


set @view_stmt_control1=concat('select "Customer_Key","First_Name","Last_Name","Mobile","product_key","product_key","product_key","product_key","product_key","Event_Execution_ID","Event_ID","Event_Execution_Date_ID","Customer_Id","Is_Target","Exclusion_Filter_ID","Communication_Template_ID","Communication_Channel","Message","Email_Message|SMS_Message|PN_Message|WhatsApp_Message|In_Control_Group|In_Event_Control_Group|LT_Control_Covers_Response_Days|ST_Control_Covers_Response_Days|Offer_Code|GV_Code|E_Voucher_Code|Booklet_Code|Is_MAB_Event|Campaign_Key|Segment_Id|Revenue_Center|Recommendation_Genome_Lov_Id_1|Recommendation_Genome_Lov_Id_2|Recommendation_Genome_Lov_Id_3|Recommendation_Genome_Lov_Id_4|Recommendation_Genome_Lov_Id_5|Recommendation_Genome_Lov_Id_6|Recommendation_Product_Id_1|Recommendation_Product_Id_2|Recommendation_Product_Id_3|Recommendation_Product_Id_4|Recommendation_Product_Id_5|Recommendation_Product_Id_6|Run_Mode|Microsite_URL|CMAB_Eligible_Arms|Created_Date|Recommendation_Algo_1|Recommendation_Algo_2|Recommendation_Algo_3|Recommendation_Algo_4|Recommendation_Algo_5|Recommendation_Algo_6" union all select c.Customer_Key,b.First_Name,"null",b.mobile,"null","null","null","null","null","null","null","null","null","null","null","999","EMAIL",replace("',@Message,'","<SOLUS_PFIELD>Customer_Name</SOLUS_PFIELD>",b.First_Name),"null"
 from CLM_Customer_Segmentation a, CDM_Customer_PII_Master b, CDM_Customer_Key_Lookup c 
 where  a.Customer_Id1 in (select Customer_Id from Hold_Out) and  a.Customer_Id1 = b.Customer_Id and a.Customer_Id1 = c.Customer_id and a.',@CLMSegmentName,' = 1 and a.',@CampaignSegName,' = 1 and b.Email != "" and a.Customer_Id1 between ' ,@Start_Cnt, ' and ' ,@End_Cnt, '  limit ',@N,' 
INTO OUTFILE ','"',@Out_FileName,'"',' FIELDS TERMINATED BY ','"',@Out_FileDelimiter,'"',' OPTIONALLY ENCLOSED BY ',@Enclosed_Character1,@Enclosed_Character2,@Enclosed_Character1,' escaped by "\" LINES TERMINATED BY "\n" ;'
);
 

PREPARE statement1 from @view_stmt_control1;
Execute statement1;
Deallocate PREPARE statement1;

end if;

SET @Start_Cnt = @End_Cnt + 1;

IF @Start_Cnt  >= @Rec_Cnt THEN
	LEAVE PROCESS_LOOP;
END IF;        

END LOOP PROCESS_LOOP;  






end if;





END


