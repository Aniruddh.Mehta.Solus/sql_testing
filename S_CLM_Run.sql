
CREATE OR REPLACE PROCEDURE `S_CLM_Run`(
IN OnDemandEventId BIGINT(20),             
IN Run_Mode Varchar(10),                   
OUT no_of_customers BIGINT(20),           
OUT no_of_customers_final BIGINT(20),     
OUT Filename Text ,                       
OUT FileHeader Text,                      
OUT FileLocation Text,                    
OUT RecoFilename Text,                    
OUT RecoFileHeader Text,                  
OUT RecoFileRecoHeader Text,              
OUT RecoFileLocation Text,                 
OUT vSuccess int (4),                      
OUT vErrMsg Text                           
 
 )
begin
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT;
DECLARE vBatchSize BIGINT DEFAULT 100000;

DECLARE CONTINUE HANDLER FOR SQLWARNING
BEGIN
	GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
	SET @logmsg=CONCAT(ifnull(@errsqlstate,'WARNING') , ifnull(@errno,'1'), ifnull(@errtext,'WARNING MESSAGE'));
    INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
		values ('S_CLM_Run', @logmsg ,now(),'Succeeded',@errno,1);
    SELECT 'S_CLM_Run : Warning Message :' as '', @logmsg as '';
END;
DECLARE EXIT HANDLER FOR SQLEXCEPTION
BEGIN
	GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
	SET @logmsg=CONCAT(ifnull(@errsqlstate,'ERROR') , ifnull(@errno,'1'), ifnull(@errtext,'ERROR MESSAGE'));
    SET vErrMsg=@logmsg;
    SET vSuccess=0;
	INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
		values ('S_CLM_Run', @logmsg ,now(),null,@errno, ifnull(1,1));
	SELECT 'S_CLM_Run : Error Message :' as '', @logmsg as '';
    UPDATE CLM_Process_Log
	SET End='FAILED'
	where ID=@Curent_Process_Id;

END;


select concat('S_CLM_Run() Started .. ',now()) as '';
select Patch_Version into @patch_version from mysolus_version  where Module = 'CLM' order by Id desc limit 1;
select concat('CLM VERSION : ',@patch_version) as '';

SET @logmsg = 'Started';
INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
values ('S_CLM_Run', @logmsg ,now(),now(),'Started', 090921);
select * from ETL_Execution_Details Order by Execution_Id DESC LIMIT 1;
set @OnDemandEventId=OnDemandEventId;
set @Run_Mode=Run_Mode;
Set @vSuccess=0;
Set @vErrMsg1='';
Set @vErrMsg2='';
Set @vErrMsg3='';
if (select IFNULL(count(1),-1) from CLM_Process_Log where End is NULL and Created_Date=current_Date())>0
THEN
	SELECT * from CLM_Process_Log where End is NULL order by Created_Date LIMIT 10;
    SELECT 0 INTO no_of_customers;
	SELECT 0 INTO no_of_customers_final;
	SELECT '' INTO Filename;
	SELECT '' INTO FileHeader;
	SELECT '' INTO FileLocation;
	SELECT 0 INTO vSuccess;
	SELECT CONCAT('ERROR : CLM is already running',(SELECT ID from CLM_Process_Log where End is NULL order by Created_Date LIMIT 1)) INTO vErrMsg;
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR : CLM is already running';
END IF;
insert into CLM_Process_Log(Start,End)
values('Start',NULL);
select ID into @Curent_Process_Id from CLM_Process_Log ORDER BY ID DESC LIMIT 1;


IF @Run_Mode<>'TEST' || @Run_Mode<>'CMAB' THEN
CALL S_CLM_Run_Pre_Checks();
END IF;

CALL S_CLM_Event_Execution(OnDemandEventId,Run_Mode,@no_of_customers,@vSuccess,@vErrMsg1);


set @logmsg=concat(now(), ' CLM IS COMPLETE !!. UPDATING CUSTOMER Segment and Revenue Center IN CAMPAIGN HISTORY !');
select @logmsg as '';
INSERT ignore into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
values ('S_CLM_Run', @logmsg ,now(),now(),' ', 090921);

IF @Run_Mode<>'TEST'  || @Run_Mode<>'CMAB' THEN
    CALL S_CLM_Update_Segment_Event_Execution();
    INSERT IGNORE INTO Event_Execution_Rejected_History(Event_Execution_ID
									, Event_ID
									, Event_Execution_Date_ID
									, Customer_Id
									, Is_Target
									, Exclusion_Filter_ID
									, Communication_Template_ID
									, Communication_Channel
									, Message
									, PN_Message
									, In_Control_Group
									, In_Event_Control_Group
									, LT_Control_Covers_Response_Days
									, ST_Control_Covers_Response_Days
									, Campaign_Key
									, Segment_Id
									, Revenue_Center
									, Microsite_URL)
									 

									SELECT
									  Event_Execution_ID
									, Event_ID
									, Event_Execution_Date_ID
									, Customer_Id
									, Is_Target
									, Exclusion_Filter_ID
									, Communication_Template_ID
									, Communication_Channel
									, Message
									, PN_Message
									, In_Control_Group
									, In_Event_Control_Group
									, LT_Control_Covers_Response_Days
									, ST_Control_Covers_Response_Days
									, Campaign_Key
									, Segment_Id
									, Revenue_Center
									, Microsite_URL

									FROM Event_Execution_Rejected;
END IF;	

SELECT @no_of_customers INTO no_of_customers;
SELECT @no_of_customers INTO no_of_customers_final;
SELECT @Filename INTO Filename;
SELECT @FileHeader INTO FileHeader;
SELECT @FileLocation INTO FileLocation;
SELECT 1 INTO vSuccess;
SELECT concat(@vErrMsg1,@vErrMsg2,@vErrMsg3) INTO vErrMsg;

UPDATE CLM_Process_Log
SET End='ENDED'
where ID=@Curent_Process_Id;

INSERT ignore into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
values ('S_CLM_Run', @Reject_Reason ,now(),now(),'Succeeded', 090921);
SET @logmsg = 'CLM Completed !!';
select 1 into @Guard_rail_check from CLM_Guard_Rail where Event_Execution_Date_Id = date_format(current_date(),'%Y%m%d') LIMIT 1;
SET @Guard_rail_check = ifnull(@Guard_rail_check,0);
IF @Guard_rail_check = 1
THEN
	select concat('GUARDRAIL: Non Governance msg exceeding 20% customer Base - ',(select count(1) from CLM_Guard_Rail where Event_Execution_Date_Id = date_format(current_date(),'%Y%m%d') and Reason = 'GUARDRAIL: Non Governance msg exceeding 20% of customer Base'),'GUARDRAIL: >25% customer for same trigger than last week - ',(select count(1) from CLM_Guard_Rail where Event_Execution_Date_Id = date_format(current_date(),'%Y%m%d') and Reason = 'GUARDRAIL: More than 25% of customer for same trigger than last week'),'GUARDRAIL: >25% Customer Base getting selected by new trigger - ',(select count(1) from CLM_Guard_Rail where Event_Execution_Date_Id = date_format(current_date(),'%Y%m%d') and Reason = 'GUARDRAIL: More than 25% of Customer Base getting selected by a new trigger')) into @gaurd_rail_reason;
	INSERT ignore into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
	values ('S_CLM_Run', @gaurd_rail_reason ,now(),now(),'Succeeded', 090921);
END IF;
INSERT ignore into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
values ('S_CLM_Run', @logmsg ,now(),now(),'Succeeded', 090921);
END

