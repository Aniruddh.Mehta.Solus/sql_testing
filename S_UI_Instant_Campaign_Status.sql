
CREATE OR REPLACE PROCEDURE `S_UI_Instant_Campaign_Status`(IN in_request_json json, OUT out_result_json json)
BEGIN
    
    

    declare IN_USER_NAME varchar(240); 
	declare IN_SCREEN varchar(400); 
    declare EDIT Bigint; 
    
declare AGGREGATION_2 text;
	declare TriggerName text;
	
	SET SQL_SAFE_UPDATES=0;
	SET foreign_key_checks=0;

	Set in_request_json = Replace(in_request_json,'\n','\\n');
  
   
	SET IN_USER_NAME = json_unquote(json_extract(in_request_json,"$.USER"));


    SET TriggerName = json_unquote(json_extract(in_request_json,"$.AGGREGATION_1"));
     SET AGGREGATION_2 = json_unquote(json_extract(in_request_json,"$.AGGREGATION_2"));

	Set @temp_result =Null;
    Set @vSuccess ='';
	
	Set @UserId=IN_USER_NAME;   
	Set @vTriggerName=TriggerName;
	Set @vErrMsg='';
	Set @vSuccess='';
	Set @Err =0;
	set @valid_trigger='';
     set @AGGREGATION_2 = AGGREGATION_2;
     
    Set @Query1=concat('select Trigger_Name into @valid_trigger from Event_Master_Instant_Campaign where Trigger_Name =','"',TriggerName,'"',' limit 1;');
    
    PREPARE statement from @Query1;
							Execute statement;
							Deallocate PREPARE statement;
	if AGGREGATION_2 = ''
then

    if @valid_trigger != '' 
    then 

   Set @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Trigger_Name",Trigger_Name,"Process_Name",Process_Name,"Status",Status,"Log_Time",Log_Time))),"]") into @temp_result from log_solus_instant_campaign where Trigger_Name = @valid_trigger order by Modified_Date desc ;') ;
    
	
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
                            
	else 
    
                            Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Trigger name") )),"]") 
														into @temp_result                       
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
                            
	end if;
    
    
     elseif  AGGREGATION_2 = 'blueprint_rejected_data'
    then 
   
   Set @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object("Trigger_Name",Trigger_Name,"Result",Loading_Status,"Campaign_Type",Campaign_Type,"LifeCycle_Segment",LifeCycle_Segment,"Campaign_Segment",Campaign_Segment,"Custom_SQL",Custom_SQL,"Channel",Channel,"Creative",Creative,"External_Campaign_Key",External_Campaign_Key,"File_Header",File_Header,"Event_Limit",Event_Limit,"Comm_Cool_Off",Comm_Cool_Off))),"]") into @temp_result from EventMaster_csv ;') ;
    
    SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
   
   
   if @temp_result is null
		Then
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Success") )),"]") 
														into @temp_result                       
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	End if;
   
  
    
    
    elseif  AGGREGATION_2 = 'sample_data'
    then 
   
   Set @Query_String = concat('SELECT CONCAT("[",(GROUP_CONCAT(Msg)),"]") into @temp_result from log_solus_instant_campaign where Trigger_Name = @valid_trigger and Process_Name = "Sample_File" order by Modified_Date desc limit 1;') ;
    
    SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
   
    end if;
    
    if @temp_result is null
		Then
								
							Set @Query_String= concat('SELECT CONCAT("[",(GROUP_CONCAT(json_object( "Message","Invalid Parameters") )),"]") 
														into @temp_result                       
												      ;') ;
   
							SELECT @Query_String;
							PREPARE statement from @Query_String;
							Execute statement;
							Deallocate PREPARE statement; 
						
	End if;
							
    SET out_result_json = @temp_result;


END

