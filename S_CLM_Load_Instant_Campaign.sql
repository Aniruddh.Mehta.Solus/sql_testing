
CREATE or replace PROCEDURE `S_CLM_Load_Instant_Campaign`(IN Trigger_Name varchar(128), IN Channel varchar(128))
BEGIN
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT;
Declare vBatchSize BIGINT;
set @trigger_name = Trigger_Name;
set @Channel = Channel;

insert ignore into CLM_Campaign(CampaignName,CampaignStartDate,CampaignEndDate) values ('Instant_Campaign','2021-08-01'
,'2024-08-01');

insert ignore into CLM_Creative(CreativeName,Creative,Header,Communication_Cooloff)
select Trigger_Name, Creative,'Message',0 from Event_Master_Instant_Campaign
where Trigger_Name = @trigger_name
and Channel = @Channel
limit 1;

insert ignore into CLM_Campaign_Trigger(CampTriggerName,CampTriggerDesc,CTState,Event_Limit)
select Trigger_Name,Theme_Name,3,Throttling from Event_Master_Instant_Campaign
where Trigger_Name = @trigger_name 
and Channel = @Channel
limit 1;

insert ignore into CLM_Campaign_Events(EventName,CampaignId,CampTriggerId,ChannelId,CreativeId)
select Trigger_Name,b.CampaignId,c.CampTriggerId,d.ChannelId,e.CreativeId from Event_Master_Instant_Campaign a, CLM_Campaign b, CLM_Campaign_Trigger c,
CLM_Channel d, CLM_Creative e
where  
 a.Theme_Name = b.CampaignName 
and a.Channel = d.ChannelName
and a.Trigger_Name = e.CreativeName
and a.Trigger_Name = c.CampTriggerName
and  a.Trigger_Name = @trigger_name
limit 1;


select Rec_Id into vStart_Cnt from Event_Execution_Instant_Campaign_Stg order by Rec_Id asc limit 1;
select Rec_Id into @Rec_Cnt from Event_Execution_Instant_Campaign_Stg order by Rec_Id desc limit 1;
set vBatchSize = 10000;

PROCESS_LOOP: LOOP

SET vEnd_Cnt = vStart_Cnt + vBatchSize;
set sql_safe_updates=0;

update Event_Execution_Instant_Campaign_Stg a, CDM_Customer_Key_Lookup b
set a.Customer_Id = b.Customer_id
where a.Customer_Key = b.Customer_Key
and a.Rec_Id between vStart_Cnt and vEnd_Cnt; 

update Event_Execution_Instant_Campaign_Stg a, CLM_Campaign_Events b
set a.Event_ID = b.EventID,
a.Communication_Template_ID = b.CreativeId
where a.Event_Name= b.EventName
and a.Rec_Id between vStart_Cnt and vEnd_Cnt; 

update Event_Execution_Instant_Campaign_Stg a, Event_Master_Instant_Campaign b
set a.TimeSlot = b.TimeSlot
where a.Event_Name = b.Trigger_Name
and a.Rec_Id between vStart_Cnt and vEnd_Cnt; 

update Event_Execution_Instant_Campaign_Stg
set Event_Execution_ID = concat(Customer_Id,'-',Event_Execution_Date_ID,'-',Event_ID,'-',Communication_Channel,'-',TimeSlot)
where Rec_Id between vStart_Cnt and vEnd_Cnt; 

update Event_Execution_Instant_Campaign_Stg a
set In_Control_Group = 'Y' 
where Is_Target = 'N'
and a.Rec_Id between vStart_Cnt and vEnd_Cnt; 

SET vStart_Cnt = vEnd_Cnt;

IF vStart_Cnt  >= @Rec_Cnt THEN
	LEAVE PROCESS_LOOP;
END IF;        
END LOOP PROCESS_LOOP;  



select Rec_Id into vStart_Cnt from Event_Execution_Instant_Campaign_Stg order by Rec_Id asc limit 1;
select Rec_Id into @Rec_Cnt from Event_Execution_Instant_Campaign_Stg order by Rec_Id desc limit 1;
set vBatchSize = 10000;

INSERT_LOOP: LOOP

SET vEnd_Cnt = vStart_Cnt + vBatchSize;

insert ignore into Event_Execution_History(Event_Execution_ID,Event_ID,Event_Execution_Date_ID,Customer_Id,Is_Target,Communication_Template_ID,Communication_Channel,Message,In_Control_Group)
 select Event_Execution_ID,Event_ID,Event_Execution_Date_ID,Customer_Id,Is_Target,Communication_Template_ID,Communication_Channel,Message,In_Control_Group
from Event_Execution_Instant_Campaign_Stg
where Event_Execution_ID !=''
and Rec_Id between vStart_Cnt and vEnd_Cnt; 

SET vStart_Cnt = vEnd_Cnt;

IF vStart_Cnt  >= @Rec_Cnt THEN
	LEAVE INSERT_LOOP;
END IF;        
END LOOP INSERT_LOOP; 

select Event_Execution_Date_ID into @Event_Execution_Date_ID from Event_Execution_Instant_Campaign_Stg limit 1;


set @updaterecord=concat("UPDATE Event_Master_Instant_Campaign
set Campaign_Loaded_On = current_time() where Trigger_Name = ","'",@trigger_name,"'"," and Channel = ","'",Channel,"'"," and   replace(date(Files_Created_On),'-','') = ", @Event_Execution_Date_ID,";");


PREPARE statement2 from @updaterecord;
Execute statement2;
Deallocate PREPARE statement2;

END


