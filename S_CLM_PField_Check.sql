
CREATE OR REPLACE PROCEDURE `S_CLM_PField_Check`(IN Communication_Template_ID_cur1 bigint(20), vStartCnt bigint(20), vEndCnt bigint(20))
BEGIN
DECLARE vTempelate TEXT DEFAULT NULL;
DECLARE vTemplate_Original TEXT DEFAULT NULL;
DECLARE V_File_Header varchar(512);

DECLARE EXIT HANDLER FOR SQLEXCEPTION
BEGIN
	GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
	SET @logmsg=CONCAT(ifnull(@errsqlstate,'ERROR') , ifnull(@errno,'1'), ifnull(@errtext,'ERROR MESSAGE'));
    INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
		values ('S_CLM_PField_Check', @logmsg ,now(),null,@errno,1);
SELECT 
    'S_CLM_PField_Check : Error Message :' AS '',
    @logmsg AS '';
END;


SET SQL_SAFE_UPDATES=0;
set @pfieild_proc = 0;

Select Template into vTemplate_Original from  Communication_Template where ID=Communication_Template_ID_cur1;
Select Header into   V_File_Header from Communication_Template where ID=Communication_Template_ID_cur1;
SELECT ifnull(Value,'FIRST') into @conf from M_Config where Name = 'Event_Execution_Id_Position';
SET @conf=ifnull(@conf,'FIRST');
IF @conf <> 'LAST' 
THEN
	SET vTempelate=CONCAT('<SOLUS_PFIELD>Event_Execution_Id</SOLUS_PFIELD>|',vTemplate_Original);
	SET @Out_File_Header = concat('"Event_Execution_ID,',replace(V_File_Header,'|',','),'"');
ELSE
	SET vTempelate=CONCAT(vTemplate_Original,'|<SOLUS_PFIELD>Event_Execution_Id</SOLUS_PFIELD>');
	SET @Out_File_Header = concat('"',replace(V_File_Header,'|',','),',Event_Executon_ID"');
END IF;

SELECT concat('concat("',replace(replace(replace(vTempelate,'|','"),concat("'),'<SOLUS_PFIELD>','",'),'</SOLUS_PFIELD>',',"'),'")') into @vPFields;

IF @vPFields <> '""' and @vPFields <> '' and @vPFields <> 'concat("")'
THEN		
	CALL PROC_CLM_Extract_All_Col_Name_From_Comm_Template(replace(replace(vTempelate,'</SOLUS_PFIELD>',") is NULL or </SOLUS_PFIELD>"),'<SOLUS_PFIELD>','<SOLUS_PFIELD>trim('),'<SOLUS_PFIELD>','</SOLUS_PFIELD>',',', @vPField_For_Check1);
	CALL PROC_CLM_Extract_All_Col_Name_From_Comm_Template(replace(replace(vTempelate,'</SOLUS_PFIELD>',") = '' or </SOLUS_PFIELD>"),'<SOLUS_PFIELD>','<SOLUS_PFIELD>trim('),'<SOLUS_PFIELD>','</SOLUS_PFIELD>',',', @vPField_For_Check2);
	
	SET @PField_Check = CONCAT(REPLACE(@vPField_For_Check1,' ,',' '),' 1<>1 or ', REPLACE(@vPField_For_Check2,' ,',' '),' 1<>1');
	
	SET @sql0=concat("UPDATE Event_Execution ee,  Customer_Details_View cdv
set ee.Is_Target='D' 
where cdv.Customer_Id = ee.Customer_Id
and ee.Customer_Id between ',vStartCnt,' and ',vEndCnt,' and ee.Communication_Template_ID= ',Communication_Template_ID_cur1,'
and cdv.Customer_Id between ',vStartCnt,' and ',vEndCnt,'
and (trim(cdv.Event_Execution_Id) is NULL or trim(cdv.Customer_Id) is NULL or trim(cdv.Event_Execution_Id) = '' )
and ( ',@PField_Check,' ) ");
    

	PREPARE stmt from @sql0;        
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	
   
	INSERT INTO Event_Execution_Rejected 
	SELECT * from Event_Execution 
	where Is_Target='D' and Customer_Id and Communication_Template_ID=Communication_Template_ID_cur1;
	
	DELETE from Event_Execution where Is_Target='D' and Customer_Id and Communication_Template_ID=Communication_Template_ID_cur1;

	CALL PROC_CLM_Extract_All_Col_Name_From_Comm_Template(replace(replace(vTempelate,'</SOLUS_PFIELD>','" </SOLUS_PFIELD>'),'<SOLUS_PFIELD>','<SOLUS_PFIELD>"'),'<SOLUS_PFIELD>','</SOLUS_PFIELD>',',', @vPField_For_Insert2);
	CALL PROC_CLM_Extract_All_Col_Name_From_Comm_Template(replace(replace(vTempelate,'</SOLUS_PFIELD>',"),' ISNULL') </SOLUS_PFIELD>"),'<SOLUS_PFIELD>','<SOLUS_PFIELD>IFNULL(trim('),'<SOLUS_PFIELD>','</SOLUS_PFIELD>',',', @vPField_For_Insert1);
	
    
	SELECT (length(@vPField_For_Insert2)-length(replace(@vPField_For_Insert2,' ,','')))+1 into @num_of_rc;
	SET @loop_var=1;
	SET @pfield_select='"Personaliztion Fields - "';
    	
	WHILE @loop_var<=@num_of_rc
	DO
		SET @Pfield_Insert= CONCAT(substring_index(substring_index(@vPField_For_Insert2,' ,',@loop_var),' ,',-1),",",substring_index(substring_index(@vPField_For_Insert1,' ,',@loop_var),' ,',-1));
		
        SET @pfield_select=concat(@pfield_select,',',@Pfield_Insert);
        
		SET @loop_var=@loop_var+1;
	END WHILE;
    
	SET @sql1=concat('Update Event_Execution_Rejected eer
	SET eer.Message="Personalised Field Not Available",
		eer.Email_Message=(select CONCAT(',@pfield_select,') from Customer_Details_View cdv where cdv.Customer_Id=eer.Customer_Id)
	WHERE eer.Is_Target="D" and eer.Customer_Id between ',vStartCnt,'  and ',vEndCnt,'  and eer.Communication_Template_ID=',Communication_Template_ID_cur1,'');
	
	PREPARE stmt from @sql1;        
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
set @pfieild_proc = 1;

END IF;
END

