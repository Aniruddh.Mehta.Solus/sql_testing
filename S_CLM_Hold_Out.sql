
CREATE OR REPLACE PROCEDURE `S_CLM_Hold_Out`(
IN vStartCnt bigint(10),
IN vEndCnt bigint(10)
)
BEGIN
DECLARE v,v1,weekly_v,weekly_v1 int;
DECLARE Exe_ID int;

SET sql_safe_updates=0;
SET foreign_key_checks=0;


INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
							values ('S_Hold_Out', '',now(),null,'Started', 090921);
SET Exe_ID = F_Get_Execution_Id();        

SELECT count(1) into @vExistingHoldOut from Hold_Out;
SELECT `Value` into @percentage from M_Config where `Name`='Hold_Out_Percentage';

IF @vExistingHoldOut = 0 
THEN
	SELECT `Value` into @percentage from M_Config where `Name`='Hold_Out_Percentage';
	SELECT count(Customer_Id) into v from V_CLM_Customer_One_View;
	SELECT round(v*(@percentage/100)) into v1;
	SET @sql1=concat('insert ignore into Hold_Out (Customer_Id,Load_Execution_ID,Inserted_On)','(
    select Customer_Id,1,now() 
    FROM V_CLM_Customer_One_View 
    ORDER BY RAND() limit ',v1,
    ')');
ELSE
	SELECT count(Customer_Id) into v from V_CLM_Customer_One_View where Customer_ID between vStartCnt and vEndCnt and Is_New_Cust_Flag="Y" ;
	SELECT round(v*(@percentage/100)) into v1;
	SET @sql1=concat('INSERT ignore into Hold_Out (Customer_Id,Load_Execution_ID,Inserted_On)','(
    SELECT Customer_Id,1,now() 
    FROM V_CLM_Customer_One_View 
    WHERE Customer_Id between ',vStartCnt,' and ',vEndCnt,'  
    AND Is_New_Cust_Flag="Y" 
    ORDER BY RAND() limit ',v1,
    ')');
END IF;

prepare stmt from @sql1;
execute stmt;
deallocate prepare stmt;
    
SELECT `Value` into @weekly_percentage from M_Config where `Name`='Weekly_Hold_Out_Percentage';

SELECT 
    COUNT(t1.Customer_Id)
INTO weekly_v FROM
    V_CLM_Customer_One_View t1
        LEFT JOIN
    Hold_Out t2 ON t1.Customer_Id = t2.Customer_Id
WHERE
    t1.Customer_Id BETWEEN vStartCnt AND vEndCnt
        AND t1.Is_New_Cust_Flag = 'Y'
        AND t2.Customer_Id IS NULL ;

select round(weekly_v*(@weekly_percentage/100)) into weekly_v1;

insert ignore into Weekly_Hold_Out (Customer_Id,Load_Execution_ID,Inserted_On)
SELECT 
    t1.Customer_Id, 1, NOW()
FROM
    V_CLM_Customer_One_View t1
        LEFT JOIN
    Hold_Out t2 ON t1.Customer_Id = t2.Customer_Id
WHERE
    t1.Customer_Id BETWEEN vStartCnt AND vEndCnt
        AND t1.Is_New_Cust_Flag = 'Y'
        AND t2.Customer_Id IS NULL
ORDER BY RAND()
LIMIT WEEKLY_V1;

SELECT IFNULL(`Value`,-1) into @Channel_Hold_Out_Check from M_Config where `Name`='Channel_Level_Hold_Out_Percentage';

IF @Channel_Hold_Out_Check = -1 

THEN 

	UPDATE CDM_Customer_Master 
	SET 
		Is_New_Cust_Flag = 'N'
	WHERE Customer_Id between vStartCnt and vEndCnt
	AND Is_New_Cust_Flag <> 'N';
    
END IF;
END

