
CREATE OR REPLACE PROCEDURE PROC_CLM_Col_Name_From_Comm_Template(
	IN vInStr TEXT,
	IN vInStartDelimiter VARCHAR(16),
	IN vInEndDelimiter VARCHAR(16),    
	IN vInStartPos INT,	
	OUT vOutEndPos INT,
    OUT vOutput TEXT
)
BEGIN
	DECLARE vStartDelimiterLen INT;
	DECLARE vEndDelimiterLen INT;
	DECLARE vColStartPos INT;
	DECLARE vColEndPos INT;
	DECLARE vStartDelimiter VARCHAR(16);
	DECLARE vEndDelimiter VARCHAR(16);
	
	SET vOutput = '';
	SET vOutEndPos = 0;

	SET vColStartPos = 0;
	SET vColEndPos = 0;
	SET vStartDelimiter = TRIM(vInStartDelimiter);
	SET vEndDelimiter = TRIM(vInEndDelimiter);

	SET vStartDelimiterLen =  LENGTH(vStartDelimiter);
	IF vStartDelimiterLen  > 0 THEN
		SET vEndDelimiterLen =  LENGTH(vEndDelimiter);
		SET vColStartPos = LOCATE(vStartDelimiter, vInStr, vInStartPos);
		IF vColStartPos > 0 THEN
			SET vColStartPos = vColStartPos + vStartDelimiterLen;
			SET vColEndPos = LOCATE(vEndDelimiter, vInStr, vColStartPos);
			IF vColEndPos > vColStartPos THEN
				SET vOutput = SUBSTR(vInStr,vColStartPos,vColEndPos-vColStartPos);
                SET vOutEndPos = vColEndPos;
			END IF;
		END IF;
	END IF;
END
