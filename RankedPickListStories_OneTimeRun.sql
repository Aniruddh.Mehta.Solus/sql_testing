
CREATE or replace PROCEDURE `S_LOOPING_RankedPickListStoriesFirstTime`(IN vBatchSize bigint)
BEGIN
DECLARE vStart_Cnt BIGINT DEFAULT 0;
DECLARE vEnd_Cnt BIGINT DEFAULT 0;
SELECT Customer_Id INTO vStart_Cnt FROM RankedPickList ORDER BY Customer_Id ASC LIMIT 1;
SELECT Customer_Id INTO @Rec_Cnt FROM RankedPickList ORDER BY Customer_Id DESC LIMIT 1;

truncate RankedPickList_Stories;
set sql_safe_updates=0;
PROCESS_LOOP: LOOP
SET vEnd_Cnt = vStart_Cnt + vBatchSize;


insert ignore into RankedPickList_Stories(Customer_Id)
select distinct(Customer_id) from RankedPickList where Customer_Id between vStart_Cnt and vEnd_Cnt;

update RankedPickList_Stories a, RankedPickList b
set Reco_Product_1 = Product_Id
where Hybridizer_Rank = 1
and a.Customer_Id = b.Customer_id
and a.Customer_Id between vStart_Cnt and vEnd_Cnt;

update RankedPickList_Stories a, RankedPickList b
set Reco_Product_2 = Product_Id
where Hybridizer_Rank = 2
and a.Customer_Id = b.Customer_id
and a.Customer_Id between vStart_Cnt and vEnd_Cnt;

update RankedPickList_Stories a, RankedPickList b
set Reco_Product_3 = Product_Id
where Hybridizer_Rank = 3
and a.Customer_Id = b.Customer_id
and a.Customer_Id between vStart_Cnt and vEnd_Cnt;

update RankedPickList_Stories a, RankedPickList b
set Reco_Product_4 = Product_Id
where Hybridizer_Rank = 4
and a.Customer_Id = b.Customer_id
and a.Customer_Id between vStart_Cnt and vEnd_Cnt;

update RankedPickList_Stories a, RankedPickList b
set Reco_Product_5 = Product_Id
where Hybridizer_Rank = 5
and a.Customer_Id = b.Customer_id
and a.Customer_Id between vStart_Cnt and vEnd_Cnt;


SET vStart_Cnt = vEnd_Cnt;

IF vStart_Cnt  >= @Rec_Cnt THEN
	LEAVE PROCESS_LOOP;
END IF;        
END LOOP PROCESS_LOOP;  

update RankedPickList_Stories
set Story_Id = 3;


END


