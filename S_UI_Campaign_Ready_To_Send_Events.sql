
CREATE or replace PROCEDURE `S_UI_Campaign_Ready_To_Send_Events`()
BEGIN
	
	/*
	DECLARE done1 INT DEFAULT FALSE;
	DECLARE Event_ID_cur1 INT;
	DECLARE Event_Execution_Date_Id_cur1 INT;
	
	
	DECLARE cur1 cursor for 
	SELECT 	Distinct  Event_id,Event_Execution_Date_ID
	FROM  Event_Execution_History
	order by Event_id desc,Event_Execution_Date_ID desc;
				
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
	*/
	Drop table if exists  Event_Master_UI_Temp;
	CREATE TABLE `Event_Master_UI_Temp` (
	`Event_ID` bigint(20) NOT NULL DEFAULT -1,
	`No_Of_Communication_Sent` VARCHAR(128) NOT NULL DEFAULT -1,
	`No_Of_Communication_Rejected` VARCHAR(128) NOT NULL DEFAULT -1,
	`Event_Execution_Date_Id` Bigint(20) NOT NULL DEFAULT -1,
	Sent_Status VARCHAR(128)NOT NULL DEFAULT -1,
	Key Event_ID(Event_ID),
	Key Event_Execution_Date_Id(Event_Execution_Date_Id)
	);
	
	DROP TABLE IF EXISTS Event_Execution_Date_table;
	CREATE TABLE Event_Execution_Date_table (Event_Execution_Date_ID varchar(128));
	INSERT INTO Event_Execution_Date_table
	SELECT DISTINCT Event_Execution_Date_ID FROM Event_Execution_History order by Event_Execution_Date_ID desc limit 2;
	
	BEGIN
		
		DECLARE done1 INT DEFAULT FALSE;
		DECLARE Event_ID_cur1 INT;
		DECLARE Event_Execution_Date_Id_cur1 INT;
		DECLARE cur1 cursor for 
		SELECT 	Distinct  Event_id,Event_Execution_Date_ID
		FROM  Event_Execution_History WHERE Event_Execution_Date_ID IN (sELECT Event_Execution_Date_ID FROM Event_Execution_Date_table)
		order by Event_id desc,Event_Execution_Date_ID desc ;
					
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
	
		Set done1=FALSE;				
		open cur1;
		BEGIN
			new_loop_1: LOOP
			FETCH cur1 into  Event_Id_cur1,Event_Execution_Date_Id_cur1;	
			IF done1 THEN
				LEAVE new_loop_1;
			END IF;
					
									
				Set @Event_Id_cur1 = Event_Id_cur1;
				Set @Event_Execution_Date_Id_cur1 = Event_Execution_Date_Id_cur1;
				
				SET @SQL1 =CONCAT('INSERT IGNORE INTO Event_Master_UI_Temp(Event_Id,Event_Execution_Date_Id,No_Of_Communication_Sent)
									SELECT Event_Id,Event_Execution_Date_Id,Convert(format(Count(1),","),CHAR)
									From 	Event_Execution_History 
									WHERE Event_Id=',@Event_Id_cur1,' and Event_Execution_Date_Id =',@Event_Execution_Date_Id_cur1,' and Is_Target="Y" and In_Control_Group is null and In_Event_Control_Group is null
									
									
									
								');
				
				SELECT @SQL1;
				PREPARE statement from @SQL1;
				Execute statement;
				Deallocate PREPARE statement;
				
				
				SET @SQL2 =CONCAT('UPDATE Event_Master_UI_Temp A
									SET No_Of_Communication_Rejected= (SELECT Convert(format(Count(1),","),CHAR) 
																		FROM Event_Execution_Rejected_History B
																		WHERE A.Event_Id=B.Event_Id 
																		AND A.Event_Execution_Date_ID=B.Event_Execution_Date_ID 
																		AND Event_Id=',@Event_Id_cur1,'
																		AND Event_Execution_Date_Id =',@Event_Execution_Date_Id_cur1,' 
																		
																		)
									where Event_Id=',@Event_Id_cur1,' AND Event_Execution_Date_Id =',@Event_Execution_Date_Id_cur1,' 
									
								');
				
				SELECT @SQL2;
				PREPARE statement from @SQL2;
				Execute statement;
				Deallocate PREPARE statement;
				
				
				SET @SQL3 =CONCAT('UPDATE Event_Master_UI_Temp A
									SET Sent_Status= (SELECT CASE WHEN COUNT_OF_RUN=COUNT_OF_CUSTOMER THEN "Sent" ELSE "Generated" END as  EventSent_Status
														FROM (SELECT Event_Id,Event_Execution_Date_Id,count(run_mode) as COUNT_OF_RUN ,COUNT(1) AS COUNT_OF_CUSTOMER
																FROM Event_Execution_History
																WHERE Event_Id=',@Event_Id_cur1,' 
																AND Event_Execution_Date_Id =',@Event_Execution_Date_Id_cur1,' 
																AND Is_Target="Y" 
																AND In_Control_Group is null 	
																AND In_Event_Control_Group is null 
																)B
														WHERE A.Event_Id=B.Event_Id AND A.Event_Execution_Date_ID=B.Event_Execution_Date_ID and B.Event_Id=',@Event_Id_cur1,'	
													)			
									WHERE Event_Id=',@Event_Id_cur1,' AND Event_Execution_Date_Id =',@Event_Execution_Date_Id_cur1,' 
								');
				
				SELECT @SQL3;
				PREPARE statement from @SQL3;
				Execute statement;
				Deallocate PREPARE statement;
				
									
			END LOOP;
							  
		End;
		CLOSE cur1;

	END;
 END

