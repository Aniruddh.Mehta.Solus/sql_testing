
CREATE OR REPLACE PROCEDURE `S_CLM_Communication_File_Gen_CustLoop`(INOUT iPrintCreative int(1),IN OnDemandEventId bigint(20),IN Communication_Template_ID_cur1 BIGINT(20), IN RUN_MODE Varchar(20),IN vStart_CntCust BIGINT(20),IN vEnd_CntCust BIGINT(20), IN vEvent_Limit BIGINT(20), IN vGlobal_Limit BIGINT(20), OUT Out_FileName text, OUT Out_FileHeader text, OUT Out_FileLocation text)
BEGIN
   /* Version : Campaign UI 3.0 20230704 */
DECLARE vTempelate NVARCHAR(65536) DEFAULT NULL;
DECLARE vTemplate_Original NVARCHAR(65536) DEFAULT NULL;
DECLARE V_ID BIGINT(20);
DECLARE V_Communication_Channel varchar(255);
DECLARE V_File_Header varchar(512);
DECLARE Exe_ID BIGINT(20);
DECLARE output1 TEXT;
DECLARE cov1,cov2 BIGINT DEFAULT 0;
DECLARE CONTINUE HANDLER FOR SQLWARNING
BEGIN
	GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
	SET @logmsg=CONCAT_WS('',ifnull(@errsqlstate,'WARNING-RP') , ifnull(@errno,'1'), ifnull(@errtext,'WARNING MESSAGE RP'));
    INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
		values ('S_CLM_Communication_File_Gen_CustLoop', @logmsg ,now(),null,@errno, 1);

END;
DECLARE EXIT HANDLER FOR SQLEXCEPTION
BEGIN
	GET DIAGNOSTICS CONDITION 1 @errsqlstate=RETURNED_SQLSTATE, @errno=MYSQL_ERRNO, @errtext = MESSAGE_TEXT;
	SET @logmsg=CONCAT_WS('',ifnull(@errsqlstate,'ERROR ') , ifnull(@errno,'1'), ifnull(@errtext,'ERROR MESSAGE '));
    INSERT into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
		values ('S_CLM_Communication_File_Gen_CustLoop', @logmsg ,now(),null,@errno,1);
SELECT 
    'S_CLM_Communication_File_Gen_CustLoop : Error Message :' AS '',
    @logmsg AS '';
END;
SET SQL_SAFE_UPDATES=0;
SET @t_msg_start=now();
SET Exe_ID = F_Get_Execution_Id();   
SELECT Template,ID,Communication_Channel,Header
FROM Communication_Template ct
where ID=Communication_Template_ID_cur1
LIMIT 1
into vTemplate_Original,V_ID,V_Communication_Channel,V_File_Header;
set @vStart_CntCust = vStart_CntCust;
set @vEnd_CntCust = vEnd_CntCust;


IF RUN_MODE <> 'CMAB'
    THEN

    SELECT 
        IFNULL(`Value`, 10000)
    INTO @update_Batchsize FROM
        M_Config
    WHERE
        `Name` = 'Message_Update_Batch_Size';
        
        SELECT 
        `value`
    INTO @unique_offer_code_value FROM
        M_Config
    WHERE
        UPPER(`name`) = 'OFFER_CODE_LEVEL';  
        

    IF @update_Batchsize < 1
    THEN SET @update_Batchsize = 10000;
    END IF;

    SELECT ifnull(Value,'FIRST') into @conf from M_Config where Name = 'Event_Execution_Id_Position';
    SET @conf=ifnull(@conf,'FIRST');
    IF @conf <> 'LAST' 
        THEN
            SET vTempelate=CONCAT('<SOLUS_PFIELD>Event_Execution_Id</SOLUS_PFIELD>|',vTemplate_Original);
            SET @Out_File_Header = CONCAT('"Event_Execution_ID,',replace(V_File_Header,'|',','),'"');
        ELSE
            SET vTempelate=CONCAT(vTemplate_Original,'|<SOLUS_PFIELD>Event_Execution_Id</SOLUS_PFIELD>');
            SET @Out_File_Header = concat('"',replace(V_File_Header,'|',','),',Event_Executon_ID"');
    END IF;

    IF vTempelate = '' or vTempelate = '""' or vTempelate is NULL
    THEN
        SET @vPFields='';
    ELSE 
        SELECT 
    CONCAT('concat("',
            REPLACE(REPLACE(REPLACE(vTempelate, '|', '"),concat("'),
                    '<SOLUS_PFIELD>',
                    '",'),
                '</SOLUS_PFIELD>',
                ',"'),
            '")')
    INTO @vPFields;
    END IF;


    SELECT `Value` INTO @Out_FileLocation FROM M_Config WHERE `Name` = 'File_Path';
    select `Value` into @Channel_Check from M_Config where `Name`='Keep_Channel_Config_Dynamic';
    if @Channel_Check ='Y' then
        CALL S_CLM_Custom_GenFileName_Custom(RUN_MODE,V_ID ,V_Communication_Channel,vEnd_CntCust, @Out_FileName, @Out_FileDelimiter,@OUT_MarkerFile);
    else
        CALL S_CLM_Custom_GenFileName(RUN_MODE,V_ID ,V_Communication_Channel,vEnd_CntCust, @Out_FileName, @Out_FileDelimiter,@OUT_MarkerFile);
    end if;

    SET @Out_File_Header = REPLACE(@Out_File_Header,',',@Out_FileDelimiter);
    IF RUN_MODE = 'TEST' OR RUN_MODE = 'TEST_ALL'
    THEN
        SET @Out_FileName = CONCAT(@Out_FileName,current_time()+0,'.test');
    END IF;
    SET @PField_msg = replace(@vPFields,",concat",(concat(",'",@Out_FileDelimiter,"',concat")));  
        
        if @vPFields <> '""' and @vPFields <> '' and @vPFields <> 'concat("")'
        THEN
            
            CALL PROC_CLM_Extract_All_Col_Name_From_Comm_Template(replace(replace(vTempelate,'</SOLUS_PFIELD>',") is NULL or </SOLUS_PFIELD>"),'<SOLUS_PFIELD>','<SOLUS_PFIELD>trim('),'<SOLUS_PFIELD>','</SOLUS_PFIELD>',',', @vPField_For_Check1);
            CALL PROC_CLM_Extract_All_Col_Name_From_Comm_Template(replace(replace(vTempelate,'</SOLUS_PFIELD>',") = '' or </SOLUS_PFIELD>"),'<SOLUS_PFIELD>','<SOLUS_PFIELD>trim('),'<SOLUS_PFIELD>','</SOLUS_PFIELD>',',', @vPField_For_Check2);
            
            SET @PField_Check = CONCAT(REPLACE(@vPField_For_Check1,' ,',' '),' 1<>1 or ', REPLACE(@vPField_For_Check2,' ,',' '),' 1<>1');
            
            CALL PROC_CLM_Extract_All_Col_Name_From_Comm_Template(replace(replace(vTempelate,'</SOLUS_PFIELD>','" </SOLUS_PFIELD>'),'<SOLUS_PFIELD>','<SOLUS_PFIELD>"'),'<SOLUS_PFIELD>','</SOLUS_PFIELD>',',', @vPField_For_Insert2);
            CALL PROC_CLM_Extract_All_Col_Name_From_Comm_Template(replace(replace(vTempelate,'</SOLUS_PFIELD>',"),' ISNULL') </SOLUS_PFIELD>"),'<SOLUS_PFIELD>','<SOLUS_PFIELD>IFNULL(trim('),'<SOLUS_PFIELD>','</SOLUS_PFIELD>',',', @vPField_For_Insert1);
        
        
        SELECT (length(@vPField_For_Insert2)-length(replace(@vPField_For_Insert2,' ,','')))+1 into @num_of_rc;
        SET @loop_var=1;
        SET @pfield_select='Personalization Field is NULL or BLANK for one or more of Columns :  ';
        
        set @Pfield_Insert = replace(replace(@vPField_For_Insert2,'"',''),',',' OR ');
        SET @pfield_select=concat('"',@pfield_select,@Pfield_Insert,'"');
                
        END IF;

    
    SET @t_initialize=now(); 

    DELETE from Event_Execution_Bitly_AutoIncrId where 1=1;
    INSERT ignore into Event_Execution_Bitly_AutoIncrId(Event_Execution_Id) 
    select Event_Execution_Id from Event_Execution
        where Customer_Id between vStart_CntCust and vEnd_CntCust
        and Communication_Template_Id=V_ID
        and Event_Id=OnDemandEventId;
        
    UPDATE Event_Execution x, Event_Execution_Bitly_AutoIncrId y
    set x.Microsite_URL=concat(GETSHORTURL(x.Event_ID),ConvertIntegerToBase36(y.id))
    where x.Event_Execution_Id=y.Event_Execution_Id;

    SET @t_microsite=now();

    SET @vStart_CntCust_Update=vStart_CntCust; 
    SET @Rec_Cnt_Update=vEnd_CntCust;
    SET @vBatchSize_update=@update_Batchsize;
    
    SET @Limit_Count = 0;

    SET @t_total_vca=time(0);
    SET @t_total_eer=time(0);
    SET @t_total_ee=time(0);
    SET @t_total_view=time(0);
    SET @t_total_logic=time(0);


    SET @t_loop_start = now();
    PROCESS_ALL_CUSTOMERS_UPDATE: LOOP
    
    BEGIN
    
    SET @vEnd_CntCust_Update = @vStart_CntCust_Update +  @vBatchSize_update;

    SET @t_vca=now();
    
        IF(upper(@unique_offer_code_value)='BOTH') 
        THEN 
            call S_CLM_Unique_Offer_Code_Assignment(OnDemandEventId,V_ID,@vStart_CntCust_Update,@vEnd_CntCust_Update,@vSuccessOffer,@vErrMsgOffer);
        END IF;
        
        SET @t_total_vca=@t_total_vca+ timediff(now(),@t_vca);
        
        
    SET @t_eer=now();
    
    SET @EER_Insert= concat_ws('','INSERT INTO `Event_Execution_Rejected`
        (`Event_Execution_ID`,
        `Event_ID`,
        `Event_Execution_Date_ID`,
        `Customer_Id`,
        `Is_Target`,
        `Exclusion_Filter_ID`,
        `Communication_Template_ID`,
        `Communication_Channel`,
        `Message`,
        `PN_Message`
        )
        SELECT 
        `Event_Execution_ID`,
        `Event_ID`,
        `Event_Execution_Date_ID`,
        `Customer_Id`,
        `Is_Target`,
        `Exclusion_Filter_ID`,
        `Communication_Template_ID`,
        `Communication_Channel`,
        ',@pfield_select,',
        `PN_Message`
        FROM
        `Event_Execution`
        WHERE EXISTS ( select 1 from Customer_Details_View where Customer_Details_View.Customer_Id between ',@vStart_CntCust_Update,' and ',@vEnd_CntCust_Update,' 
        AND Event_Execution.Customer_Id=Customer_Details_View.Customer_Id
        AND ( ',@PField_Check,' ) AND Event_Execution.Communication_Template_ID= (',V_ID,' )',')');
        
        PREPARE stmt0 from @EER_Insert;        
        EXECUTE stmt0;
        DEALLOCATE PREPARE stmt0;
        
        SET @t_total_eer=@t_total_eer+ timediff(now(),@t_eer);
        
        SET @t_ee=now();
        
        SET @EE_Deletion= concat_ws('','DELETE FROM Event_Execution 
        WHERE EXISTS (SELECT 1 from Event_Execution_Rejected WHERE Event_Execution_Rejected.Event_Execution_Id = Event_Execution.Event_Execution_Id
        AND Event_Execution_Rejected.Customer_Id between ',@vStart_CntCust_Update,' AND ',@vEnd_CntCust_Update,')');
        
        PREPARE stmt1 from @EE_Deletion;        
        EXECUTE stmt1;
        DEALLOCATE PREPARE stmt1;
        
        SET @t_total_ee=@t_total_ee+ timediff(now(),@t_ee);
        
        SET @t_view=now();
        SET @sql_msg = concat_ws(""," 
        update Event_Execution tgt, (
        select concat(",@PField_msg,") as val, 
        Customer_Id, ID, Event_Execution_Id
        from Customer_Details_View  cdv
                        where cdv.Customer_Id between ",@vStart_CntCust_Update," and ",@vEnd_CntCust_Update,"  
                        and cdv.ID =",V_ID," 
            ) src
        set tgt.Message = src.val
        where tgt.Customer_Id = src.Customer_Id
        and tgt.Customer_Id between ",@vStart_CntCust_Update," and ",@vEnd_CntCust_Update,"  
        and tgt.Event_Execution_Id = src.Event_Execution_Id");

        IF iPrintCreative=0
        THEN
            SELECT concat('<SQL> ', @sql_msg, '</SQL>') as '';
            SET iPrintCreative=1;
        END IF;

        PREPARE stmt2 from @sql_msg;        
        EXECUTE stmt2;
        DEALLOCATE PREPARE stmt2;
        
        SET @t_total_view=@t_total_view+ timediff(now(),@t_view);
        
        
        /* Event/Global Limit Logic */
           
           SET @t_logic=now();
        
        	  SELECT COUNT(1) INTO @Cur_Count FROM Event_Execution
		WHERE In_Control_Group IS NULL AND In_Event_Control_Group IS NULL AND Message IS NOT NULL AND Message <> '';

		SELECT COUNT(1) INTO @Event_Count from Event_Execution_Current_Run WHERE In_Control_Group IS NULL AND In_Event_Control_Group IS NULL AND Event_id = OnDemandEventId;
		
		SELECT COUNT(1) INTO @Global_Count from Event_Execution_Current_Run WHERE In_Control_Group IS NULL AND In_Event_Control_Group IS NULL;
		
		SET @Event_Limit=vEvent_Limit; 
		SET @Global_Limit=vGlobal_Limit;
		
		
		IF (@Cur_Count + @Event_Count) >= @Event_Limit 
		THEN 
            
            SET @Limit_Count = (@Cur_Count + @Event_Count) - @Event_Limit;
			
			SET @vEnd_CntCust_Update = @Rec_Cnt_Update + 1;
			
		ELSEIF (@Cur_Count + @Global_Count) >= @Global_Limit
		THEN
			
            SET @Limit_Count = (@Cur_Count + @Global_Count) - @Global_Limit;
			
			SET @vEnd_CntCust_Update = @Rec_Cnt_Update + 1;
			
		END IF;
        
        SET @t_total_logic=@t_total_logic+ timediff(now(),@t_logic);
       
       IF @vEnd_CntCust_Update >= @Rec_Cnt_Update
        THEN
        LEAVE PROCESS_ALL_CUSTOMERS_UPDATE;
        END IF;
        SET @vStart_CntCust_Update = @vEnd_CntCust_Update+1;
     
     END;
    END LOOP PROCESS_ALL_CUSTOMERS_UPDATE;
    SET @t_loop_end = now();
    
        SET @t_limit=now();
 
    
    SELECT `Value` into @Out_FileCharset from M_Config where `Name`='Default_Character_Set';
    SELECT `Value` into @fields_config_check from M_Config where `Name`='Enclose_Output_in_Double_Quotes';
    IF @fields_config_check='Y' 
        THEN set @fields_config='FIELDS enclosed by \'\"\' escaped by \"\"';
        ELSE set @fields_config='FIELDS escaped by \"\"' ;
    END IF;


    SET @sqlGenerateFile=concat_ws('',' SELECT * INTO OUTFILE "',@Out_FileName,'" charset ',@Out_FileCharset,' ',@fields_config,' LINES TERMINATED BY "\n" 
        FROM
        (SELECT ', @Out_File_Header,' UNION ALL  SELECT MESSAGE 
        FROM Event_Execution ee
        WHERE ee.Customer_Id between "',vStart_CntCust,'" and "',vEnd_CntCust,'"  
        AND ee.Event_Id =',OnDemandEventId,' 
        AND ee.Communication_Template_ID =',V_ID,' 
        AND ee.In_Control_Group is null and ee.In_Event_Control_Group is null AND ee.Is_Target = "Y"
        AND ee.Communication_Channel= "',V_Communication_Channel,'") A');
        
        PREPARE stmt from @sqlGenerateFile;        
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
        SELECT CONCAT_WS('','File Generated Successfully :',NOW(),' File :', @Out_FileName) AS '';
        IF @OUT_MarkerFile <> '' THEN
            SET @sqlMarkerFile=concat('SELECT "" INTO OUTFILE "',@OUT_MarkerFile,'" FIELDS TERMINATED BY "|" LINES TERMINATED BY "\n"');
            PREPARE stmtsqlMarkerFile from @sqlMarkerFile;
            EXECUTE stmtsqlMarkerFile;
            DEALLOCATE PREPARE stmtsqlMarkerFile;
        END IF;
        SET @t_gf=now();
        
    IF RUN_MODE <> 'TEST' AND RUN_MODE <> 'CMAB'
    THEN
            INSERT IGNORE INTO Event_Execution_History
                (`Event_Execution_ID`,
                `Event_ID`,
                `Event_Execution_Date_ID`,
                `Customer_Id`,
                `Is_Target`,
                `Exclusion_Filter_ID`,
                `Communication_Template_ID`,
                `Communication_Channel`,
                `Message`,
                `PN_Message`,
                `In_Control_Group`,
                `In_Event_Control_Group`,
                `LT_Control_Covers_Response_Days`,
                `ST_Control_Covers_Response_Days`,
                `Offer_Code`,
                `Campaign_Key`, 
                `Segment_Id`, 
                `Revenue_Center`,
                `Recommendation_Product_Id_1`,
                `Recommendation_Product_Id_2`,
                `Recommendation_Product_Id_3`,
                `Recommendation_Product_Id_4`,
                `Recommendation_Product_Id_5`,
                `Recommendation_Product_Id_6`,
                `Microsite_URL`)
                SELECT
                Event_Execution_ID,
                Event_ID,
                Event_Execution_Date_ID,
                Customer_Id,
                Is_Target,
                Exclusion_Filter_ID,
                Communication_Template_ID,
                Communication_Channel,
                Message,
                PN_Message,
                In_Control_Group,
                In_Event_Control_Group,
                LT_Control_Covers_Response_Days,
                ST_Control_Covers_Response_Days,
                Offer_Code,
                Campaign_Key,
                Segment_Id,
                Revenue_Center,
                Recommendation_Product_Id_1,
                Recommendation_Product_Id_2,
                Recommendation_Product_Id_3,
                Recommendation_Product_Id_4,
                Recommendation_Product_Id_5,
                Recommendation_Product_Id_6,
                Microsite_URL
                FROM Event_Execution
                WHERE Customer_Id BETWEEN vStart_CntCust AND vEnd_CntCust  
                AND Event_Id = OnDemandEventId;
            
            SET @t_eeh = now();
            
            SET @t_total_vca=cast(@t_total_eer as time(0));
            SET @t_total_eer=cast(@t_total_eer as time(0));
            SET @t_total_ee=cast(@t_total_ee as time(0));
            SET @t_total_view=cast(@t_total_view as time(0));
            SET @t_total_logic=cast(@t_total_logic as time(0));
            


        set @sql_msg_log=
                            CONCAT_WS( '', ' Time(Initialize): ',timediff(@t_initialize,@t_msg_start),
                                    ' Time(Microsite): ',timediff(@t_microsite,@t_initialize),
                                    'Time(VCA): ',@t_total_vca,
                                    ' Time(EER): ',@t_total_eer,
                                    ' Time(EE): ',@t_total_ee,
                                    ' Time(CDV): ',@t_total_view,
                                    ' Time(Limit): ',@t_total_logic,
                                    ' Time(Inner Loop): ',timediff(@t_loop_end,@t_loop_start),
                                    ' Time(Limit Check): ',timediff(@t_limit,@t_loop_end),
                                    ' Time(GF): ',timediff(@t_gf,@t_eeh_check),
                                    ' Time(EEH): ',timediff(@t_eeh,@t_gf)
                                    ) ;
        SELECT @sql_msg_log;
        
        
        /* Update The Comms & Rejects In Summary Table*/
        
        SET @No_Of_Comms = 0;
        SET @No_Of_Rejects = 0;
        
        SET @No_Of_Comms = (SELECT count(1) FROM Event_Execution 
							WHERE Customer_Id BETWEEN vStart_CntCust AND vEnd_CntCust  
							AND Event_Id = OnDemandEventId);
                            
		
        SET @No_Of_Rejects = (SELECT count(1) FROM Event_Execution_Rejected 
							WHERE Customer_Id BETWEEN vStart_CntCust AND vEnd_CntCust  
							AND Event_Id = OnDemandEventId);
                            
		SET @EE_Date_Id = (SELECT Event_Execution_Date_Id FROM Event_Execution 
										WHERE Event_Id = OnDemandEventId LIMIT 1);
                            
		UPDATE Event_Execution_History_Summary
        SET No_Of_Comms = No_Of_Comms + @No_Of_Comms,
			NO_Of_Rejected_Comms = NO_Of_Rejected_Comms + @No_Of_Rejects
		WHERE Event_Id = OnDemandEventId
        AND	  Event_Execution_Date_Id = @EE_Date_Id;

    END IF;

END IF;


IF RUN_MODE='CMAB'
THEN 
    

    SELECT Value INTO @OutFileLocation from M_Config
    WHERE Name = 'CMAB_Input_File_Location';

    SET @Out_FileName=CONCAT_WS('',@OutFileLocation,'/','Trigger_',OnDemandEventId,'.csv');
    SET @Out_File_Header="'Customer_Id'";

    SET @sqlGenerateFile=concat_ws('',' SELECT * INTO OUTFILE "',@Out_FileName,'" LINES TERMINATED BY "\n"
    FROM
    (SELECT ', @Out_File_Header,' UNION ALL  SELECT Customer_Id
    FROM Event_Execution_Current_Run ee
    WHERE ee.Customer_Id between "',vStart_CntCust,'" and "',vEnd_CntCust,'"  
    AND ee.Event_Id =',OnDemandEventId,' 
    AND ee.Communication_Template_ID =',V_ID,' 
    AND ee.Is_Target = "Y"
    AND ee.Communication_Channel= "',V_Communication_Channel,'") A');
    
    PREPARE cmab_stmt from @sqlGenerateFile;        
    EXECUTE cmab_stmt;
    DEALLOCATE PREPARE cmab_stmt;
    SELECT CONCAT_WS('','File Generated Successfully :',NOW(),' File :', @Out_FileName) AS '';

END IF;

UPDATE ETL_Execution_Details SET Status = 'Succeeded', End_Time = NOW() WHERE Procedure_Name = 'S_CLM_Communication_File_Gen_CustLoop' AND Execution_ID = Exe_ID;
SELECT 
    `Value`
INTO @Out_FileLocation FROM
    M_Config
WHERE
    `Name` = 'File_Path';    
SET Out_FileName=@Out_FileName;
SET Out_FileHeader=@Out_File_Header;
SET Out_FileLocation=@Out_FileLocation;
END

